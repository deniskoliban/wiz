// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXIiOnsiZW1haWwiOiJobWFobW91ZEB3aXpob2xkaW5nLmNvbSJ9fSwiZXhwIjoxNjA5NzczMTAyLCJpc3MiOiIiLCJpc3QiOjE2MDkxNjgzMDIsIm5iZiI6MTYwOTE2ODMwMn0.EMMdpZxK0XY08rZsJ1qIKyLGtD0AGmdfZXjTjfmDG6E',
  // apiWithUrl: 'https://test-with-tarek-staging-api.wizshops.com/',
  apiWithUrl: 'test-egypt-mm-123.wizshops.com',
  locales: ['ar', 'en'],
  defaultLocale: 'en',
  googleAPIkey: 'AIzaSyBHmyXH49NMxNXSdaOW9kmRibW14RLemHQ'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
