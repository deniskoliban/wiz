// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  jwt: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvbG9jYWwuZGV2LndpenNob3BzLmNvbSIsImlhdCI6MTYwOTc2OTkxNCwibmJmIjoxNjA5NzY5OTE0LCJleHAiOjE2MTAzNzQ3MTQsImRhdGEiOnsidXNlciI6eyJlbWFpbCI6Im1lcmNoYW50QHdpenNob3BzLmNvbSJ9fX0.6TRjfobOF70DDK-3Dm2xV4DvqK4TOol45x-65loGrkg",
  apiWithUrl: 'local.dev.wizshops.com',
  locales: ['ar', 'en'],
  defaultLocale: 'en',
  googleAPIkey: 'AIzaSyBHmyXH49NMxNXSdaOW9kmRibW14RLemHQ'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
