import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { AuthService } from './services/auth.service';
import { environment } from '../environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'wiz-merchant-manager';

  authorized = false;

  constructor(
    private auth: AuthService,
    private cookies: CookieService,
    private translateService: TranslateService
  ) {
    // console.log(this.auth.isAuthorized(), 'auth');
  }

  ngOnInit(): void {
    this.authorized = this.auth.Authorize();
    this.translateService.addLangs(environment.locales);
    this.translateService.use(environment.defaultLocale);
  }

}
