import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Configs
import { translationConfig } from './translation/translation.config';

// Services
import { OrdersService } from './services/orders.service';
import { AuthService } from './services/auth.service';
import { ProductsService } from './services/products.service';

// Modules

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { CookieModule } from 'ngx-cookie';
import { TranslateModule } from '@ngx-translate/core';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserModule } from '@src/app/modules/user/user.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    UserModule,
    HttpClientModule,
    CookieModule.forRoot(),
    TranslateModule.forRoot(translationConfig),
    NgbModule
  ],
  providers: [
    OrdersService,
    AuthService,
    ProductsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
