import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, ɵangular_packages_common_http_http_e } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class WizconfigService {
  params: HttpParams;
  url: string;
  jwt: string;
  constructor(private http: HttpClient, private cookieService: CookieService) {
      // Replace constants
      this.jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3Atd2VjLXRlc3QtYmgud2l6c2hvcHMuY29tIiwiaWF0IjoxNjA1NzA4Njc2LCJuYmYiOjE2MDU3MDg2NzYsImV4cCI6MTYwNjMxMzQ3NiwiZGF0YSI6eyJ1c2VyIjp7ImVtYWlsIjoiYW50b25Ad2l6aG9sZGluZy5jb20ifX19.o-ffy3zzq-LopFcwr4iAuKg5z-ctXiullFnsiuL1vPA';
      this.url = 'https://wp-wec-test-bh.wizshops.com';
      // with
      // this.jwt = cookieService.get('WIZ_AUTH');
      // this.url = cookieService.get('WIZ_API');
      this.params = new HttpParams()
        .set('jwt', this.jwt);
   }

   private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

   getListOrders() {
    return this
            .http
            .get(`${this.url}/wp-json/wc/v3/orders`, {params: this.params}).pipe(catchError( this.handleError));
          }
}
