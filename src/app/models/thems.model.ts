

export interface Themes {
  themeName?: string;
  data?: ThemeData;
}

export interface ThemeData {
  title?: string;
  id?: number;
  version?: string;
  status?: string;
  activeRequest?: boolean;
}
