export interface PickUpAddress {
  pickup_address?: AddressData | any;
}

export interface AddressData {
  pickup_country?: string; // country
  pickup_city?: string; // state
  pickup_area?: string; // city
  pickup_street?: string;
  pickup_longitude?: string;
  pickup_latitude?: string;
  pickup_locality?: string;
  pickup_building?: string;
  pickup_appartment?: string;
  pickup_phone?: string;
  pickup_location_type?: string;
  pickup_days?: Array<string>;
  pickup_start_time?: string | any;
  pickup_end_time?: string | any;
  phone?: {e164Number?: string};
}



export interface Countries {
  id: number;
  name: string;
  iso3: string;
  iso2: string;
  phone_code: string;
  active: number;
  states: Array<States>;
}

export interface States {
  id: number;
  country_id: number;
  name: string;
  state_code: string;
  cities: Array<Cities>;
  selected: boolean;
}

export interface Cities {
  id: number;
  country_id: number;
  name: string;
  state_code: string;
  selected: boolean;
}
