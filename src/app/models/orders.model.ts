export interface Orders {
  id?: number;
  date_created?: string;
  currency?: string;
  status?: string;
  total?: string;
  billing?: Billing;
  payment_method?: string;
  line_items?: LineItems[];
  discount_total?: string;
  shipping?: Shipping;
  meta_data?: MetaData[];
}

export interface Shipping {
  address_1?: string;
  address_2?: string;
  city?: string;
  company?: string;
  country?: string;
  first_name?: string;
  last_name?: string;
  postcode?: string;
  state?: string;
}

export interface MetaData {
  id?: number;
  key?: string;
  value?: string;
}

export interface LineItems {
  id?: number;
  name?: string;
  product_id?: number;
  variation_id?: number;
  quantity?: number;
  tax_class?: string;
  subtotal?: string;
  subtotal_tax?: string;
  total?: string;
  total_tax?: string;
  selected?: boolean;
}

export interface Billing {
  first_name?: string;
  last_name?: string;
  address_1?: string;
  address_2?: string;
  city?: string;
  country?: string;
  email?: string;
  phone?: string;
  state?: string;
  company?: string;
}

export interface OrdersStatus {
  slug?: string;
  name?: string;
  total?: number;
}


export interface ActivityBoard {
  meta?: Meta;
  data?: BoardData[];
}

export interface BoardData {
  id?: number;
  title_ar?: string;
  title_en?: string;
  created_at?: string;
}

export interface Meta {
  page?: number;
}


export interface DateSelection {
  start_date?: string;
  end_Date?: string;
}


export interface ProductImages {
  id?: number;
  images?: ImagesSource[];
}
export interface ImagesSource {
  src?: string;
}

export interface WizardStatus {
  step?: number;
  status?: string;
  caption?: string;
}
