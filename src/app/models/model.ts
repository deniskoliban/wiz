import { Billing, LineItems, MetaData, Shipping} from './orders.model';

export interface Invoice {
  id?: number;
  parent_id?: string;
  currency?: string;
  status?: string;
  total?: string;
  payment_method_title?: string;
  payment_method?: string;
  date_created?: string;
  date_modified?: string;
  email?: string;
  phone?: string;
  shipping?: Shipping;
  billing?: Billing;
  meta_data?: MetaData[];
  line_items?: LineItems[];
}
