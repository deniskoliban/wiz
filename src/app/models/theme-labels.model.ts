export interface ThemeLabels {
    id: string,
    label: string;
    icon: string;
}