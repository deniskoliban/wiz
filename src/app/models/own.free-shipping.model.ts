import {
  ChargesType,
  ConditionalFreeShippingOwn,
  ConditionalFreeShippingWeight,
  FreeShippingCondition
} from '@src/app/models/shipping';

export interface OwnFreeShippingLocal {
  charge_type?: string;
  order_range_method?: string;
  flat_rate_type?: string;
  conditions?: Array<ConditionsLocal>;
}
export interface ConditionsLocal {
  from_weight_value?: string;
  to_weight_value?: string;
  percentage_rate?: string;
  regions?: Array<RegionsLocal>;
}

export interface RegionsLocal {
  name: string;
  selected: boolean;
  disabled?: boolean;
  cities: {
    selected?: boolean;
    name?: string;
    disabled?: boolean;
  }[];
}

export enum OrderRangeMethod {
  ORDER = 'order amount',
  WEIGHT = 'weight amount'
}

export interface OwnFreeShipping<T = FreeShippingCondition | ConditionalFreeShippingOwn | ConditionalFreeShippingWeight> {
  charge_type: ChargesType;
  conditions: Array<T>;
  order_range_method?: OrderRangeMethod;
  flat_rate_type?: string;
}

export interface Conditions {
  from_weight_value?: string;
  to_weight_value?: string;
  percentage_rate?: string;
  regions?: Regions;
}

export interface Regions {
  [index: string]: string[];
}

export interface Tab {
  conditions: TabConditions[];
  regions: RegionsLocal[];
  allSelected?: boolean;
  copyRegions?: RegionsLocal[];
}

export interface TabConditions {
  from_value?: number;
  from_order_value?: number;
  from_weight_value?: number;
  to_value?: number;
  to_weight_value?: number;
  to_order_value?: number;
  percentage_rate?: number;
  regions?: {};
}
