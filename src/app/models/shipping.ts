import { Dictionary } from 'lodash';
import { OrderRangeMethod } from '@src/app/models/own.free-shipping.model';

export enum ChargesType {
  DEFAULT = '',
  FLAT = 'flat rate',
  CONDITIONAL = 'conditional free shipping',
  FREE = 'free shipping'
}

export interface ShippingCharges {
  charge_type: ChargesType;
  conditions: Array<ConditionalFreeShipping | FlatRateShipping>;
  order_range_method?: OrderRangeMethod;
  flat_rate_type?: string;
}

export interface ShippingChargesOwn extends ShippingCharges {
  groups: Array<Array<ConditionalFreeShippingOwn>>;
}

export interface FreeShippingCondition {
  minimum_order_value?: number;
  flat_rate?: number;
}

export interface FlatRateShipping {
  calculation_method?: string;
  rate?: number;
}

export interface ConditionalFreeShipping {
  from_order_value?: number;
  to_order_value?: number;
  from_weight_value?: number;
  to_weight_value?: number;
  percentage_rate?: number;
  // Custom properties
  from_value?: number;
  to_value?: number;
}

export interface ConditionalFreeShippingOwn extends ConditionalFreeShipping {
  regions: Dictionary<Array<string>>;
}

export interface ConditionalFreeShippingWeight {
  from_weight_value?: number;
  to_weight_value?: number;
  percentage_rate?: number;
  regions?: Dictionary<Array<string>>;
}


export interface COD {
  cod_enabled?: string;
  cod_fees?: number;
}

export interface DeliveryRegions {
  delivery_region_rule: string;
  delivery_region_cities: Dictionary<Array<string>>;
}
