export interface WizardData {
  status: string;
  url: string;
}

export interface IDictionary {
  [index: string]: WizardData;
}

export interface DataAfterLogin {
  ID?: number;
  user_login?: string;
  user_nicename?: string;
  user_email?: string;
  user_url?: string;
  user_registered?: string;
  user_status?: string;
  display_name?: string;
}

export interface MerchantInfo {
  address: string;
  country: string;
  currency: string;
  name: string;
  phone: string;
  city: string;
  business_type: string;
}
