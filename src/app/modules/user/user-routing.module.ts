import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { HomeComponent } from './routes/home/home.component';
import { OrdersComponent } from './routes/orders/orders.component';
import { ActivityBoardComponent } from './components/activity-board/activity-board.component';
import { OrderDetailsComponent } from './routes/order-details/order-details.component';
import { DiscountComponent } from './routes/discount/discount.component';
import { MarketingComponent } from './routes/marketing/marketing.component';
import { AnalyticsComponent } from './routes/analytics/analytics.component';
import { SettingsComponent } from './routes/settings/settings.component';
import { ThemesComponent } from './routes/themes/themes.component';
import { CustomersComponent } from './routes/customers/customers.component';
import { ProductsComponent } from './routes/products/products.component';
import { ProductListComponent } from './routes/product-list/product-list.component';
import { ProductCategoriesComponent } from './routes/product-categories/product-categories.component';
import { InvoiceComponent } from './routes/invoice/invoice.component';
import { AnalyticsOverviewComponent } from './routes/analytics-overview/analytics-overview.component';
import { AnalyticsRevenueComponent } from './routes/analytics-revenue/analytics-revenue.component';
import { AnalyticsOrdersComponent } from './routes/analytics-orders/analytics-orders.component';
import { AnalyticsProductsComponent } from './routes/analytics-products/analytics-products.component';
import { AnalyticsCategoriesComponent } from './routes/analytics-categories/analytics-categories.component';
import { AnalyticsCouponsComponent } from './routes/analytics-coupons/analytics-coupons.component';
import { AnalyticsTaxesComponent } from './routes/analytics-taxes/analytics-taxes.component';
import { AnalyticsDownloadsComponent } from './routes/analytics-downloads/analytics-downloads.component';
import { AnalyticsStockComponent } from './routes/analytics-stock/analytics-stock.component';
import { ThemesLayoutsComponent } from './routes/themes-layouts/themes-layouts.component';
import { ThemesThemesOptionsComponent } from './routes/themes-themes-options/themes-themes-options.component';
import { ThemesThemesStaticPagesComponent } from './routes/themes-themes-static-pages/themes-themes-static-pages.component';
import { ThemesThemesFaqsComponent } from './routes/themes-themes-faqs/themes-themes-faqs.component';
import { ThemesThemesCustomizeComponent } from './routes/themes-themes-customize/themes-themes-customize.component';
import { SettingsGeneralSettingsComponent } from './routes/settings-general-settings/settings-general-settings.component';
import { SettingsPaymentSettingsComponent } from './routes/settings-payment-settings/settings-payment-settings.component';
import { SettingsMarketingSettingssComponent } from './routes/settings-marketing-settingss/settings-marketing-settingss.component';
import { SettingsShippingSettingsComponent } from './routes/settings-shipping-settings/settings-shipping-settings.component';
import { NewProductComponent } from './routes/new-product/new-product.component';
import { TagsComponent } from './routes/tags/tags.component';
import { AttributesComponent } from './routes/attributes/attributes.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { NewThemesComponent } from './routes/new-themes/new-themes.component';
import { CommingSoonComponent } from './components/comming-soon/comming-soon.component';
import { PaymentMethodsComponent } from '@src/app/modules/user/routes/payment-methods/payment-methods.component';
import { WizpayFormComponent } from '@src/app/modules/user/routes/wizpay-form/wizpay-form.component';
import { PricingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/pricing/pricing.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'payment-methods',
        component: PaymentMethodsComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'pricing',
        component: PricingComponent
      },
      {
        path: 'orders/:id',
        component: OrderDetailsComponent
      },
      {
        path: 'products',
        component: ProductsComponent,
        children: [
          {
            path: 'all-products',
            // ProductListComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/edit.php?frame=yes&post_type=product' },
          },
          {
            path: 'categories',
            // ProductCategoriesComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/edit-tags.php?frame=yes&taxonomy=product_cat&post_type=product' },
          },
          {
            path: 'new-product',
            // NewProductComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/post-new.php?post_type=product' },
          },
          {
            path: 'tags',
            // TagsComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/edit-tags.php?frame=yes&taxonomy=product_tag&post_type=product' }
          },
          {
            path: 'attributes',
            // AttributesComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/edit.php?post_type=product&page=product_attributes' }
          }
        ]
      },
      {
        path: 'shipping',
        loadChildren: () => import('./routes/shipping/shipping.module').then(m => m.ShippingModule)
      },
      {
        path: 'customers',
        // component: CustomersComponent
        component: IframeComponent,
        data: { redirect: '/wp-admin/admin.php?page=customers-dashboard'}
      },
      {
        path: 'themes',
        component: ThemesComponent,
        children: [
          {
            path: 'layouts',
            // ThemesLayoutsComponent
            component: ThemesLayoutsComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_themes' }
          },
          {
            path: 'themes-options-old',
            // ThemesThemesOptionsComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/themes.php?page=optionsframework' }
          },
          {
            path: 'static-pages',
            // ThemesThemesStaticPagesComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_themes_static_pages' }
          },
          {
            path: 'faqs',
            // ThemesThemesFaqsComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_faqs' }
          },
          {
            path: 'customize/:layout',
            // ThemesThemesCustomizeComponent
            component: ThemesThemesCustomizeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_merchant_customize' }
          },
          {
            path: 'native-customization',
            // ThemesThemesCustomizeComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/customize.php' }
          },
          {
            path: 'themes-options',
            loadChildren: () => import('./routes/new-themes/new-themes.module').then(m => m.NewThemesModule)
          },          
        ]
      },
      {
        path: 'settings',
        component: SettingsComponent,
        children: [
          {
            path: 'general-settings',
            // SettingsGeneralSettingsComponent
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_merchant_settings' }
          },
          {
            path: 'payment-methods',
            // component: SettingsPaymentSettingsComponent,
            component: PaymentMethodsComponent,
          },
          {
            path: 'marketing-settings',
            // component: SettingsMarketingSettingssComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_merchant_marketing_settings' }
          },
          {
            path: 'shipping-settings',
            // component: SettingsShippingSettingsComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wiz_merchant_shipping_settings' }
          }
        ]
      },
      {
        path: 'discount',
        component: DiscountComponent
      },
      {
        path: 'marketing',
        component: MarketingComponent
      },
      {
        path: 'analytics',
        component: AnalyticsComponent,
        children: [
          {
            path: 'overview',
            // component: AnalyticsOverviewComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/overview' }
          },
          {
            path: 'revenue',
            // component: AnalyticsRevenueComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/revenue' }
          },
          {
            path: 'orders',
            // component: AnalyticsOrdersComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/orders' }
          },
          {
            path: 'products',
            // component: AnalyticsProductsComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/products' }
          },
          {
            path: 'categories',
            // component: AnalyticsCategoriesComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/categories' }
          },
          {
            path: 'coupons',
            // component: AnalyticsCouponsComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/coupons' }
          },
          {
            path: 'taxes',
            // component: AnalyticsTaxesComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/taxes' }
          },
          {
            path: 'downloads',
            // component: AnalyticsDownloadsComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/downloads' }
          },

          {
            path: 'stock',
            // component: AnalyticsStockComponent,
            component: IframeComponent,
            data: { redirect: '/wp-admin/admin.php?page=wc-admin&path=/analytics/stock' }
          },

        ]
      },
      {
        path: 'activity-board',
        component: ActivityBoardComponent
      },
      {
        path: 'invoice/:id',
        component: InvoiceComponent
      },

      {
        path : 'coming-soon',
        component: CommingSoonComponent
      },
      {
        path: 'wizpay-form',
        component: WizpayFormComponent
      },
      { path: 'shipping-setup', loadChildren: () => import('./sub-modules/shipping-setup/shipping-setup.module').then(m => m.ShippingSetupModule) }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
