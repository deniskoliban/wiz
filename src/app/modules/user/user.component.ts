import { Component, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { animate, style, transition, trigger } from '@angular/animations';
import { environment } from '../../../environments/environment';
import { Direction } from '@angular/cdk/bidi';
import { Router } from '@angular/router';
import {HomeService} from '../../services/home.service';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({height: 0}),
        animate('0.3s cubic-bezier(.04,1.02,.62,1.31)', style({height: 'auto'}))
      ]),
      transition(':leave', [
        animate('0.3s cubic-bezier(.04,1.02,.62,1.31)', style({height: 0}))
      ])
    ])
  ]
})
export class UserComponent implements OnInit {
  // variables
  productsMenu = false;
  analyticsMenu = false;
  settings = false;
  themes = false;
  @ViewChild('drawer', {static: false}) drawer: any;
  window = window;
  shopUrl = '';
  langDirection: Direction = 'ltr';

  isProduction = environment.production;

  constructor(public auth: AuthService , private router: Router, public home: HomeService ) {
  }

  ngOnInit(): void {
    this.customizeOnResize();
    if (environment.production || '' === environment.apiWithUrl) {
      this.shopUrl = this.auth.GetURL('/');
    } else  {
      this.shopUrl = 'https://' + environment.apiWithUrl;
    }
  }

  isDevMode(): boolean {
    return !environment.production;
  }

   logout(): void {
    this.auth.LogOff();
    if (environment.production) {
      window.location.href = 'https://merchant.wizshops.com/login';
    } else {
      alert('not authorized!');
    }
 }

  toggleProductsMenu(): void {
    this.productsMenu = !this.productsMenu;
  }

  toggleThemes(): void {
    this.themes = !this.themes;
  }

  toggleAnalyticsMenu(): void {
    this.analyticsMenu = !this.analyticsMenu;
  }

  toggleSettings(): void {
    this.settings = !this.settings;
  }

  closeProductMenu(): void {
    this.productsMenu = false;
  }

  closeAnalytics(): void {
    this.analyticsMenu = false;
  }

  closeThemes(): void {
    this.themes = false;
  }

  closeSettings(): void {
    this.settings = false;
  }

  customizeOnResize(): void {
    window.addEventListener('resize', () => {
      window.innerWidth > 1200 ? this.drawer.open = true : this.drawer.open = false;
    });
  }

}
