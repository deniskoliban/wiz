import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrdersService } from '../../../../services/orders.service';

@Component({
  selector: 'app-reject-order-alert',
  templateUrl: './reject-order-alert.component.html',
  styleUrls: ['./reject-order-alert.component.scss']
})
export class RejectOrderAlertComponent implements OnInit {
  // variables
  sendRequest: boolean | null = false;

  constructor(public dialogRef: MatDialogRef<RejectOrderAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: { orderId: number }, public order: OrdersService) {
  }

  ngOnInit(): void {
  }

  acceptRejection(): void {
    this.sendRequest = true;
    this.order.rejectOrder(this.data.orderId, {status: 'cancelled', meta_data: [{key: 'wiz_shipping_status', value: 'cancelled'}, {key: 'wiz_payment_status', value: 'cancelled'}]}).subscribe(res => {
      this.sendRequest = false;
      console.log(res, 'REJECTED DATA');
      this.dialogRef.close({data: res});
      this.dialogRef.afterClosed().subscribe(res2 => {
        this.order.orderDetailsObj = res2.data;
        console.log(this.order.orderDetailsObj, 'DIALOG');
      });
    }, err => this.sendRequest = false);
  }

  closeRejectionAlert(): void {
    this.dialogRef.close();
  }
}
