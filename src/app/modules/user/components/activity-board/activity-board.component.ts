import {Component, Input, OnInit} from '@angular/core';
import {ActivityBoard} from '../../../../models/orders.model';

@Component({
  selector: 'app-activity-board',
  templateUrl: './activity-board.component.html',
  styleUrls: ['./activity-board.component.scss']
})
export class ActivityBoardComponent implements OnInit {
  @Input() activityBoard: ActivityBoard = {};
  constructor() { }

  ngOnInit(): void {
  }

}
