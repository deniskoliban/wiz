import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-activate-wiz-pay',
  templateUrl: './activate-wiz-pay.component.html',
  styleUrls: ['./activate-wiz-pay.component.scss']
})
export class ActivateWizPayComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ActivateWizPayComponent>, @Inject(MAT_DIALOG_DATA) public data: {head: string, title: string}) { }

  ngOnInit(): void {
  }

  closeAlert(): void {
    this.dialogRef.close();
  }
}
