import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-success-alert',
  templateUrl: './success-alert.component.html',
  styleUrls: ['./success-alert.component.scss']
})
export class SuccessAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SuccessAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: {orderId: number}) { }

  ngOnInit(): void {
  }

  closeSuccessAlert(): void {
    this.dialogRef.close();
  }

}
