import { Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-error-layer',
  templateUrl: './error-layer.component.html',
  styleUrls: ['./error-layer.component.scss']
})
export class ErrorLayerComponent implements OnInit {
  @Input() errorMsg = '';
  constructor() { }

  ngOnInit(): void {
  }

}
