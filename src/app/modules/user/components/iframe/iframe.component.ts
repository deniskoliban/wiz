import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {
  url: SafeResourceUrl | undefined;
  redirectUrl!: string | '';
  constructor(private sanitizer: DomSanitizer, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.redirectUrl = this.route.snapshot.data.redirect;
    this.url = this.auth.GetIframeURL(this.redirectUrl);
  }

}
