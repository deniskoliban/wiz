import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {HomeService} from '../../../../services/home.service';

@Component({
  selector: 'app-confirm-order-alert',
  templateUrl: './confirm-order-alert.component.html',
  styleUrls: ['./confirm-order-alert.component.scss']
})
export class ConfirmOrderAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmOrderAlertComponent>,
              private homeService: HomeService,
              @Inject(MAT_DIALOG_DATA) public data: { title: string | any, titleWithLink: { title: string | any, link: string | any } }) {
  }

  ngOnInit(): void {
  }

  sendVerification(): void {
    this.homeService.verifyShop().subscribe(res => {
      this.closeDialog();
    }, err => {
      console.log(err);
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
