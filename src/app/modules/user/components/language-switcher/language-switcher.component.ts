import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Direction } from '@angular/cdk/bidi';

@Component({
  selector: 'app-language-switcher',
  templateUrl: './language-switcher.component.html',
  styleUrls: ['./language-switcher.component.scss']
})
export class LanguageSwitcherComponent implements OnInit {

  currentLang!: string;
  languages: Array<string> = [];

  @Output() langDirectionChanged: EventEmitter<Direction> = new EventEmitter();

  private htmlTag!: HTMLHtmlElement;

  constructor(
    private translate: TranslateService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.htmlTag = this.document.getElementsByTagName('html')[0] as HTMLHtmlElement;
    this.currentLang = this.translate.currentLang;
    this.languages = this.translate.getLangs();
    setTimeout(() => {
      this.changeLang(this.currentLang);
    });
  }

  changeLang(lang: string): void {
    this.translate.use(lang);
    this.currentLang = lang;
    this.htmlTag.lang = lang;
    this.changeLangDirection();
  }

  changeLangDirection(): void {
    const newDirection = this.currentLang === 'ar' ? 'rtl' : 'ltr';
    this.htmlTag.dir = newDirection;
    this.langDirectionChanged.next(newDirection);
  }

}
