import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-pickup-request',
  templateUrl: './pickup-request.component.html',
  styleUrls: ['./pickup-request.component.scss']
})
export class PickupRequestComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PickupRequestComponent>) { }

  ngOnInit(): void {
  }

  closePickUpAlert(): void {
    this.dialogRef.close();
  }
}
