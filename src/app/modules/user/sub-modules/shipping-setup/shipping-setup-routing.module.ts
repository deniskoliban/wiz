import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShippingSetupComponent } from './shipping-setup.component';
import { ProductsSelectionComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/products-selection/products-selection.component';
import { DeliveryMethodsComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/delivery-methods/delivery-methods.component';
import { PickupAddressComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/pickup-address/pickup-address.component';
import { ShippingChargesComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/shipping-charges/shipping-charges.component';
import { SettingUpFreeShippingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/setting-up-free-shipping/setting-up-free-shipping.component';
import { SettingUpConditionalFreeShippingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/setting-up-conditional-free-shipping/setting-up-conditional-free-shipping.component';
import { SettingUpFlatRateShippingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/setting-up-flat-rate-shipping/setting-up-flat-rate-shipping.component';
import { CongratsComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/congrats/congrats.component';
import { DeliveryRegionsComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/delivery-regions/delivery-regions.component';
import { SettingUpFlatRateOwnShippingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/setting-up-flat-rate-own-shipping/setting-up-flat-rate-own-shipping.component';
import { CodComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/cod/cod.component';
import { OwnConditionalFreeShippingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/own-conditional-free-shipping/own-conditional-free-shipping.component';
import { PricingComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/pricing/pricing.component';

const routes: Routes = [
  {
    path: '',
    component: ShippingSetupComponent,
    children: [
      {
        path: 'product-selection',
        component: ProductsSelectionComponent
      },
      {
        path: 'delivery-methods',
        component: DeliveryMethodsComponent
      },
      {
        path: 'pickup-address',
        component: PickupAddressComponent
      },
      {
        path: 'shipping-charges',
        component: ShippingChargesComponent
      },
      {
        path: 'setting-up-free-shipping',
        component: SettingUpFreeShippingComponent
      },
      {
        path: 'setting-up-conditional-free-shipping',
        component: SettingUpConditionalFreeShippingComponent
      },
      {
        path: 'setting-up-flat-rate',
        component: SettingUpFlatRateShippingComponent
      },
      {
        path: 'congratulations',
        component: CongratsComponent
      },
      {
        path: 'delivery-regions',
        component: DeliveryRegionsComponent
      },
      {
        path: 'setting-up-own-flat-rate',
        component: SettingUpFlatRateOwnShippingComponent
      },
      {
        path: 'cod',
        component: CodComponent
      },
      {
        path: 'wiz-congrats',
        component: CongratsComponent,
        data: {data: {desc: 'Your Wiz Shipping has been successfully set up.', hint: 'You just need to confirm your orders from the dashboard,\n and we will deliver it for you.'}}
      },
      {
        path: 'setting-up-own-conditional-free-shipping',
        component: OwnConditionalFreeShippingComponent
      },
      {
        path: 'own-congrats',
        component: CongratsComponent,
        data: {data: {desc: 'You have successfully set up your own Shipment.', hint: 'It is advised to timely confirm your orders from the dashboard \n& provide delivery for better customer satisfaction.'}}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingSetupRoutingModule {
}
