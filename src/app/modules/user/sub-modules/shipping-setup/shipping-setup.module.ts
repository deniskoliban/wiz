import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ShippingSetupRoutingModule } from './shipping-setup-routing.module';
import { ShippingSetupComponent } from './shipping-setup.component';
import { ProductsSelectionComponent } from './components/products-selection/products-selection.component';
import { DeliveryMethodsComponent } from './components/delivery-methods/delivery-methods.component';
import { PickupAddressComponent } from './components/pickup-address/pickup-address.component';
import { ShippingChargesComponent } from './components/shipping-charges/shipping-charges.component';
import { SettingUpFreeShippingComponent } from './components/setting-up-free-shipping/setting-up-free-shipping.component';
import { SettingUpConditionalFreeShippingComponent } from './components/setting-up-conditional-free-shipping/setting-up-conditional-free-shipping.component';
import { SettingUpFlatRateShippingComponent } from './components/setting-up-flat-rate-shipping/setting-up-flat-rate-shipping.component';
import { CongratsComponent } from './components/congrats/congrats.component';
import { DeliveryRegionsComponent } from './components/delivery-regions/delivery-regions.component';
import { SettingUpFlatRateOwnShippingComponent } from './components/setting-up-flat-rate-own-shipping/setting-up-flat-rate-own-shipping.component';
import { CodComponent } from './components/cod/cod.component';
import { OwnShippingSetupComponent } from './components/own-shipping-setup/own-shipping-setup.component';

import { ReactiveFormsModule } from '@angular/forms';
import { MatRippleModule } from '@angular/material/core';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { NgbButtonsModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ShippingHeaderComponent } from './components/shipping-header/shipping-header.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { MatChipsModule } from '@angular/material/chips';
import { ShippingModule } from '@src/app/modules/user/routes/shipping/shipping.module';
import { SuccesAlertComponent } from './components/succes-alert/succes-alert.component';
import { MatDialogModule } from '@angular/material/dialog';
import { _MatMenuDirectivesModule, MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { SettingConditionalFreeFormComponent } from './components/setting-conditional-free-form/setting-conditional-free-form.component';


@NgModule({
  declarations: [ShippingSetupComponent,
    ProductsSelectionComponent,
    DeliveryMethodsComponent,
    PickupAddressComponent,
    ShippingChargesComponent,
    SettingUpFreeShippingComponent,
    SettingUpConditionalFreeShippingComponent,
    SettingUpFlatRateShippingComponent,
    CongratsComponent,
    DeliveryRegionsComponent,
    SettingUpFlatRateOwnShippingComponent,
    CodComponent,
    OwnShippingSetupComponent,
    ShippingHeaderComponent,
    SuccesAlertComponent,
    SettingConditionalFreeFormComponent,
  ],
  imports: [
    CommonModule,
    ShippingSetupRoutingModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatRippleModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    TranslateModule,
    NgbButtonsModule,
    FormsModule,
    NgxIntlTelInputModule,
    MatChipsModule,
    ShippingModule,
    MatDialogModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
  ]
})
export class ShippingSetupModule {
}
