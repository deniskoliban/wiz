import { Component, OnInit } from '@angular/core';
import { HomeService } from '@services/home.service';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  isEgypt(): boolean {
    return localStorage.getItem('_country') === 'Egypt';
  }

}
