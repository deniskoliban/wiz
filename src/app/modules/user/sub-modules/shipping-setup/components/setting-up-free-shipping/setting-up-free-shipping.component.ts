import { Component, OnInit } from '@angular/core';
import { ShippingService } from '@services/shipping.service';
import { ChargesType, FreeShippingCondition, ShippingCharges } from '@src/app/models/shipping';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SuccesAlertComponent } from '@src/app/modules/user/sub-modules/shipping-setup/components/succes-alert/succes-alert.component';
import { OwnFreeShipping } from '@src/app/models/own.free-shipping.model';
import { HomeService } from '@services/home.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-setting-up-free-shipping',
  templateUrl: './setting-up-free-shipping.component.html',
  styleUrls: ['./setting-up-free-shipping.component.scss']
})
export class SettingUpFreeShippingComponent implements OnInit {

  shippingCharges: OwnFreeShipping = {
    charge_type: ChargesType.FREE,
    conditions: []
  };
  currency: string;

  loading = false;

  form: FormGroup;
  flatRate: FormControl;
  minimumOrder: FormControl;

  constructor(
    private shippingService: ShippingService,
    private home: HomeService,
    private router: Router,
    private matDialog: MatDialog,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.home.merchantInfo$.subscribe(res => this.currency = res.currency);
    this.home.checkMerchant();

    this.form = this.fb.group({
      flat_rate: [undefined, Validators.compose([Validators.required, Validators.max(12)])],
      minimum_order_value: [undefined, Validators.compose([Validators.required, Validators.max(12)])]
    });
    this.flatRate = this.form.get('flat_rate') as FormControl;
    this.minimumOrder = this.form.get('minimum_order_value') as FormControl;

    if (this.isOwn()) {
      console.log('Coming soon...');
    } else {
      this.shippingService.getShippingCharges().subscribe(res => {
        if (res.charge_type === ChargesType.FREE) {
          this.flatRate.setValue((res.conditions[0] as FreeShippingCondition).flat_rate);
          this.minimumOrder.setValue((res.conditions[0] as FreeShippingCondition).minimum_order_value);
        }
      });
    }
  }

  isOwn(): boolean {
    return localStorage.getItem('_d_method') === '0';
  }

  setShippingCharges(): void {
    this.loading = true;
    this.shippingCharges.conditions.push(this.form.value);

    if (this.isOwn()) {
      this.shippingService.setOwnShippingCharges(this.shippingCharges).subscribe(() => {
        this.loading = false;
        this.openDialog('Free shipping has been setup on all your orders.', 'cod');
      });
    } else {
      this.shippingService.setShippingCharges(this.shippingCharges as ShippingCharges).subscribe(() => {
        this.loading = false;
        this.openDialog('Free shipping has been setup on all your orders. \nAll the Wiz expenses will be borne by the merchant.', 'wiz-congrats');
      });
    }
  }

  openDialog(data: string, navigateTo: string): void {
    const dialogRef = this.matDialog.open(SuccesAlertComponent, {
      data,
      width: '500px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigate(['/user/shipping-setup/', navigateTo]);
    });
  }

}
