import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ShippingHeader, ShippingService } from '@services/shipping.service';
import { AlertDialogComponent } from '@src/app/modules/user/routes/shipping/components/alert-dialog/alert-dialog.component';
import { StepProductsComponent } from '@src/app/modules/user/routes/shipping/components/step-products/step-products.component';
import { Product, ProductsService } from '@services/products.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import some from 'lodash-es/some';

@Component({
  selector: 'app-products-selection',
  templateUrl: './products-selection.component.html',
  styleUrls: ['./products-selection.component.scss']
})
export class ProductsSelectionComponent extends StepProductsComponent implements OnInit {
  ownShippingSupported = false;
  control = new FormControl(undefined, Validators.required);

  productSelectionHead: ShippingHeader = {
    step: 'Step 1 of 4',
    head: 'Product Selection',
    bigParagraph: 'Choose the list of products that you sell',
  };
  loading = false;
  constructor(
    protected shipping: ShippingService,
    protected products: ProductsService,
    protected dialog: MatDialog,
    protected router: Router
  ) {
    super(shipping, products);
  }

  postItems(): void {
    if (this.control.invalid) {
      return;
    }
    if (some(this.control.value, item => !item.shipping_available)) {
      this.openDialog().subscribe(res => {
        if (res) {
          // do something
          this.updateProducts();
          localStorage.setItem('shipping-status', '0');
          this.setCurrentMethod('own');
        }
      });
    } else {
      this.updateProducts();
      localStorage.setItem('shipping-status', '1');
      this.setCurrentMethod('wiz');
    }
  }

  setCurrentMethod(method: string): void {
    this.loading = true;
    this.shipping.setDeliveryMethod({delivery_method: method})
      .pipe(
        finalize(() => {
          this.loading = false;
          this.router.navigate(['/user/shipping-setup/delivery-methods']);
        })
      )
      .subscribe(res => {
        console.log(res, 'METHOD');
      });
  }

  updateProducts(): void {
    this.shipping.postSelectedProducts({categories: this.control.value.map((item: Product) => item.name)})
      .subscribe();
  }

  openDialog(template = 'alert'): Observable<boolean> {
    const dialogRef = this.dialog.open(AlertDialogComponent, {data: {template}});

    return dialogRef.afterClosed();
  }

  emitUpdates(): void {}

}
