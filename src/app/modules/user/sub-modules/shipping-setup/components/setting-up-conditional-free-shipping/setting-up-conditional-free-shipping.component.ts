import { Component, OnInit } from '@angular/core';
import { ShippingService } from '@services/shipping.service';
import { Router } from '@angular/router';
import { ChargesType, ShippingCharges } from '@src/app/models/shipping';
import { HomeService } from '@services/home.service';
import { FormControl, FormGroup } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-setting-up-conditional-free-shipping',
  templateUrl: './setting-up-conditional-free-shipping.component.html',
  styleUrls: ['./setting-up-conditional-free-shipping.component.scss']
})
export class SettingUpConditionalFreeShippingComponent implements OnInit {

  loading = false;
  currency: string;
  shippingCharges: Observable<ShippingCharges>;

  form: FormGroup = new FormGroup({
    charge_type: new FormControl(ChargesType.CONDITIONAL)
  });

  constructor(
    private shippingService: ShippingService,
    private home: HomeService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.home.merchantInfo$.subscribe(res => this.currency = res.currency);
    this.home.checkMerchant();
    this.getShippingCharges();
  }

  setShippingCharges(): void {
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    this.shippingService.setShippingCharges(this.form.value)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(() => {
        this.router.navigateByUrl('/user/shipping-setup/wiz-congrats');
      });
  }

  getShippingCharges(): void {
    this.shippingCharges = this.shippingService.getShippingCharges();
  }
}
