import { Component, OnInit } from '@angular/core';
import { ShippingService } from '@services/shipping.service';
import { ChargesType, FlatRateShipping, ShippingCharges } from '@src/app/models/shipping';
import { Router } from '@angular/router';
import { HomeService } from '@services/home.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-setting-up-flat-rate-shipping',
  templateUrl: './setting-up-flat-rate-shipping.component.html',
  styleUrls: ['./setting-up-flat-rate-shipping.component.scss']
})
export class SettingUpFlatRateShippingComponent implements OnInit {

  shippingCharges: ShippingCharges = {
    charge_type: ChargesType.FLAT,
    conditions: []
  };
  loading = false;

  currency: string;

  form: FormGroup;
  calculationMethod: FormControl;
  rate: FormControl;

  constructor(
    private shippingService: ShippingService,
    private home: HomeService,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.home.merchantInfo$.subscribe(res => this.currency = res.currency);
    this.home.checkMerchant();

    this.form = this.fb.group({
      calculation_method: [undefined, Validators.required],
      rate: [undefined, Validators.compose([Validators.required, Validators.max(12)])]
    });
    this.calculationMethod = this.form.get('calculation_method') as FormControl;
    this.rate = this.form.get('rate') as FormControl;

    this.shippingService.getShippingCharges().subscribe(res => {
      if (res.charge_type === ChargesType.FLAT) {
        this.calculationMethod.setValue((res.conditions[0] as FlatRateShipping).calculation_method);
        this.rate.setValue((res.conditions[0] as FlatRateShipping).rate);
      }
    });
  }

  setShippingCharges(): void {
    this.loading = true;
    this.shippingCharges.conditions.push(this.form.value);
    this.shippingService.setShippingCharges(this.shippingCharges).subscribe(res => {
      this.loading = false;
      this.router.navigateByUrl('/user/shipping-setup/wiz-congrats');
    });

  }

}
