import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-succes-alert',
  templateUrl: './succes-alert.component.html',
  styleUrls: ['./succes-alert.component.scss']
})
export class SuccesAlertComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SuccesAlertComponent>, @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

}
