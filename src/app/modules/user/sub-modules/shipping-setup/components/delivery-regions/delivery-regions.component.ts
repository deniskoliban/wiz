import {Component, OnInit} from '@angular/core';
import {City, CountriesService, Country, State} from '@services/countries.service';
import {ShippingService} from '@services/shipping.service';
import {DeliveryRegions} from '@src/app/models/shipping';
import {Router} from '@angular/router';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-delivery-regions',
  templateUrl: './delivery-regions.component.html',
  styleUrls: ['./delivery-regions.component.scss']
})
export class DeliveryRegionsComponent implements OnInit {
  deliveryRegion: DeliveryRegions = {
    delivery_region_rule: '',
    delivery_region_cities: {}
  };
  loading = false;
  countries: Country[] = [];
  currentCountry!: number;
  stateSearch = false;
  citySearch = false;
  filteredStates: State[] = [];
  filteredCity: City[] = [];
  stateSearchQuery = '';

  constructor(private countriesService: CountriesService, private shippingService: ShippingService, private router: Router) {
  }

  ngOnInit(): void {
    this.getCountries();
    this.getDeliveryRegions();
    this.getCurrentCountry();
  }

  getCurrentCountry(): void {
    if (localStorage.getItem('_country') === 'United Arab Emirates') {
      this.currentCountry = 0;
    } else {
      this.currentCountry = 1;
    }
  }

  selectState(state: State): void {
    console.log(state);
    if (state?.selected) {
      state.cities?.forEach(s => s.selected = false);
    } else {
      state?.cities?.forEach(s => s.selected = true);
    }
  }

  searchStates(searchQuery: string): void {
    if (searchQuery.trim()) {
      // @ts-ignore
      this.filteredStates = this.countries[this.currentCountry].states
        .filter((s: any) => s.name?.toLowerCase()?.includes(searchQuery.toLowerCase()));
      this.stateSearch = true;
    } else {
      this.stateSearch = false;
      // this.filteredStates = [];
    }
  }

  searchCity(evt: any, state: State): void {
    if (evt?.target?.value.trim()) {
      // @ts-ignore
      this.filteredCity = state?.cities
        .filter((s: any) => s.name?.toLowerCase()?.includes(evt?.target?.value.toLowerCase()));
      this.citySearch = true;
    } else {
      this.citySearch = false;
      // this.filteredCity = [];
    }
  }

  getCountries(): void {
    this.countriesService.getCountries().subscribe((res) => {
      this.countries = res;
      console.log(this.countries);
    });
  }

  checkCountries(): void {
    if (this.deliveryRegion.delivery_region_rule === 'specific regions') {
      // @ts-ignore
      this.countries[this.currentCountry].states.forEach((state: any) => {
        if (state.selected) {
          this.deliveryRegion.delivery_region_cities[state?.name] = state.cities
            ?.filter((city: any) => city.selected === true).map((c: any) => c.name);
        }
        if (this.checkIfHasCitySelected(state) && !state.selected) {
          state.selected = true;
          const old = this.deliveryRegion.delivery_region_cities;
          this.deliveryRegion.delivery_region_cities[state?.name] = {
            ...state.cities
              ?.filter((city: any) => city.selected === true).map((c: any) => c.name), ...old
          };
        }
      });
    }
  }

  checkIfHasCitySelected(state: State): boolean {
    // @ts-ignore
    return state.cities?.some(c => c.selected === true);
  }

  setDeliveryRegion(): void {
    this.loading = true;
    this.checkCountries();
    console.log(this.deliveryRegion, 'before post');
    this.shippingService.setDeliveryRegions(this.deliveryRegion)
      .pipe(
        finalize(() => {
          this.loading = false;
          this.router.navigateByUrl('/user/shipping-setup/shipping-charges');
        })
      )
      .subscribe(res => {
        console.log(res);
      }, err => console.log(err));
  }

  getDeliveryRegions(): void {
    this.shippingService.getDeliveryRegions().subscribe(res => {
      console.log(res);
      this.deliveryRegion.delivery_region_rule = res.delivery_region_rule;
      this.checkForSelectedCities(res.delivery_region_cities);
    }, err => console.log(err));
  }

  checkForSelectedCities(states: {}): void {
    // @ts-ignore
    this.countries[this.currentCountry].states.forEach((state: any) => {
      if (Object.keys(states).includes(state.name)) {
        state.selected = true;
        state.cities.forEach((city: any) => {
          // @ts-ignore
          if (Object.values(states).flat(1).includes(city.name)) {
            city.selected = true;
          }
        });
      }
    });
  }

}
