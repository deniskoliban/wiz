import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ChargesType,
  ConditionalFreeShippingOwn,
  ConditionalFreeShippingWeight,
  ShippingCharges,
  ShippingChargesOwn
} from '@src/app/models/shipping';
import { ShippingService } from '@services/shipping.service';
import { Router } from '@angular/router';
import { Countries } from '@src/app/models/pickup-address.model';
import {
  OrderRangeMethod,
  OwnFreeShipping,
  RegionsLocal,
  Tab,
  TabConditions
} from '@src/app/models/own.free-shipping.model';
import { finalize, map, switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import clone from 'lodash-es/clone';
import forEach from 'lodash-es/forEach';
import findIndex from 'lodash-es/findIndex';
import assignIn from 'lodash-es/assignIn';

@Component({
  selector: 'app-own-conditional-free-shipping',
  templateUrl: './own-conditional-free-shipping.component.html',
  styleUrls: ['./own-conditional-free-shipping.component.scss']
})

export class OwnConditionalFreeShippingComponent implements OnInit, OnDestroy {
  loading = false;
  citiesCount = 0;
  panelOpenState = false;
  orderRange: OrderRangeMethod = OrderRangeMethod.ORDER;
  shippingChargesOrder: OwnFreeShipping<ConditionalFreeShippingOwn> = {
    charge_type: ChargesType.CONDITIONAL,
    conditions: [
      {
        from_order_value: 5,
        percentage_rate: 5,
        to_order_value: 5,
        regions: {}
      }
    ],
    order_range_method: OrderRangeMethod.ORDER,
    flat_rate_type: ''
  };

  shippingChargesWeight: OwnFreeShipping<ConditionalFreeShippingWeight> = {
    charge_type: ChargesType.CONDITIONAL,
    conditions: [
      {
        from_weight_value: 5,
        percentage_rate: 5,
        to_weight_value: 5,
        regions: {}
      }
    ],
    order_range_method: OrderRangeMethod.WEIGHT,
    flat_rate_type: ''
  };

  customConditionOrder: ConditionalFreeShippingOwn = {
    from_order_value: undefined,
    percentage_rate: undefined,
    to_order_value: -1,
    regions: {}
  };
  customConditionWeight: ConditionalFreeShippingWeight = {
    from_weight_value: undefined,
    percentage_rate: undefined,
    to_weight_value: -1,
    regions: {}
  };

  // ************ OWN PART ************ //
  allCountries: Countries;
  allSelected = false;
  // allRegions: OwnFreeShippingLocal[] = [];
  allStates: Array<RegionsLocal> = [];
  newAllStates: Array<RegionsLocal> = [];
  comparableStates: any = new Set();
  comparableCities: any = new Set();

  // Tabs Container
  tabsContainer: Array<Tab> = [
    {
      conditions: [
        {
          from_order_value: undefined,
          from_weight_value: undefined,
          percentage_rate: undefined,
          to_weight_value: undefined,
          to_order_value: undefined,
          regions: {}
        },
        {
          from_order_value: undefined,
          from_weight_value: undefined,
          percentage_rate: undefined,
          to_weight_value: -1,
          to_order_value: -1,
          regions: {}
        }
      ],
      regions: [],
      allSelected: false
    }
  ];
  selectedTab = this.tabsContainer?.length - 1;

  private subscriptions = new Subscription();

  constructor(private shippingService: ShippingService, private router: Router) {
  }

  ngOnInit(): void {
    this.getOwnShippingData();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  getOwnShippingData(): void {
    this.subscriptions.add(this.shippingService.getDeliveryRegions()
      .pipe(
        switchMap(res => {
          Object.keys(res.delivery_region_cities).forEach(stateName => {
            this.allStates.push({
              name: stateName,
              selected: false,
              cities: res.delivery_region_cities[stateName].map((s: any) => ({name: s, selected: false}))
            });
          });
          this.allStates.forEach(s => {
            this.citiesCount += s.cities?.length || 0;
          });
          const states = [...this.allStates];
          this.tabsContainer.forEach(t => {
            t.regions = states;
          });

          return this.shippingService.getOwnShippingCharges<ConditionalFreeShippingOwn>();
        }),
        map(res => {
          if (res.order_range_method) {
            this.orderRange = res.order_range_method;
          }
          forEach((res as ShippingChargesOwn).groups, groups => {
            forEach(groups, g => {
              if (res.order_range_method === OrderRangeMethod.ORDER) {
                g.from_order_value = g.from_value;
                g.to_order_value = g.to_value || -1;
              } else if (res.order_range_method === OrderRangeMethod.WEIGHT) {
                g.from_weight_value = g.from_value;
                g.to_weight_value = g.to_value || -1;
              }
              delete g.from_value;
              delete g.to_value;
            });
          });

          return res;
        })
      )
      .subscribe(res => {
        const updateTab = (i: number, group: Array<ConditionalFreeShippingOwn>) => {
          this.tabsContainer[i].conditions = group;
          forEach(this.tabsContainer[i].regions, region => {
            if (group[0].regions[region.name]) {
              forEach(group[0].regions[region.name], cityName => {
                const cityIndex = findIndex(region.cities, city => city.name === cityName);
                if (cityIndex >= 0) {
                  region.selected = true;
                  region.cities[cityIndex].selected = true;
                }
              });
            }
          });
        };
        forEach((res as ShippingChargesOwn).groups, (group, i) => {
          if (this.tabsContainer[i]) {
            updateTab(i, group);
          } else {
            this.createNewTab();
            updateTab(i, group);
          }
        });
      }));
  }


  // state and city compare start
  checkIfStateAndCitiesSelected(): void {
    this.allStates.forEach(state => {
      if (this.comparableStates.has(state.name)) {
        state.selected = true;
      }
      state.cities?.forEach(city => {
        if (this.comparableCities.has(city.name)) {
          city.selected = true;
        }
      });
    });
  }

  filledState(state: RegionsLocal): boolean {
    // @ts-ignore
    return state?.cities?.length > 0 || false as boolean;
  }

  tabsLog(): void {
    console.log(this.tabsContainer);
  }

  get selectedCities(): number {
    let selected = 0;
    this.tabsContainer.forEach(t => {
      t.regions?.forEach(r => {
        r.cities?.forEach(c => {
          if (c.selected) {
            selected++;
          }
        });
      });
    });
    return selected;
  }

  get remainingCities(): number {
    return this.citiesCount - this.selectedCities;
  }

  // state and city compare end


// checkbox selection start
  selectAllRegions(): void {
    if (this.allSelected) {
      this.allStates.forEach(state => {
        state.selected = true;
        state.cities?.forEach(city => {
          city.selected = true;
        });
      });
    } else {
      this.allStates.forEach(state => {
        state.selected = false;
        state.cities?.forEach(city => {
          city.selected = false;
        });
      });
    }
    console.log(this.allStates, this.allSelected);
  }

  toggleCitiesSelection(city: { selected?: boolean; name?: string; }, state: RegionsLocal): void {
    city.selected = !city.selected;
    console.log(city);
    state.selected = true;
  }

  // checkbox selection end


  // post data if user select all regions
  setShippingCharges(): void {
    // senior frontend.
    const newTabs: Array<Tab> = clone(this.tabsContainer);

    newTabs.forEach((tab, index) => {
      tab.conditions.forEach(condition => {
        condition.regions = clone(tab.regions
          .filter(fr => fr.selected)
          .map(r => ({
            [r.name]: r.cities
              .filter(sc => sc.selected === true)
              .map(sx => sx.name)
          })));
        Object.values(condition.regions).forEach((v, i) => {
          if (i === 0) {
            condition.regions = {};
          }
          const old = condition.regions;
          condition.regions = assignIn({}, v, old);
        });
      });
    });

    this.postShippingCharges(newTabs);
  }


  selectAllCitiesInState(tabIndex: number, stateIndex: number): void {
    // @ts-ignore
    if (this.tabsContainer[tabIndex]?.regions[stateIndex].selected) {
      // @ts-ignore
      this.tabsContainer[tabIndex]?.regions[stateIndex]?.cities?.forEach(c => {
        c.selected = false;
      });
    } else {
      // @ts-ignore
      this.tabsContainer[tabIndex]?.regions[stateIndex]?.selected = true;
      // @ts-ignore
      this.tabsContainer[tabIndex]?.regions[stateIndex]?.cities?.forEach(c => {
        c.selected = true;
      });
    }
    console.log(tabIndex, stateIndex, 'index for region');
  }

  // setting tab to tabs container end

  userSelectAllRegions(): void {
    this.loading = true;
    let ownObj: any = {};
    let ownInterface: any = {};
    ownInterface = {...this.allStates.map((state: any, index) => ({[state.name]: state.cities.map((city: any) => city.name)}))};
    ownObj = {};
    Object.values(ownInterface).forEach((v: any) => {
      const old = ownObj;
      ownObj = {...v, ...old};
    });

    const conditions = this.shippingChargesWeight.conditions;
    conditions.push(this.orderRange === OrderRangeMethod.WEIGHT ? this.customConditionWeight : this.customConditionOrder);
    conditions.forEach(condition => {
      condition.regions = ownObj;
    });

    this.shippingService.setOwnShippingCharges(
      this.orderRange === OrderRangeMethod.WEIGHT
        ? this.shippingChargesWeight
        : this.shippingChargesOrder
    )
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(() => {
        this.router.navigateByUrl('/user/shipping-setup/cod');
      });

  }

  // ************** OWN SHIPPING PART END **************** //

  insertCondition(index: number): void {
    if (!this.tabsContainer[index]) {
      return;
    }
    const newCondition: TabConditions = {
      from_weight_value: undefined,
      regions: [],
      percentage_rate: undefined,
      from_order_value: undefined,
      to_weight_value: undefined,
      to_order_value: undefined
    };
    this.tabsContainer[index].conditions.splice(this.tabsContainer[index].conditions.length - 1, 0, newCondition);
    const newData = this.tabsContainer[index].conditions;
    this.tabsContainer[index].conditions = [
      {
        from_order_value: undefined,
        from_weight_value: undefined,
        percentage_rate: undefined,
        to_weight_value: -1,
        to_order_value: -1,
        regions: []
      }
    ];
    setTimeout(() => {
      this.tabsContainer[index].conditions = newData;
      console.log(this.tabsContainer[index].conditions, 'after unshift');
    });
  }

  insertOrderRow(): void {
    const condition: ConditionalFreeShippingOwn = {
      from_order_value: undefined,
      percentage_rate: undefined,
      to_order_value: undefined,
      regions: {}
    };
    this.shippingChargesOrder.conditions.push(condition);
  }

  removeCondition(tabIndex: number, conditionIndex: number): void {
    this.tabsContainer[tabIndex].conditions.splice(conditionIndex, 1);
  }

  allRegionsSelected(tabIndex: number): boolean {
    // @ts-ignore
    return this.tabsContainer[tabIndex]?.regions?.every(r => r?.cities?.every(c => c?.selected === true)) || false as boolean;
  }

  allCitiesSelected(stateIndex: number, tabIndex: number): boolean {
    // @ts-ignore
    return this.tabsContainer[tabIndex]?.regions[stateIndex]?.cities.slice().some(c => c.selected === true) || false as boolean;
  }

  isOwn(): boolean {
    return localStorage.getItem('_d_method') === '0';
  }

  toggleAllSelection(tabIndex: number, evt: any): void {
    if (evt.target.checked) {
      // @ts-ignore
      this.tabsContainer[tabIndex].regions.forEach(r => {
        r.selected = true;
        r?.cities?.forEach(c => {
          c.selected = true;
        });
      });
    } else {
      // @ts-ignore
      this.tabsContainer[tabIndex].regions.forEach(r => {
        r.selected = false;
        r?.cities?.forEach(c => {
          c.selected = false;
        });
      });
    }
  }

  checkIfAllTabsSelected(): void {
    for (let i = 0; i < this.tabsContainer.length; i++) {
      this.tabsContainer[i].allSelected = this.allRegionsSelected(i);
    }
    if (this.tabsContainer.every(t => t.allSelected === true)) {
      this.setShippingCharges();
    } else {
      this.createNewTab();
    }
  }

  newTabInstance(): Tab {
    // @ts-ignore
    const newTab: Tab = {
      conditions: [
        {
          from_order_value: undefined,
          from_weight_value: undefined,
          percentage_rate: undefined,
          to_weight_value: undefined,
          to_order_value: undefined,
          regions: {}
        },
        {
          from_order_value: undefined,
          from_weight_value: undefined,
          percentage_rate: undefined,
          to_weight_value: -1,
          to_order_value: -1,
          regions: {}
        }
      ],
      regions: [],
      allSelected: false,
      copyRegions: []
    };

    // 1- we initially start with updating the pointer of the states array
    const copiedStates = clone(this.allStates);

    // 2- loop over each state object (which happens to have the same pointer of each corresponding object in the old array)
    // in order to change its pointer as well
    copiedStates.forEach((state, i) => {

      const copiedState = clone(state); // {cities:[]}
      // if (copiedState.selected) {
      //   copiedState.disabled = true;
      // }
      // if (!state.selected) {
      //   state.disabled = true;
      // }

      state.cities = state.cities.filter(c => c.selected === true);
      // @ts-ignore
      // 3- make a new pointer for each state object in memory

      // @ts-ignore
      // 4- take those cities in a new pointer
      const copiedStateCities = clone(copiedState.cities); // cities

      // 5- loop over each city object and spread it into  a new object and then return it back into its same prev position
      copiedStateCities.forEach((copiedStateCity, cIndex) => {

        // @ts-ignore
        // if (city.selected) {
        //   city.disabled = true;
        // }
        // if (!copiedStateCity.selected) {
        //   copiedStateCity.disabled = true;
        // }
        copiedStateCities[cIndex] = clone(copiedStateCity); // put it back with different pointer
      });

      // replace the old cities with the new updated cities(updated pointers i mean) inside the state
      copiedState.cities = copiedStateCities.filter(c => c.selected === false);
      if (copiedState.cities.length > 0) {
        newTab.regions.push(copiedState); // put your newly formed state inside the regions` tabs
      }
    });
    // this.tabsContainer[this.tabsContainer?.length - 1] = this.tabsContainer[this.tabsContainer?.length - 1].regions.filter(s => s?.cities?.length > 0);
    newTab.regions.forEach(r => r.selected = false);

    this.allStates = clone<Array<RegionsLocal>>(newTab.regions);

    return newTab;
  }

  newRegionInstance(region: Array<RegionsLocal>): Array<RegionsLocal> {
    // @ts-ignore
    const newRegion: Array<RegionsLocal> = [];

    // 1- we initially start with updating the pointer of the states array
    const copiedStates = clone(region);

    // 2- loop over each state object (which happens to have the same pointer of each corresponding object in the old array)
    // in order to change its pointer as well
    copiedStates.forEach((state, i) => {

      const copiedState = clone(state); // {cities:[]}

      // 3- make a new pointer for each state object in memory

      // @ts-ignore
      // 4- take those cities in a new pointer
      const copiedStateCities = clone(copiedState.cities); // cities

      // 5- loop over each city object and spread it into  a new object and then return it back into its same prev position
      copiedStateCities.forEach((copiedStateCity, cIndex) => copiedStateCities[cIndex] = clone(copiedStateCity));

      // replace the old cities with the new updated cities(updated pointers i mean) inside the state
      copiedState.cities = copiedStateCities;
      // put your newly formed state inside the regions` tabs
      newRegion.push(copiedState);
    });

    return newRegion;
  }

  copyRegionToAnother(): void {
    this.tabsContainer.forEach(t => {
      t.copyRegions = [...this.newRegionInstance([...t.regions])];
      t.conditions.forEach(c => {
        const cCopy = {...c};
        // @ts-ignore
        cCopy.regions = {...t.copyRegions?.filter(fr => fr.selected = true).map(r => ({[r.name]: r.cities?.filter(sc => sc.selected = true).map(sx => sx.name)}))};

        // @ts-ignore
        Object.values(cCopy.regions).forEach(v => {
          // @ts-ignore
          const vCopy = cCopy.regions;
          // @ts-ignore
          cCopy.regions = {...vCopy, ...v};
        });
      });
    });

    console.log(this.tabsContainer, 'tabs cont');
  }

  createNewTab(): void {
    // this.copyRegionToAnother();
    this.tabsContainer.push(this.newTabInstance());
    // @ts-ignore
    // newTab.regions.filter(region => region.selected === false || region.cities?.every(city => city.selected === false));

    // @ts-ignore
    // this.tabsContainer.push({...newTab});
    // console.log(this.tabsContainer);
    // this.setShippingCharges();
    this.selectedTab = this.tabsContainer?.length - 1;
  }

  postShippingCharges(tabs: Array<Tab>): void {
    this.loading = true;
    const conditions: Array<TabConditions> = [];
    for (const tab of tabs) {
      if (this.orderRange === 'order amount') {
        for (const condition of tab.conditions) {
          delete condition.to_weight_value;
          delete condition.from_weight_value;
        }
      } else {
        for (const condition of tab.conditions) {
          delete condition.to_order_value;
          delete condition.from_order_value;
        }
      }
      // @ts-ignore
      conditions.push(...tab.conditions);
    }
    console.log(conditions, 'before post');
    const charges: OwnFreeShipping = {
      conditions,
      charge_type: ChargesType.CONDITIONAL,
      flat_rate_type: '',
      order_range_method: this.orderRange
    };
    this.shippingService.setOwnShippingCharges(charges).subscribe(res => {
      this.loading = false;
      this.router.navigateByUrl('/user/shipping-setup/cod');
    }, err => console.log(err));
  }

  disableState(state: RegionsLocal): boolean {
    return state?.disabled || false as boolean;
  }

  disableCity(state: RegionsLocal, index: number): boolean {
    // @ts-ignore
    return state?.cities[index]?.disabled || false as boolean;
  }

  disableSelectAll(tabIndex: number): boolean {
    return (this.tabsContainer?.length > 1) as boolean;
  }

  trackByFn(index: number): number {
    return index;
  }

}
