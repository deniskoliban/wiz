import { Component, OnInit } from '@angular/core';
import { ShippingService } from '@services/shipping.service';
import { ChargesType, ShippingCharges } from '@src/app/models/shipping';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shipping-charges',
  templateUrl: './shipping-charges.component.html',
  styleUrls: ['./shipping-charges.component.scss']
})
export class ShippingChargesComponent implements OnInit {

  shippingCharges: ShippingCharges = {
    charge_type: ChargesType.DEFAULT,
    conditions: []
  };

  constructor(private shippingService: ShippingService, private router: Router) {
  }

  ngOnInit(): void {
    this.getShippingCharges();
  }

  isOwn(): boolean {
    return localStorage.getItem('_d_method') === '0';
  }

  getShippingCharges(): void {
    if (this.isOwn()) {
      this.shippingService.getOwnShippingCharges().subscribe(res => {
        this.shippingCharges = res as ShippingCharges;
      });
    } else {
      this.shippingService.getShippingCharges().subscribe(res => {
        this.shippingCharges = res;
      });
    }
  }

  routeToShippingType(): void {
    const ownPath = this.isOwn() ? '-own' : '';
    switch (this.shippingCharges.charge_type) {
      case ChargesType.FREE:
        this.router.navigate(['/user/shipping-setup/setting-up-free-shipping']);
        break;
      case ChargesType.FLAT:
        this.router.navigate([`/user/shipping-setup/setting-up${ownPath}-flat-rate`]);
        break;
      case ChargesType.CONDITIONAL:
        this.router.navigate([`/user/shipping-setup/setting-up${ownPath}-conditional-free-shipping`]);
        break;
    }
  }

  back(): void {
    if (this.isOwn()) {
      this.router.navigateByUrl('/user/shipping-setup/delivery-regions');
    } else {
      this.router.navigateByUrl('/user/shipping-setup/pickup-address');
    }
  }


}
