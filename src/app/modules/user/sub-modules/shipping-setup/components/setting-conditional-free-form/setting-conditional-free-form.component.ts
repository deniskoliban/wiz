import { Component, Input, OnInit } from '@angular/core';
import { ConditionalFreeFormComponent } from '@src/app/modules/user/routes/shipping/components/conditional-free-form/conditional-free-form.component';
import { ChargesType, ConditionalFreeShipping, ShippingCharges } from '@src/app/models/shipping';
import { Observable } from 'rxjs';
import forEach from 'lodash-es/forEach';

@Component({
  selector: 'app-setting-conditional-free-form',
  templateUrl: './setting-conditional-free-form.component.html',
  styleUrls: ['./setting-conditional-free-form.component.scss']
})
export class SettingConditionalFreeFormComponent extends ConditionalFreeFormComponent implements OnInit {

  @Input() currency: string;
  @Input() data: Observable<ShippingCharges>;

  ngOnInit(): void {
    super.ngOnInit();
    this.data.subscribe(res => {
      if (res.charge_type === ChargesType.CONDITIONAL) {
        this.conditionsArray.clear();
        forEach(res.conditions, condition => {
          this.addConditional(condition as ConditionalFreeShipping);
        });
        this.conditionsArray.markAllAsTouched();
      }
    });
  }

}
