import {Component, OnInit} from '@angular/core';
import {COD} from '@src/app/models/shipping';
import {ShippingService} from '@services/shipping.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cod',
  templateUrl: './cod.component.html',
  styleUrls: ['./cod.component.scss']
})
export class CodComponent implements OnInit {
  loading = false;

  cod: COD = {
    cod_enabled: '',
    cod_fees: undefined
  };

  constructor(private shippingService: ShippingService, private router: Router) {
  }

  ngOnInit(): void {
    this.getCOD();
  }

  getCOD(): void {
    this.shippingService.getCOD().subscribe(res => {
      this.cod = res;
    }, err => console.log(err));
  }

  setCOD(): void {
    this.loading = true;
    this.shippingService.setCOD(this.cod).subscribe(res => {
      console.log(res);
      this.router.navigateByUrl('/user/shipping-setup/own-congrats');
      this.loading = false;
    }, err => console.log(err));
  }

}
