import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

interface Data {
  desc: string;
  hint: string;
}

@Component({
  selector: 'app-congrats',
  templateUrl: './congrats.component.html',
  styleUrls: ['./congrats.component.scss']
})
export class CongratsComponent implements OnInit {

  data: Data = {
    desc: '',
    hint: ''
  };

  constructor(private activatedRoute: ActivatedRoute) {
    this.data = activatedRoute.snapshot.data.data;
  }

  ngOnInit(): void {
  }

}
