import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryMethod, ShippingHeader, ShippingService } from '@services/shipping.service';

@Component({
  selector: 'app-delivery-methods',
  templateUrl: './delivery-methods.component.html',
  styleUrls: ['./delivery-methods.component.scss']
})
export class DeliveryMethodsComponent implements OnInit {
  ownShippingSupported = false;
  shippingStatus: string | any;
  deliveryMethodsHead: ShippingHeader = {
    step: 'Step 2 of 4',
    head: 'Choose Your Delivery Method',
    bigParagraph: 'Select one of the two below options',
  };
  sectionStatus: DeliveryMethod;

  constructor(private activated: ActivatedRoute, private shipping: ShippingService, private router: Router) {
  }

  ngOnInit(): void {
    this.checkShippingType();
    this.getMethod();
  }

  checkIfEgypt(): boolean {
    return localStorage.getItem('_country') === 'Egypt';
  }

  getMethod(): void {
    this.shipping.getDeliveryMethod()
      .subscribe(res => {
        if (!this.ownShippingSupported) {
          this.sectionStatus = DeliveryMethod.WIZ;

          return;
        }

        if (this.checkIfEgypt()) {
          this.sectionStatus = DeliveryMethod.OWN;
        } else {
          this.sectionStatus = res.delivery_method;
        }
      });
  }

  checkShippingType(): void {
    this.shippingStatus = localStorage.getItem('shipping-status');
  }

  next(): void {
    (this.sectionStatus === DeliveryMethod.OWN) ? this.selectDeliveryMethod('0') : this.selectDeliveryMethod('1');
    this.router.navigateByUrl('/user/shipping-setup/pickup-address');
  }

  selectDeliveryMethod(method: string): void {
    localStorage.setItem('_d_method', method);
  }
}
