import { Component, Input, OnInit } from '@angular/core';
import { ShippingHeader } from '@services/shipping.service';

@Component({
  selector: 'app-shipping-header',
  templateUrl: './shipping-header.component.html',
  styleUrls: ['./shipping-header.component.scss']
})
export class ShippingHeaderComponent implements OnInit {
  @Input() headerData: ShippingHeader;
  constructor() { }

  ngOnInit(): void {
  }

}
