import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ShippingHeader, ShippingService } from '@services/shipping.service';
import { CountryISO, PhoneNumberFormat, SearchCountryField, TooltipLabel } from 'ngx-intl-tel-input';
import PlaceResult = google.maps.places.PlaceResult;
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { google } from 'google-maps';
import AutocompleteOptions = google.maps.places.AutocompleteOptions;
import GeocoderResult = google.maps.GeocoderResult;
import { Cities, Countries, PickUpAddress, States } from '@src/app/models/pickup-address.model';
import { Router } from '@angular/router';
import find from 'lodash-es/find';
import assignIn from 'lodash-es/assignIn';
import { finalize, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-pickup-address',
  templateUrl: './pickup-address.component.html',
  styleUrls: ['./pickup-address.component.scss']
})
export class PickupAddressComponent implements OnInit, OnDestroy {
  loading = false;
  pickupPhone: string;
  // interfaces
  pickupInterface: PickUpAddress = {
    pickup_address: {
      pickup_building: '',
      pickup_street: '',
      pickup_appartment: '',
      pickup_area: '',
      pickup_city: '',
      pickup_country: '',
      pickup_days: null,
      pickup_end_time: '',
      pickup_latitude: '',
      pickup_locality: '',
      pickup_location_type: '',
      pickup_longitude: '',
      pickup_phone: '',
      phone: {e164Number: ''},
      pickup_start_time: ''
    }
  };
  allCountries: Array<Countries> = [];
  allStates: Array<States> = [];
  allCites: Array<Cities> = [];
  selectedDaysContainer: Array<string> = [];
  // variables

  // end variables

  pickupHeader: ShippingHeader = {
    step: '',
    head: 'Address Setup',
    bigParagraph: 'Tell us where we can pick up your products from',
    viewPricing: {link: ''},
  };
  // tel
  preferredCountries: Array<CountryISO> = [CountryISO.Egypt, CountryISO.UnitedArabEmirates];
  onlyCountries: Array<CountryISO> = [CountryISO.Egypt, CountryISO.UnitedArabEmirates, CountryISO.SaudiArabia];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  separateDialCode = true;
  phone: FormControl;
  daysList = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  daysControls = new FormControl();
  from = new FormControl(undefined, [Validators.required]);
  to = new FormControl(undefined, [Validators.required]);
  stNumber: Array<number> = [0, 0];
  enNumber: Array<number> = [0, 0];

  // google maps
  private autoComplete: ElementRef;
  private autoComplete2: ElementRef;
  private map: ElementRef;

  @ViewChild('autoComplete', {static: false}) set autoCompleteContent(content: ElementRef) {
    if (content) {
      this.autoComplete = content;
      this.initGoogleAutocomplete();
    }
  }

  @ViewChild('autoComplete2', {static: false}) set autoCompleteContent2(content: ElementRef) {
    if (content) {
      this.autoComplete2 = content;
      this.initGoogleAutocomplete2();
    }
  }

  @ViewChild('map', {static: false}) set mapContent(content: ElementRef) {
    if (content && !this.mapInstance) {
      this.map = content;
      this.initGoogleMap();
    }
  }

  center: google.maps.LatLngLiteral | google.maps.LatLng = {lat: 26.8357675, lng: 30.7956597};
  autoCompleteInstance: google.maps.places.Autocomplete;
  autoCompleteInstance2: google.maps.places.Autocomplete;
  autoCompleteListener: google.maps.MapsEventListener;
  autoCompleteListener2: google.maps.MapsEventListener;

  mapInstance: google.maps.Map;
  mapOptions: google.maps.MapOptions = {
    zoom: 5,
    center: this.center,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  };
  searchControl = new FormControl();
  mapMarkers: Array<google.maps.Marker> = [];
  streetControl = new FormControl();

  googlePlaceOptions: AutocompleteOptions | any = {
    types: ['geocode'],
    componentRestrictions: {country: 'AE'}
  };
  form: FormGroup;
  isOwn = false;

  constructor(
    private shipping: ShippingService,
    private router: Router
  ) {
  }


  ngOnInit(): void {
    this.checkIfOwn();
    this.pickupHeader.step = this.isOwn ? 'Step 3 of 6' : 'Step 3 of 4';
    this.getPickupData();
  }

  ngOnDestroy(): void {
    if (this.autoCompleteListener) {
      this.autoCompleteListener.remove();
    }
    if (this.autoCompleteListener2) {
      this.autoCompleteListener2.remove();
    }
  }

  checkIfOwn(): void {
    this.isOwn = localStorage.getItem('_d_method') === '0';
  }

  next(): void {
    if (this.isOwn) {
      this.router.navigate(['/user/shipping-setup/delivery-regions']);
    } else {
      this.router.navigate(['/user/shipping-setup/shipping-charges']);
    }
  }

  submitPickup(): void {
    // will be in the res success of the submit.
    this.loading = true;
    this.pickupInterface.pickup_address.pickup_phone = this.pickupInterface.pickup_address.phone.e164Number;
    delete this.pickupInterface.pickup_address.phone;
    this.shipping.setPickupAddress(this.pickupInterface)
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(() => this.next());
  }

  // date and time
  endDateChanged(event: Date): void {
    if (!event) {
      return;
    }
    this.pickupInterface.pickup_address.pickup_end_time = event.toLocaleString().substr(12, 5);
  }

  startDateChanged(event: Date): void {
    if (!event) {
      return;
    }
    this.pickupInterface.pickup_address.pickup_start_time = event.toLocaleString().substr(12, 5);
  }

  countryChanged(iso2: string): void {
    this.allCites = [];
    this.allStates = find(this.allCountries, country => country.iso2 === iso2)?.states || [];
    this.googlePlaceOptions.componentRestrictions.country = iso2;
    this.initGoogleAutocomplete();
    this.initGoogleAutocomplete2();
  }

  stateChanged(name: string): void {
    const cities = this.allStates.filter(data => data.name === name);
    this.allCites = cities[0].cities;
  }

  selectedDays(daysControl: FormControl): void {
    this.pickupInterface.pickup_address.pickup_days = daysControl.value;
  }

  deleteDay(dayName: string): void {
    const alteredArray = this.daysControls.value.filter((day: string) => {
      return day !== dayName;
    });
    this.pickupInterface.pickup_address.pickup_days = alteredArray;
    this.daysControls.setValue(alteredArray);
  }

  getPickupData(): void {
    this.shipping.countriesAndStates()
      .pipe(
        switchMap(countries => {
          this.allCountries = countries;

          return this.shipping.getPickupAddress();
        })
      )
      .subscribe(res => {
        this.pickupInterface = assignIn(this.pickupInterface, res);

        this.pickupInterface.pickup_address.phone = this.pickupInterface.pickup_address.pickup_phone;
        this.daysControls.setValue(res.pickup_address.pickup_days);
        this.countryChecker();
        if (!this.isOwn) {
          this.passingTime();
        }
        this.center = new google.maps.LatLng(
          parseFloat(res.pickup_address.pickup_latitude),
          parseFloat(res.pickup_address.pickup_longitude)
        );
        this.googlePlaceOptions.componentRestrictions.country = res.pickup_address.pickup_country;
        this.moveMap();
      });
  }

  countryChecker(): void {
    if (!this.pickupInterface || !this.allCountries.length) {
      return;
    }
    this.allStates = find(this.allCountries, country => country.iso2 === this.pickupInterface.pickup_address.pickup_country)?.states || [];
    this.allCites = this.allStates.filter(data => data.name === this.pickupInterface.pickup_address.pickup_city)[0].cities;
  }

  passingTime(): void {
    this.stNumber = this.removeZeros(this.pickupInterface.pickup_address?.pickup_start_time);
    this.enNumber = this.removeZeros(this.pickupInterface.pickup_address?.pickup_end_time);
  }

  removeZeros(data: string = '00:00'): Array<number> {
    const formattedData = data.split(':');
    // index of 0 is number of hrs, index of 1 is number of minutes
    return [parseInt(formattedData[0], 10), parseInt(formattedData[1], 10)];
  }

  // google maps
  initGoogleMap(): void {
    this.mapInstance = new google.maps.Map(this.map.nativeElement, this.mapOptions);
    this.mapInstance.panTo(this.center);
    this.mapInstance.setZoom(8);
  }

  initGoogleAutocomplete(): void {
    this.ngOnDestroy();
    this.autoCompleteInstance = new google.maps.places.Autocomplete(this.autoComplete.nativeElement, this.googlePlaceOptions);
    this.autoCompleteListener = google.maps.event.addListener(this.autoCompleteInstance, 'place_changed', (places: PlaceResult) => {
      const place: PlaceResult = places ? places : this.autoCompleteInstance.getPlace();
      this.updateAddress(place);
    });
  }

  initGoogleAutocomplete2(): void {
    this.ngOnDestroy();
    this.autoCompleteInstance2 = new google.maps.places.Autocomplete(this.autoComplete2.nativeElement, this.googlePlaceOptions);
    this.autoCompleteListener2 = google.maps.event.addListener(this.autoCompleteInstance2, 'place_changed', (places: PlaceResult) => {
      const place: PlaceResult = places ? places : this.autoCompleteInstance2.getPlace();
      this.updateAddress(place);
    });
  }

  updateAddress(result: PlaceResult | GeocoderResult): void {
    if (!result) {
      return;
    }
    this.pickupInterface.pickup_address.pickup_street = result.formatted_address || '';
    this.pickupInterface.pickup_address.pickup_longitude = result.geometry?.location.lng();
    this.pickupInterface.pickup_address.pickup_latitude = result.geometry?.location.lat();
    this.pickupInterface.pickup_address.pickup_locality = result.address_components && result.address_components[0].short_name;

    this.searchControl.setValue(result.geometry && result.geometry.location);
    this.searchControl.updateValueAndValidity();
    this.setMarker(result);
  }

  setMarker(result: PlaceResult | GeocoderResult): void {
    if (!result || !result.geometry) {
      return;
    }
    this.mapMarkers.forEach(m => m.setMap(null));
    this.center = result.geometry.location;
    const marker = new google.maps.Marker({
      position: result.geometry.location,
      draggable: true,
      clickable: true,
    });
    marker.setMap(this.mapInstance);
    marker.addListener('dragend', res => this.moveMap(res));
    this.mapMarkers = [];
    this.mapMarkers.push(marker);
    this.mapInstance.panTo(result.geometry.location);
    this.mapInstance.setZoom(16);

    this.streetControl.setValue(result.formatted_address);
    this.streetControl.updateValueAndValidity();
    console.log(result, 'RESULT');
  }

  moveMap(event?: any): void {
    if (event) {
      this.center = event.latLng;
    }
    if (!this.center) {
      return;
    }
    const geocoder = new google.maps.Geocoder();
    const params: google.maps.GeocoderRequest = {};
    if (event && event.hasOwnProperty('placeId')) {
      params.placeId = (event as google.maps.IconMouseEvent).placeId;
    } else {
      params.location = (this.center as google.maps.LatLng).toJSON();
    }
    geocoder.geocode(params, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        this.autoComplete.nativeElement.value = results[0].formatted_address;
        this.updateAddress(results[0]);
      }
    });
  }
}
