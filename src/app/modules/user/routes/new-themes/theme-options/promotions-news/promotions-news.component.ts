import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-promotions-news',
  templateUrl: './promotions-news.component.html',
  styleUrls: ['./promotions-news.component.scss']
})
export class PromotionsNewsComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};
  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      enable_post_top: this.data.enable_post_top.value || this.data.enable_post_top.std,
      type_display: this.data.type_display,
      content_custom: this.data.content_custom.value || this.data.content_custom.std,
      category_post: this.data.category_post.value || this.data.category_post.std,
      number_post: this.data.number_post.value || this.data.number_post.std,
      number_post_slide: this.data.number_post_slide.value || this.data.number_post_slide.std,
      enable_fullwidth: this.data.enable_fullwidth.value || this.data.enable_fullwidth.std,
      t_promotion_color: this.data.t_promotion_color.value || this.data.t_promotion_color.std,
      background_area: this.data.background_area.value || this.data.background_area.std,


    }
  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
  }

  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key })
    });

    return arr;

  }

}
