import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header-footer',
  templateUrl: './header-footer.component.html',
  styleUrls: ['./header-footer.component.scss']
})
export class HeaderFooterComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};

  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      header_type: this.data['header-type'],
      fullwidth_main_menu: this.data.fullwidth_main_menu.value || this.data.fullwidth_main_menu.std,
      header_transparent: this.data.header_transparent.value || this.data.header_transparent.std,
      //header-block: this.data.vertical_menu_selected.value || this.data.vertical_menu_selected.std,
      fixed_nav: this.data.fixed_nav.value || this.data.fixed_nav.std,
      search_effect: this.data.search_effect,
      topbar_toggle: this.data.topbar_toggle.value || this.data.topbar_toggle.std,
      topbar_default_show: this.data.topbar_default_show.value || this.data.topbar_default_show.std,
      switch_lang: this.data.switch_lang.value || this.data.switch_lang.std,
      switch_currency: this.data.switch_currency.value || this.data.switch_currency.std,
      switch_currency_format: this.data.switch_currency_format.value || this.data.switch_currency_format.std,
      topbar_content: this.data.topbar_content.value || this.data.topbar_content.std,
      topbar_mobile_icons_toggle: this.data.topbar_mobile_icons_toggle.value || this.data.topbar_mobile_icons_toggle.std,
      bg_color_topbar: this.data.bg_color_topbar.value || this.data.bg_color_topbar.std,
      text_color_topbar: this.data.text_color_topbar.value || this.data.text_color_topbar.std,
      text_color_hover_topbar: this.data.text_color_hover_topbar.value || this.data.text_color_hover_topbar.std,
      bg_color_header: this.data.bg_color_header.value || this.data.bg_color_header.std,
      text_color_header: this.data.text_color_header.value || this.data.text_color_header.std,
      text_color_hover_header: this.data.text_color_hover_header.value || this.data.text_color_hover_header.std,
      bg_color_main_menu: this.data.bg_color_main_menu.value || this.data.bg_color_main_menu.std,
      text_color_main_menu: this.data.text_color_main_menu.value || this.data.text_color_main_menu.std,
      //footer-type: this.data.vertical_menu_selected.value || this.data.vertical_menu_selected.std,
      //footer-mobile: this.data.vertical_menu_selected.value || this.data.vertical_menu_selected.std,

    }
  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
  }

  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key })
    });
    return arr;

  }
}