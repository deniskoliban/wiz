import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  constructor() { }
  optionsValues:any= [];
  optionsKeys:any =[];
  optionObj: any = {};
  singleProductLayout:any = 'new';
  showRelateProducts:any;
  ngOnInit(): void {
    console.log('single-product'+this.data)
   
      this.optionObj = {
        product_detail_layout: this.data.product_detail_layout,
        product_image_layout: this.data.product_image_layout,
        product_image_style: this.data.product_image_style,
        product_thumbs_style: this.data.product_thumbs_style ,
        product_slide_dot: this.data.product_slide_dot.value || this.data.product_slide_dot.std,
        product_slide_arrows: this.data.product_slide_arrows.value || this.data.product_slide_arrows.std,
        product_zoom: this.data['product-zoom'].value || this.data['product-zoom'].std,
        product_image_lightbox: this.data['product-image-lightbox'].value || this.data['product-image-lightbox'].std,
        enable_focus_main_image: this.data.enable_focus_main_image.value || this.data.linkedin_url_follow.std,
        product_sidebar: this.data.product_sidebar,
        single_product_deal: this.data["single-product-deal"].value || this.data["single-product-deal"].std,
        enable_buy_now: this.data.enable_buy_now.value || this.data.enable_buy_now.std,
        buy_now_bg_color: this.data.buy_now_bg_color.value || this.data.buy_now_bg_color.std,
        buy_now_bg_color_hover: this.data.buy_now_bg_color_hover.value || this.data.buy_now_bg_color_hover.std,
        buy_now_color_shadow: this.data.buy_now_color_shadow.value || this.data.buy_now_color_shadow.std,
        enable_fixed_add_to_cart: this.data.enable_fixed_add_to_cart.value || this.data.enable_fixed_add_to_cart.std,
        enable_fixed_buy_now_desktop: this.data.enable_fixed_buy_now_desktop.value || this.data.enable_fixed_buy_now_desktop.std,
        mobile_fixed_add_to_cart: this.data.mobile_fixed_add_to_cart,
        enable_progess_stock: this.data.enable_progess_stock.value || this.data.enable_progess_stock.std,
        tab_style_info: this.data.tab_style_info,
        tab_align_info: this.data.tab_align_info ,
        hide_additional_tab: this.data.hide_additional_tab.value || this.data.hide_additional_tab.std,
        relate_product: this.data.relate_product.value || this.data.relate_product.std,
        relate_product_number: this.data.relate_product_number.value || this.data.relate_product_number.std,
        relate_columns_desk: this.data.relate_columns_desk,
        relate_columns_small: this.data.relate_columns_small,
        relate_columns_tablet: this.data.relate_columns_tablet,
        enable_ajax_addtocart: this.data.enable_ajax_addtocart.value || this.data.enable_ajax_addtocart.std,
        single_product_mobile: this.data.single_product_mobile.value || this.data.single_product_mobile.std,
      }

      this.optionObj.relate_product == "1"? this.showRelateProducts = true:this.showRelateProducts=false

    }


    onSingleProductLayoutChange(obj:any){
      this.singleProductLayout = obj.newValue
      this.onOptionChanged(obj);
    }

    onRelateProductChange(obj:any){
      obj.newValue =='1'?this.showRelateProducts = true:this.showRelateProducts=false
      this.onOptionChanged(obj);
      
    }

    onOptionChanged(obj: any) {    
      this.optionObj[obj.optionKey] = (obj.newValue);    
      this.changedData.emit(obj);
    }

}
