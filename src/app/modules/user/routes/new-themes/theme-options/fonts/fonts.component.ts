import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-fonts',
  templateUrl: './fonts.component.html',
  styleUrls: ['./fonts.component.scss']
})
export class FontsComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};
  styles: any;
  fontType: any;

  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      type_font_select: this.data.type_font_select,
      type_headings: this.data.type_headings,
      type_texts: this.data.type_texts,
      type_nav: this.data.type_nav,
      type_banner: this.data.type_banner,
      type_price: this.data.type_price,
      type_subset: this.data.type_subset,
      custom_font: this.data.custom_font.value || this.data.custom_font.std,
      minify_font_icons: this.data.minify_font_icons.value || this.data.minify_font_icons.std,
      include_font_awesome_new: this.data.include_font_awesome_new.value || this.data.include_font_awesome_new.std,
    }

    
    delete this.optionObj.type_font_select.options.custom;
    this.fontType = this.optionObj.type_font_select.value
  }

  onFontTypeChanged(obj: any) {
    this.fontType = obj.newValue
    this.onOptionChanged(obj)
  }

  onFontChanged(obj: any) {
    this.fontChange(obj.newValue, obj.optionKey)
    this.onOptionChanged(obj)
  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);

  }

  fontChange(selectedFont: any, optionKey: any) {

    //replace spaces with "+" sign
    var the_font = selectedFont.replace(/\s+/g, '+')
    console.log(the_font)
    const font_not = ['arial', 'verdana', 'trebuchet', 'georgia', 'times', 'tahoma', 'helvetica'];

    if (font_not.indexOf(the_font) === -1) {

      this.styles = `
    @import url('https://fonts.googleapis.com/css2?family=${the_font}&display=swap');
    #${optionKey}{
      font-family: ${selectedFont}, cursive;
  }
    `
      const node = document.createElement('style');
      node.innerHTML = this.styles;
      document.head.appendChild(node);
    }
  }
}
