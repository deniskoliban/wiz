import { Component, OnInit } from '@angular/core';
import { ThemeOptionsI } from 'src/app/models/theme-options.model';
import { ThemeLabels } from "../../../../../models/theme-labels.model";
import { ThemeService } from "../../../../../services/theme.service";
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-theme-options',
  templateUrl: './theme-options.component.html',
  styleUrls: ['./theme-options.component.scss'],

})

export class ThemeOptionsComponent implements OnInit {
  tabLabels: Array<ThemeLabels> = [];
  tabContent: any;
  selectedIndex: any = null;
  themeObj: ThemeOptionsI;
  postObj: any = {};
  imgsToBeUploadedArr: any = [];
  uploadForm: FormGroup;
  themeSettingsSub: Subscription;
  layoutDetailsSub: Subscription;
  layoutDetails: any = {};
  activeLayout: any = {};
  reset: Boolean = false;
  saveLoader: Boolean = false;
  resetLoader: Boolean = false;

  constructor(private themeService: ThemeService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.tabLabels = [
      {
        'label': 'General',
        'icon': 'theme-options-general',
        'id': 'tab-general'
      },
      {
        'label': 'Logo & Favicon',
        'icon': 'theme-options-logo',
        'id': 'tab-logo'
      },
      {
        'label': 'Header & Footer',
        'icon': 'theme-options-header',
        'id': 'tab-header'
      },
      {
        'label': 'Style & Colors',
        'icon': 'theme-options-styles',
        'id': 'tab-styles'
      },
      {
        'label': 'Fonts',
        'icon': 'theme-options-fonts',
        'id': 'tab-fonts'
      },
      {
        'label': 'Breadcrumb',
        'icon': 'theme-options-breadcrumb',
        'id': 'tab-breadcrumb'
      },
      {
        'label': 'Promotion News',
        'icon': 'theme-options-promotions',
        'id': 'tab-promotions'
      },
      {
        'label': 'Product Global',
        'icon': 'theme-options-product',
        'id': 'tab-product'
      },
      {
        'label': 'Single Product',
        'icon': 'theme-options-single-product',
        'id': 'tab-single-product'
      },
      {
        'label': 'Share & Follow',
        'icon': 'theme-options-share',
        'id': 'tab-share'
      },
    ]
    this.getTabsContentContainer(this.tabLabels[0], 0);
    this.getLayoutDetailHttp();
    this.getThemeOptionsSettings();

    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });
  }

  /** get tabs label */
  getTabsContentContainer = (tab: any, index: Number) => {
    this.selectedIndex = index;
    this.tabContent = tab.id;
  }

  /** get theme layout details */
  getLayoutDetailHttp() {
    this.layoutDetailsSub = this.themeService.getLayoutDetail().subscribe((res) => {
      this.layoutDetails = res;
      for (let item in this.layoutDetails) {
        if (this.layoutDetails[item].status == 'active') {
          this.activeLayout = this.layoutDetails[item];
        }
      }
    })
  }

  /** get theme option settings */
  getThemeOptionsSettings() {
    this.themeSettingsSub = this.themeService.getThemeOptions().subscribe((res) => {
      this.themeObj = res;
      console.log(this.themeObj);
      this.resetLoader = false;
      this.saveLoader = false;
      // save current route first
      const currentRoute = this.router.url;
      if (this.reset) {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate([currentRoute]); // navigate to same route
        });
        this.reset = false;
      }
    })
  }

  /** save media files to server if any */
  saveMediaFilesHttp = () => {
    if (this.imgsToBeUploadedArr.length > 0) {
      for (let i = 0; i < this.imgsToBeUploadedArr.length; i++) {
        let formData = new FormData();
        formData.append('media', this.imgsToBeUploadedArr[i].value);
        delete this.postObj[this.imgsToBeUploadedArr[i].optionId];
        this.themeService.uploadMedia(formData, this.imgsToBeUploadedArr[i].optionId).subscribe((response) => {
          this.saveLoader = false;
          if (i == (this.imgsToBeUploadedArr.length - 1)) {
            if (!this.isEmptyObject(this.postObj)) {
              this.saveThemeOptionsHttp();
            }
          }
        })
      }

    }
  }


  /** Save theme options settings */
  saveThemeOptionsHttp() {
    this.themeService.saveThemeOptions(this.postObj).subscribe((res) => {
      this.postObj = {};
      this.imgsToBeUploadedArr = [];
      this.getThemeOptionsSettings();

    })
  }

  /** reset all theme option settings */
  resetThemeOptionSettings = () => {
    this.reset = true;
    let copyThemeObject: any = { ...this.themeObj }
    for (let item in copyThemeObject) {
      this.postObj[item] = copyThemeObject[item].std
    }
    this.saveThemeOptionsHttp();
  }

  onDataChange(eventObj: any) {
    if (eventObj.type == 'media' && eventObj.newValue != "") {
      this.uploadForm.get('profile')!.setValue(eventObj.newValue);

      let imgObj = {
        'optionId': eventObj.optionKey,
        'value': this.uploadForm.get('profile')!.value
      }

      this.imgsToBeUploadedArr.push(imgObj);
    } else {
      this.postObj[eventObj.optionKey] = eventObj.newValue;
    }

  }

  /** on any of the action buttons clicked */
  onActionBtnClicked(event: any) {
    if (event == 'reset') {
      this.resetLoader = true;
      this.resetThemeOptionSettings();
    } else if (this.imgsToBeUploadedArr.length > 0) {
      this.saveLoader = true;
      this.saveMediaFilesHttp();
    } else {
      this.saveLoader = true;
      this.saveThemeOptionsHttp();
    }
  }

  /** check if an object is empty */
  isEmptyObject(obj: any) {
    return (obj && (Object.keys(obj).length === 0));
  }

  ngOnDestroy(): void {
    this.layoutDetailsSub.unsubscribe();
    this.themeSettingsSub.unsubscribe();
  }
}
