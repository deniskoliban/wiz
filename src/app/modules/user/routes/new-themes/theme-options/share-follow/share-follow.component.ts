import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-share-follow',
  templateUrl: './share-follow.component.html',
  styleUrls: ['./share-follow.component.scss']
})
export class ShareFollowComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};
  constructor() { }

  ngOnInit(): void {
    console.log('share and follow'+this.data)
    
      this.optionObj = {
        social_icons: this.data.social_icons,
        facebook_url_follow: this.data.facebook_url_follow.value || this.data.facebook_url_follow.std,
        vk_url_follow: this.data.vk_url_follow.value || this.data.vk_url_follow.std,
        twitter_url_follow: this.data.twitter_url_follow.value || this.data.twitter_url_follow.std,
        email_url_follow: this.data.email_url_follow.value || this.data.email_url_follow.std,
        pinterest_url_follow: this.data.pinterest_url_follow.value || this.data.pinterest_url_follow.std,
        instagram_url: this.data.instagram_url.value || this.data.instagram_url.std,
        rss_url_follow: this.data.rss_url_follow.value || this.data.rss_url_follow.std,
        linkedin_url_follow: this.data.linkedin_url_follow.value || this.data.linkedin_url_follow.std,
        youtube_url_follow: this.data.youtube_url_follow.value || this.data.youtube_url_follow.std,
        tumblr_url_follow: this.data.tumblr_url_follow.value || this.data.tumblr_url_follow.std,
        flickr_url_follow: this.data.flickr_url_follow.value || this.data.flickr_url_follow.std,
        telegram_url_follow: this.data.telegram_url_follow.value || this.data.telegram_url_follow.std,
        whatsapp_url_follow: this.data.whatsapp_url_follow.value || this.data.whatsapp_url_follow.std,
        weibo_url_follow: this.data.weibo_url_follow.value || this.data.weibo_url_follow.std,
        amazon_url_follow: this.data.amazon_url_follow.value || this.data.amazon_url_follow.std,
      }
    }

    onOptionChanged(obj: any) {    
      this.optionObj[obj.optionKey] = (obj.newValue);    
      this.changedData.emit(obj);
      console.log(obj)
    }

    
  

}
