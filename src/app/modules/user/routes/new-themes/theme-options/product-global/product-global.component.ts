import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-global',
  templateUrl: './product-global.component.html',
  styleUrls: ['./product-global.component.scss']
})
export class ProductGlobalComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};

  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      loop_layout_buttons: this.data.loop_layout_buttons,
      animated_products: this.data.animated_products,
      mobile_back_image: this.data.mobile_back_image.value || this.data.mobile_back_image.std,
      disable_cart: this.data['disable-cart'].value || this.data['disable-cart'].std,
      loop_add_to_cart: this.data.loop_add_to_cart.value || this.data.loop_add_to_cart.std,
      event_after_add_to_cart: this.data['event-after-add-to-cart'],
      mini_cart_icon: this.data['mini-cart-icon'].value || this.data['mini-cart-icon'].std,
      cart_icon_grid: this.data['cart-icon-grid'].value || this.data['cart-icon-grid'].std,
      disable_quickview: this.data['disable-quickview'].value || this.data['disable-quickview'].std,
      style_quickview: this.data.style_quickview,
      quick_view_item_thumb: this.data.quick_view_item_thumb.value || this.data.quick_view_item_thumb.std,
      style_cart: this.data['style-cart'],
      style_wishlist: this.data['style-wishlist'],
      anything_search: this.data.anything_search.value || this.data.anything_search.std,
      enable_live_search: this.data.enable_live_search.value || this.data.enable_live_search.std,
      limit_results_search: this.data.limit_results_search.value || this.data.limit_results_search.std,
      hotkeys_search: this.data.hotkeys_search.value || this.data.hotkeys_search.std,
      show_icon_cat_top: this.data.show_icon_cat_top.value || this.data.show_icon_cat_top.std,
      disable_top_level_cat: this.data.disable_top_level_cat.value || this.data.disable_top_level_cat.std,
      show_uncategorized: this.data.show_uncategorized.value || this.data.show_uncategorized.std,

    }
  }

  onQuickViewOptionChanged(obj: any){
    this.optionObj.disable_quickview = obj.newValue;
    this.onOptionChanged(obj);
  }

  onOptionChanged(obj: any) {    
    this.optionObj[obj.optionKey] = (obj.newValue); 
    this.changedData.emit(obj);
  }

  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key })
    });
    return arr;

  }

}
