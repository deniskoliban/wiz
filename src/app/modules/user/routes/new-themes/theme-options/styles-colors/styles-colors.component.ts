import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-styles-colors',
  templateUrl: './styles-colors.component.html',
  styleUrls: ['./styles-colors.component.scss']
})
export class StylesColorsComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};

  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      color_primary: this.data.color_primary.value || this.data.color_primary.std,
      color_success: this.data.color_success.value || this.data.color_success.std,
      color_hot_label: this.data.color_hot_label.value || this.data.color_hot_label.std,
      color_deal_label: this.data.color_deal_label.value || this.data.color_deal_label.std,
      color_sale_label: this.data.color_sale_label.value || this.data.color_sale_label.std,
      color_variants_label: this.data.color_variants_label.value || this.data.color_variants_label.std,
      color_price_label: this.data.color_price_label.value || this.data.color_price_label.std,
      color_button: this.data.color_button.value || this.data.color_button.std,
      color_hover: this.data.color_hover.value || this.data.color_hover.std,
      button_border_color: this.data.button_border_color.value || this.data.button_border_color.std,
      button_border_color_hover: this.data.button_border_color_hover.value || this.data.button_border_color_hover.std,
      button_text_color: this.data.site_logo.value || this.data.site_logo.std,
      button_text_color_hover: this.data.button_text_color_hover.value || this.data.button_text_color_hover.std,
      button_radius: this.data.button_radius.value || this.data.button_radius.std,
      button_border: this.data.button_border.value || this.data.button_border.std,
      input_radius: this.data.input_radius.value || this.data.input_radius.std,

    }
  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
  }

}
