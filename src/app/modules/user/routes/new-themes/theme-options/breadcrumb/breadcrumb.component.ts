import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};
  constructor() { }

  ngOnInit(): void {
    this.optionObj = {
      breadcrumb_show: this.data.breadcrumb_show.value || this.data.breadcrumb_show.std,
      breadcrumb_row: this.data.breadcrumb_row,
      breadcrumb_type: this.data.breadcrumb_type,
      breadcrumb_bg: this.data.breadcrumb_bg.value || this.data.breadcrumb_bg.std,
      breadcrumb_bg_lax: this.data.breadcrumb_bg_lax.value || this.data.breadcrumb_bg_lax.std,
      breadcrumb_bg_color: this.data.breadcrumb_bg_color.value || this.data.breadcrumb_bg_color.std,
      breadcrumb_color: this.data.breadcrumb_color.value || this.data.breadcrumb_color.std,
      breadcrumb_align: this.data.breadcrumb_align,
      breadcrumb_height: this.data.breadcrumb_height.value || this.data.breadcrumb_height.std,

    }
  }

  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key })
    });

    return arr;

  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
  }

}
