import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-form-item-label',
  templateUrl: './form-item-label.component.html',
  styleUrls: ['./form-item-label.component.scss']
})
export class FormItemLabelComponent implements OnInit {
  @Input() formItemHeading: any;
  @Input() formItemSubHeading: any;
  @Input() formItemRules: any;
  constructor() { }

  ngOnInit(): void {
  }

}
