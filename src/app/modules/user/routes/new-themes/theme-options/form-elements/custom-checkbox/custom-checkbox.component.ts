import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-checkbox',
  templateUrl: './custom-checkbox.component.html',
  styleUrls: ['./custom-checkbox.component.scss'],
})
export class CustomCheckboxComponent implements OnInit {
  @Input() settingObj: any;
  @Input() optionKey: any;
  @Output() changedOption = new EventEmitter<object>();
  checkboxArr: any = [];
  checkboxSelectedArr: any = [];
  selectedOption: any = '';
  options: any = [];
  
  constructor() { }

  ngOnInit(): void {
    console.log(this.settingObj)
    this.checkboxArr = this.getArray(this.settingObj.options);
    console.log(this.checkboxArr)
    this.checkboxSelectedArr = this.getArray(this.settingObj.value);
    console.log(this.checkboxSelectedArr)
    this.selectedOption = this.settingObj.value;


    if(this.settingObj.value == 'Array' ){
      for (let i = 0; i < this.checkboxArr.length; i++){
        this.options.push({
          label: false,
          optionVal: this.checkboxArr[i].optionVal,
        })

      }
    }else{
      for (let i = 0; i < this.checkboxSelectedArr.length; i++) {
        if (this.checkboxSelectedArr[i].label == 1) {
          console.log(this.checkboxSelectedArr)
          this.options.push({
            label: true,
            optionVal: this.checkboxSelectedArr[i].optionVal,
          });
        } else {
          this.options.push({
            label: false,
            optionVal: this.checkboxSelectedArr[i].optionVal,
          });
        }
  
        
      }

    }
    console.log(this.options)
    console.log(this.options[0].label)
    
  }

  HandleChange() {
    let val: any = {};

    for (let i = 0; i < this.options.length; i++) {
      if (this.options[i].label == true) {
        val = { ...val, [this.options[i].optionVal]: 1 };
      } else {
        val = { ...val, [this.options[i].optionVal]: 0 };
      }
    }

    let obj = {
      optionKey: this.optionKey,
      newValue: val,
    };
    this.changedOption.emit(obj);
    console.log(obj);
  }

  getArray(obj: Object) {

    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key });
    });
    console.log(arr);

    return arr;
  }

  
}
