import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { RadioImageI } from "../../../../../../../models/theme-options.model";

@Component({
  selector: 'app-radio-image',
  templateUrl: './radio-image.component.html',
  styleUrls: ['./radio-image.component.scss']
})
export class RadioImageComponent implements OnInit {
  @Input()
  settingObj!: RadioImageI;
  @Input() optionKey: string = '';
  @Output() selectedImage = new EventEmitter<object>();
  imgArr: Array<any> = [];

  constructor() { }

  ngOnInit(): void {
    this.imgArr = this.getArray(this.settingObj.options);

  }

  /** method to convert object to an array */
  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ imgSrc: value, optionVal: key })
    });

    return arr;

  }

  changeState() {    
    let obj = {
      optionKey: this.optionKey,
      newValue: this.settingObj.value
    }
    this.selectedImage.emit(obj);
  }

}
