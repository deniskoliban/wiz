import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
@Component({
  selector: 'app-date-dropdown',
  templateUrl: './date-dropdown.component.html',
  styleUrls: ['./date-dropdown.component.scss']
})
export class DateDropdownComponent implements OnInit {
  @Input() selectedDate: any;
  @Input() optionKey:any;
  @Output() changedDate = new EventEmitter<any>();

  date: any;
  selectedYear: any;
  selectedMonth: any;
  selectedDay: any;
  months: Array<any> = Array(12).fill(0).map((i, idx) => (idx + 1));

  constructor() { }

  ngOnInit(): void {

    this.date = this.selectedDate !=''? new Date(this.selectedDate):new Date()
    console.log(this.date)
    this.selectedYear = this.date.getFullYear();
    this.selectedMonth = (this.date.getMonth() + 1);
    this.selectedDay = this.date.getDate()
  }

  public get days() {
    const dayCount = this.getDaysInMonth(this.selectedYear, this.selectedMonth);
    return Array(dayCount).fill(0).map((i, idx) => idx + 1)
  }

  public getDaysInMonth(year: number, month: number) {
    return 32 - new Date(year, month - 1, 32).getDate();
  }

  public get years() {
    var currentYear = new Date().getFullYear(), years = [];
    for (let i = 0; i < 2; i++) {
      years.push(currentYear++);
    }
    return years;

  }

  dateChangeHandler(){
    console.log(this.selectedDay);
    console.log(this.selectedMonth);
    console.log(this.selectedYear);
    let day = this.selectedDay;
    let month = this.selectedMonth;
    let year = this.selectedYear
    
 let obj = {
  newValue: year+'-'+month+'-'+day,
  optionKey: this.optionKey
}
 this.changedDate.emit(obj);

    console.log(new Date(year+'-'+month+'-'+day));
    
  }

}
