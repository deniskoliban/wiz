import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  @Input() fileUrl: any;
  @Input() optionKey: any;
  @Input() max_height: number;
  @Input() max_width: number;
  @Output() selectedFile = new EventEmitter<object>();


  url: string = '';
  defaultImage: any = 'assets/default-image.svg';
  imageError: string = ''
  constructor(private auth: AuthService, public dialog: MatDialog) { }

  openDialog(imageModal: any) {
    this.dialog.open(imageModal);
  }

  closeDialog() {
    this.dialog.closeAll()
  }

  ngOnInit(): void {
    console.log(this.fileUrl);
    
    if (this.fileUrl && !this.fileUrl.includes('http')) {
      this.url = this.auth.GetURL(this.fileUrl);

    } else {
      this.url = this.fileUrl
    }


  }

  onSelectFile(event: any): void {
    this.imageError = '';
    if (event.target.files && event.target.files[0]) {
      const allowed_types = ['image/png'];

      if (!allowed_types.includes(event.target.files[0].type)) {
        this.imageError = 'Only PNG type image is allowed';

      }

      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0])
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = (rs: any): void => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          if (img_height > this.max_height || img_width > this.max_width) {
            this.imageError =
              'Maximum dimentions allowed ' +
              this.max_width +
              '*' +
              this.max_height +
              'px';

          }

          if (this.imageError == '') {
            this.url = e.target.result;
            let obj = {
              newValue: event.target.files[0],
              optionKey: this.optionKey,
              type: 'media'
            }
            this.selectedFile.emit(obj);
          }

        }
      }
    }

  }
  removeImage() {
    this.url = '';
    this.imageError = '';
    let obj = {
      newValue: this.url,
      optionKey: this.optionKey,
      type: 'media'
    }
    this.selectedFile.emit(obj);
  }

}


