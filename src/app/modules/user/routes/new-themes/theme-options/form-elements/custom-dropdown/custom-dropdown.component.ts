import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
})
export class CustomDropdownComponent implements OnInit {
  @Input() settingObj: any;
  @Input() optionKey: any;
  @Output() changedOption = new EventEmitter<object>();
  optionslabels: any = [];
  optionObj: any = {};
  defaultSelected: String = '';
  constructor() { }

  ngOnInit(): void {
    this.defaultSelected = this.settingObj.value;
    this.getOptions(this.settingObj);
  }

  getOptions(obj: any) {
    const objectArray = Object.entries(obj.options);
    objectArray.forEach(([key, value]) => {
      this.optionslabels.push({ label: value, optionVal: key });
    });
  }

  changeState() {
    let obj = {
      optionKey: this.optionKey,
      newValue: this.defaultSelected,
    };
    this.changedOption.emit(obj);
  }
}
