import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {
  @Input() rangeValue: any;
  @Input() optionKey: any;
  @Output() changedRange = new EventEmitter<any>();
  showPixelBlock:boolean = true

  inputValue: any;
  constructor() { }



  ngOnInit(): void {
    this.rangeValue = parseInt(this.rangeValue);
    this.inputValue = this.rangeValue
    this.optionKey == 'relate_product_number' ? this.showPixelBlock = false:''
  }

  onValueChange() {
    this.rangeValue = this.inputValue
    let obj = {
      optionKey: this.optionKey,
      newValue: this.showPixelBlock ? this.rangeValue + 'px' : this.rangeValue
    }
    this.changedRange.emit(obj);
  }

  rangeChangeHandler() {
    let obj = {
      optionKey: this.optionKey,
      newValue: this.showPixelBlock ? this.rangeValue + 'px' : this.rangeValue
    }
    this.inputValue = this.rangeValue

    this.changedRange.emit(obj);
  }

}
