import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
  @Input() color: any;
  @Input() optionKey: any;
  @Output() pickedColor = new EventEmitter<object>();
  defaultColor: string = '#EEE';

  constructor() { }

  ngOnInit(): void {
  }

  colorPicked(colorSelected: string) {  
    let obj = {
      optionKey: this.optionKey,
      newValue: colorSelected
    }  
    this.pickedColor.emit(obj);
  }

}
