import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss']
})
export class CustomSelectComponent implements OnInit {
  @Input() settingObj: any;
  @Input() optionKey: any;
  @Output() changedOption = new EventEmitter<object>();
  radioArr: any = [];
  selectedOption: any = '';

  constructor() { }

  ngOnInit(): void {
    console.log(this.settingObj);
    
    this.radioArr = this.getArray(this.settingObj.options);
    this.selectedOption = this.settingObj.value;
  }

  changeState(state: any) {

    let obj = {
      optionKey: this.optionKey,
      newValue: state
    }
    this.changedOption.emit(obj);
  }

  getArray(obj: Object) {
    const objectArray = Object.entries(obj);

    let arr: Array<any> = [];
    objectArray.forEach(([key, value]) => {
      arr.push({ label: value, optionVal: key })
    });

    return arr;

  }

}
