import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-buttons',
  templateUrl: './toggle-buttons.component.html',
  styleUrls: ['./toggle-buttons.component.scss']
})
export class ToggleButtonsComponent implements OnInit {
  @Input() toggleState: any;
  switchVal: any;
  @Input() optionKey: any;
  @Input() forceOfflineStatus: any;
  @Output() changedState = new EventEmitter<object>();
  disabled: any;


  constructor() { }

  ngOnInit(): void {
    if (this.forceOfflineStatus === undefined) {
      this.disabled = false;
      this.switchVal = this.toggleState
    }
    else {
      if (this.forceOfflineStatus == '0') {
        this.switchVal = this.toggleState
        this.disabled = false
      } else {
        this.switchVal = "1"
        this.disabled = true;
      }

    }
  }


  changeState() {
    let obj = {
      optionKey: this.optionKey,
      newValue: this.switchVal
    }
    this.changedState.emit(obj);
  }

}
