import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-card-settings',
  templateUrl: './card-settings.component.html',
  styleUrls: ['./card-settings.component.scss']
})
export class CardSettingsComponent implements OnInit {
  @Output() clicked = new EventEmitter<string>();
  @Input() saveLoader: Boolean;
  @Input() resetLoader: Boolean;
  
  constructor() { }

  ngOnInit(): void {
  }

  btnClickHandler(action: string){
    console.log("action", action);
    this.clicked.emit(action);
  }
}
