import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ThemeService } from '@services/theme.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})

export class GeneralComponent implements OnInit {
  @Input() data: any;
  @Output() clicked = new EventEmitter<any>();
  @Output() changedData = new EventEmitter<any>();
  optionObj: any = {};
  @Input() force_offline_status: any;
  showCommingSoon: any;
  siteLayoutSelected: String = '';
  constructor(private themeService: ThemeService) { }

  ngOnInit(): void {
    console.log("this.data", this.data);
    
    this.optionObj = {

      site_layout: this.data.site_layout,
      site_bg_color: this.data.site_bg_color.value || this.data.site_bg_color.std,
      site_bg_image: this.data.site_bg_image.value,
      hide_tini_menu_acc: this.data.hide_tini_menu_acc.value || this.data.hide_tini_menu_acc.std,
      login_ajax: this.data.login_ajax.value || this.data.login_ajax.std,
      main_screen_acc_mobile: this.data.main_screen_acc_mobile.value || this.data.main_screen_acc_mobile.std,
      register_captcha: this.data.register_captcha.value || this.data.register_captcha.std,
      effect_before_load: this.data.effect_before_load.value || this.data.effect_before_load.std,
      nasa_gdpr_notice: this.data.nasa_gdpr_notice.value || this.data.nasa_gdpr_notice.std,
      nasa_gdpr_policies: this.data.nasa_gdpr_policies.value || this.data.nasa_gdpr_policies.std,
      site_offline: this.data.site_offline.value || this.data.site_offline.std,
      site_offline_forced: this.data.site_offline_forced.value || this.data.site_offline_forced.std,
      coming_soon_title: this.data.coming_soon_title.value || this.data.coming_soon_title.std,
      coming_soon_info: this.data.coming_soon_info.value || this.data.coming_soon_info.std,
      coming_soon_img: this.data.coming_soon_img.value || this.data.coming_soon_img.std,
      coming_soon_time: this.data.coming_soon_time.value || this.data.coming_soon_time.std,

    }

    /** to show hide background color and backgroud image field */
    this.siteLayoutSelected = this.optionObj.site_layout.value;

    if (this.optionObj.site_offline_forced == '1') {
      this.showCommingSoon = false
    } else {
      if (this.optionObj.site_offline == '1') {
        this.showCommingSoon = true
      } else {
        this.showCommingSoon = false
      }
    }

  }


  onOfflineStatusChanged(obj: any) {
    if (obj.newValue == "0") {
      this.showCommingSoon = false
    } else {
      this.showCommingSoon = true
    }
    this.onOptionChanged(obj)

  }



  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
    console.log(obj)
  }

  onSiteLayoutChanged(obj: any) {
    this.siteLayoutSelected = obj.newValue;
    this.onOptionChanged(obj);
  }
}
