import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-logo-favicon',
  templateUrl: './logo-favicon.component.html',
  styleUrls: ['./logo-favicon.component.scss']
})
export class LogoFaviconComponent implements OnInit {
  @Input() data: any;
  @Output() changedData = new EventEmitter<any>();

  optionObj: any = {};
  constructor() { }

  ngOnInit(): void {
    this.optionObj = {

      site_logo: this.data.site_logo.value || this.data.site_logo.std,
      site_logo_retina: this.data.site_logo_retina.value || this.data.site_logo_retina.std,
      max_height_logo: this.data.max_height_logo.value || this.data.max_height_logo.std,
      max_height_mobile_logo: this.data.max_height_mobile_logo.value || this.data.max_height_mobile_logo.std,
      max_height_sticky_logo: this.data.max_height_sticky_logo.value || this.data.max_height_sticky_logo.std,
      site_favicon: this.data.site_favicon.value || this.data.site_favicon.std,
    }
  }

  onOptionChanged(obj: any) {
    this.optionObj[obj.optionKey] = (obj.newValue);
    this.changedData.emit(obj);
  }

}
