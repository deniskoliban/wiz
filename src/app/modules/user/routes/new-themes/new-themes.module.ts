import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NewThemesRoutingModule } from './new-themes-routing.module';
import { CommonModule } from '@angular/common';
import { NewThemesComponent } from './new-themes.component';
import { ThemeOptionsComponent } from "./theme-options/theme-options.component";
import { TranslateModule } from '@ngx-translate/core';
import { GeneralComponent } from './theme-options/general/general.component';
import { LogoFaviconComponent } from './theme-options/logo-favicon/logo-favicon.component';
import { HeaderFooterComponent } from './theme-options/header-footer/header-footer.component';
import { StylesColorsComponent } from './theme-options/styles-colors/styles-colors.component';
import { FontsComponent } from './theme-options/fonts/fonts.component';
import { BreadcrumbComponent } from './theme-options/breadcrumb/breadcrumb.component';
import { PromotionsNewsComponent } from './theme-options/promotions-news/promotions-news.component';
import { ProductGlobalComponent } from './theme-options/product-global/product-global.component';
import { SingleProductComponent } from './theme-options/single-product/single-product.component';
import { ShareFollowComponent } from './theme-options/share-follow/share-follow.component';
import { CardSettingsComponent } from './theme-options/card-settings/card-settings.component';
import { FormItemLabelComponent } from './theme-options/form-item-label/form-item-label.component';
import { ColorPickerComponent } from './theme-options/form-elements/color-picker/color-picker.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { FileUploadComponent } from './theme-options/form-elements/file-upload/file-upload.component';
import { ToggleButtonsComponent } from './theme-options/form-elements/toggle-buttons/toggle-buttons.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DateDropdownComponent } from './theme-options/form-elements/date-dropdown/date-dropdown.component';
import { RangeSliderComponent } from './theme-options/form-elements/range-slider/range-slider.component';
import { RadioImageComponent } from './theme-options/form-elements/radio-image/radio-image.component';
import { CustomSelectComponent } from './theme-options/form-elements/custom-select/custom-select.component';
import { CustomCheckboxComponent } from './theme-options/form-elements/custom-checkbox/custom-checkbox.component';
import { environment } from '../../../../../environments/environment';
import { CustomDropdownComponent } from './theme-options/form-elements/custom-dropdown/custom-dropdown.component';
// Configs
import { translationConfig } from '../../../../translation/translation.config';
@NgModule({
  declarations: [NewThemesComponent, ThemeOptionsComponent, GeneralComponent, LogoFaviconComponent, HeaderFooterComponent, StylesColorsComponent, FontsComponent, BreadcrumbComponent, PromotionsNewsComponent, ProductGlobalComponent, SingleProductComponent, ShareFollowComponent, CardSettingsComponent, FormItemLabelComponent, ColorPickerComponent, FileUploadComponent, ToggleButtonsComponent, DateDropdownComponent, RangeSliderComponent, RadioImageComponent, CustomSelectComponent, CustomCheckboxComponent, CustomDropdownComponent],
  imports: [
    CommonModule,
    NewThemesRoutingModule,
    TranslateModule,
    ColorPickerModule,
    FormsModule,
    NgbModule
  ],
})
export class NewThemesModule { }
