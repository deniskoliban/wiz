import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewThemesComponent } from './new-themes.component';
import { ThemeOptionsComponent } from './theme-options/theme-options.component';

const routes: Routes = [
  {
    path: '',
    component: NewThemesComponent,
    children: [
      {
        path: '',
        component: ThemeOptionsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewThemesRoutingModule {
}
