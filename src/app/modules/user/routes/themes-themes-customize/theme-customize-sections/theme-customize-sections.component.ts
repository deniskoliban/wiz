import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Section, ThemeCustomizationService } from '@services/theme-customization.service';

@Component({
  selector: 'app-theme-customize-sections',
  templateUrl: './theme-customize-sections.component.html',
  styleUrls: ['./theme-customize-sections.component.scss']
})
export class ThemeCustomizeSectionsComponent implements OnInit, AfterViewInit {
  @Input() currentLayout: string;
  currentSection: string;
  sections: Section[];
  previousIndex: number;

  constructor(private themeCustomization: ThemeCustomizationService) { }

  ngOnInit(): void {
    this.sections = this.themeCustomization.themes[this.currentLayout].sections;
  }

  ngAfterViewInit(): void {
    this.selectSection(0);
  }

  selectSection(index: number): void {
    this.currentSection = this.sections[index].section;
    this.themeCustomization.currentSection.next(this.sections[index]);
    this.highlightSection(index);
  }

  highlightSection(index: number): void {
    if ((this.previousIndex || this.previousIndex === 0) && this.previousIndex !== index) {
      const previousSection = document.querySelector(`#sectionItem${this.previousIndex}`);
      if (previousSection) {
        previousSection.classList.remove('sections-list-option-highlight');
      }
    }
    const selectedSection = document.querySelector(`#sectionItem${index}`);
    if (selectedSection) {
      selectedSection.classList.add('sections-list-option-highlight');
    }
    this.previousIndex = index;
  }




}
