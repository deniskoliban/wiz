import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subheading, ThemeCustomizationService } from '@services/theme-customization.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

export interface FormControlGroup {
  [str: string]: FormControl;
}

@Component({
  selector: 'app-section-customize-subheading',
  templateUrl: './section-customize-subheading.component.html',
  styleUrls: ['./section-customize-subheading.component.scss']
})
export class SectionCustomizeSubheadingComponent implements OnInit, OnDestroy {
  @Input() indexOfSubheading: number;
  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  fileNames: string[] = [];
  currentSubscription: Subscription;
  subheadings: Subheading[] | undefined;
  subheadingForm: FormGroup;
  subheadingFormControls: FormControlGroup = {};

  constructor(
    public themeCustomization: ThemeCustomizationService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.currentSubscription = this.themeCustomization.currentSection.subscribe((section) => {
      this.subheadings = section?.subheadings;
      if (!this.subheadingForm) {
        this.initSubheadingFormControls();
        this.subheadingForm = this.fb.group(this.subheadingFormControls);
      }
    });
  }

  initSubheadingFormControls(): void {
    if (this.subheadings && this.subheadings[this.indexOfSubheading].subheading) {
      this.subheadings[this.indexOfSubheading].subheading.map(
        (subheading) => {
          const camelWord = this.themeCustomization.toCamelCaseString(subheading.topCategories);
          this.subheadingFormControls[camelWord] = new FormControl({
            value: subheading.value,
            disabled: !!subheading.image,
          });
        }
      );
    }
  }

  fileChangeEvent(fileInput: Event, imgElement: HTMLImageElement, topCategories: string, inputIndex: number): void {
    const target: HTMLInputElement = fileInput.target as HTMLInputElement;
    if (target.files?.length) {
      this.fileNames[inputIndex] = target.files[0].name;
      const camelWord = this.themeCustomization.toCamelCaseString(topCategories);
      this.subheadingFormControls[camelWord].setValue(this.fileNames);

      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = () => imgElement.src = e.target.result;
      };
      reader.readAsDataURL(target.files[0]);

      this.uploadFileInput.nativeElement.value = '';
    } else {
      this.fileNames[inputIndex] = '';
    }
  }

  ngOnDestroy(): void {
    if (this.currentSubscription) {
      this.currentSubscription.unsubscribe();
    }
  }
}
