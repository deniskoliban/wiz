import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { InputBlock, ThemeCustomizationService } from '@services/theme-customization.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FormControlGroup } from '@src/app/modules/user/routes/themes-themes-customize/theme-customize-sections/theme-customize-section/section-customize-subheading/section-customize-subheading.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-section-customize-heading',
  templateUrl: './section-customize-heading.component.html',
  styleUrls: ['./section-customize-heading.component.scss']
})
export class SectionCustomizeHeadingComponent implements OnInit, OnDestroy {
  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  fileNames: string[] = [];
  currentSubscription: Subscription;
  heading: InputBlock[] | undefined;
  headingForm: FormGroup;
  headingFormControls: FormControlGroup = {};
  constructor(public themeCustomization: ThemeCustomizationService, private fb: FormBuilder ) { }

  ngOnInit(): void {
    this.themeCustomization.currentSection.subscribe((section) => {
      this.heading = section?.heading;
      this.initHeadingFormControls();
      this.headingForm = this.fb.group(this.headingFormControls);
    });
  }

  initHeadingFormControls(): void {
    if (this.heading) {
      this.heading.map(
        (heading) => {
          const camelWord = this.themeCustomization.toCamelCaseString(heading.topCategories);
          this.headingFormControls[camelWord] = new FormControl({
            value: heading.value,
            disabled: !!heading.image,
          });
        }
      );
    }
  }


  fileChangeEvent(fileInput: Event, imgElement: HTMLImageElement, topCategories: string, inputIndex: number): void {
    const target: HTMLInputElement = fileInput.target as HTMLInputElement;
    if (target.files?.length) {
      this.fileNames[inputIndex] = target.files[0].name;
      const camelWord = this.themeCustomization.toCamelCaseString(topCategories);
      this.headingFormControls[camelWord].setValue(this.fileNames);

      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = () => imgElement.src = e.target.result;
      };
      reader.readAsDataURL(target.files[0]);

      this.uploadFileInput.nativeElement.value = '';
    } else {
      this.fileNames[inputIndex] = '';
    }
  }


  ngOnDestroy(): void {
    if (this.currentSubscription) {
      this.currentSubscription.unsubscribe();
    }
  }
}
