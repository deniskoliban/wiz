import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Section, ThemeCustomizationService } from '@services/theme-customization.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-theme-customize-section',
  templateUrl: './theme-customize-section.component.html',
  styleUrls: ['./theme-customize-section.component.scss']
})
export class ThemeCustomizeSectionComponent implements OnInit, OnDestroy {
  currentSubscription: Subscription;
  sectionName: string | undefined;
  currentSection: Section | undefined;

  constructor(
    private themeCustomization: ThemeCustomizationService,
    private cd: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.currentSubscription = this.themeCustomization.currentSection.subscribe((section) => {
      this.currentSection = section;
      this.sectionName = section?.section;
      this.cd.detectChanges();
    });
  }

  ngOnDestroy(): void {
    if (this.currentSubscription) {
      this.currentSubscription.unsubscribe();
    }
  }

}
