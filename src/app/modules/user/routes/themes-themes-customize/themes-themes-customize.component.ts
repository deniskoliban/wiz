import { Component, OnInit } from '@angular/core';
import { environment } from '@src/environments/environment';
import { DomSanitizer , SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ThemeCustomizationService } from '@services/theme-customization.service';

@Component({
  selector: 'app-themes-themes-customize',
  templateUrl: './themes-themes-customize.component.html',
  styleUrls: ['./themes-themes-customize.component.scss']
})
export class ThemesThemesCustomizeComponent implements OnInit {
  layoutTitle: string;
  layout: string;
  url: SafeResourceUrl | undefined;
  constructor(
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private themeCustomization: ThemeCustomizationService,
    ) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wiz_merchant_customize`);
    this.layout = this.activatedRoute.snapshot.params['layout'];
    this.layoutTitle = this.themeCustomization.themes[this.layout].title;
  }

}
