import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-settings-general-settings',
  templateUrl: './settings-general-settings.component.html',
  styleUrls: ['./settings-general-settings.component.scss']
})
export class SettingsGeneralSettingsComponent implements OnInit {

  url: SafeResourceUrl| undefined;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wiz_merchant_settings`);
  }

}
