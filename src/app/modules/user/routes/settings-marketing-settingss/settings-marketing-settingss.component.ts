import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-settings-marketing-settingss',
  templateUrl: './settings-marketing-settingss.component.html',
  styleUrls: ['./settings-marketing-settingss.component.scss']
})
export class SettingsMarketingSettingssComponent implements OnInit {

  url: SafeResourceUrl | undefined;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wiz_merchant_marketing_settings`);
  }

}
