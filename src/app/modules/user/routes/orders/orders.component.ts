import { Component, OnInit } from '@angular/core';
import {MetaData, Orders} from '../../../../models/orders.model';
import { OrdersService } from '../../../../services/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  // variables
  emptyOrders = false;
  currentActivePage = 0;
  isNextPageEmpty = false;
  leftArrowLoader = false;
  searchText: any;
  switchArray = false;
  isPlural = false;
  pageNumber = 1;
  tableErrLayer = false;
  // containers
  nameColors = [
    {
      color: '#EA5B29',
      bg: '#F9CDBE'
    },
    {
      color: '#229FFF',
      bg: '#EBF6FF'
    },
    {
      color: '#00823D',
      bg: '#EBF5EF'
    },
    {
      color: '#FFD55C',
      bg: '#FFF2CE'
    },
    {
      color: '#BB3EE1',
      bg: '#FAE3FF'
    },
    {
      color: '#EA5B29',
      bg: '#F9CDBE'
    },
    {
      color: '#229FFF',
      bg: '#EBF6FF'
    },
    {
      color: '#00823D',
      bg: '#EBF5EF'
    },
    {
      color: '#FFD55C',
      bg: '#FFF2CE'
    },
    {
      color: '#BB3EE1',
      bg: '#FAE3FF'
    }
  ];
  orderList = ['', '', '', '', '', '', '', ''];
  pageNumbers = [
    {number: 1, active: true},
    {number: 2, active: false},
    {number: 3, active: false},
  ];
  ordersContainer!: Orders[] | any;
  searchedContainer!: Orders[] | any;
  wizShippingContainer: MetaData[] = [];
  wizPaymentsContainer: MetaData[] = [];

  constructor(private orders: OrdersService) {
  }

  ngOnInit(): void {
    this.getOrdersList(1);
  }

  getShippingOrPaymentValue(order: Orders, type: string): string {
    return order.meta_data?.filter(meta => meta?.key === type)[0]?.value || '-' as string;
  }

  getOrdersList(page: number): void {
    this.pageNumber = page;
    this.orders.getOrders(page, 5).subscribe(res => {
      this.ordersContainer = res;
      this.leftArrowLoader = false;
      console.log(this.ordersContainer);
      if (res.length > 1) {
        this.isPlural = true;
      }
      if (res.length >= 1) {
        this.isNextPageEmpty = false;
      } else {
        this.isNextPageEmpty = true;
        this.pageNumber--;
        this.getOrdersList(this.pageNumber);
      }

    }, err => this.tableErrLayer = true);
  }

  sortOrderList(event: any): void {
    this.orders.getSortedOrders(event.target.value, 5).subscribe(res => {
      this.ordersContainer = res;
    });
  }
  searchById(): void {
    if (this.searchText.trim().length > 0) {
      // this.searchedContainer = this.ordersContainer.filter(orderId => String(orderId.id).includes(this.searchText));
      this.orders.searchInOrders(this.searchText).subscribe(res => {
        this.searchedContainer = res;
        console.log(this.searchedContainer);
      });

      this.switchArray = true;
      console.log(this.searchedContainer);
    } else {
      this.switchArray = false;
      console.log('empty');
    }
  }

  goNext(): void {
    this.isNextPageEmpty = true;
    this.pageNumber++;
    // this.pageNumbers.forEach(value => {
    //   value.number += 3;
    // });
    this.getOrdersList(this.pageNumber);
  }

  goPrevious(): void {
    this.leftArrowLoader = true;
    this.pageNumber--;
    // this.pageNumbers.map(value => value.number -= 3);
    this.getOrdersList(this.pageNumber);
  }

  getPage(page: number): void {
    console.log(page);
    this.getOrdersList(page);
  }

  sortOrdersAscending(): void {
    const aDate = this.ordersContainer.sort((a: any, b: any) => {
      const keyA = new Date(a.date_created);
      const keyB = new Date(b.date_created);
      // Compare the 2 dates
      if (keyA < keyB) {
        return -1;
      }
      if (keyA > keyB) {
        return 1;
      }
      return 0;
    });

    console.log(aDate);
  }

  sortOrdersDeciding(): void {
    const dDate = this.ordersContainer.sort((a: any, b: any) => {
      const keyA = new Date(a.date_created);
      const keyB = new Date(b.date_created);
      // Compare the 2 dates
      if (keyA < keyB) {
        return 1;
      }
      if (keyA > keyB) {
        return -1;
      }
      return 0;
    });

    console.log(dDate);
  }

  sortLowToHighAmount(): void {
    const sortLowHigh = this.ordersContainer.sort((a: any, b: any) => a - b);
    console.log(sortLowHigh);
  }

  sortHighToLowAmount(): void {
    const sortHighLow = this.ordersContainer.sort((a: any, b: any) => b - a );
    console.log(sortHighLow);
  }

  activePage(index: any): void {
    this.pageNumbers.forEach(page => {
      if (page.active) {
        page.active = false;
      }
    });
    this.pageNumbers[index].active = true;
    this.currentActivePage = index;
  }

  activeNextPage(): void {
    this.pageNumbers.forEach((page, index) => {
      if (page.active) {
        this.currentActivePage = index;
        page.active = false;
      }
    });
    this.currentActivePage++;
    this.pageNumbers[this.currentActivePage].active = true;
    console.log(this.currentActivePage);
  }

  activePreviousPage(): void {
    this.pageNumbers.forEach((page, index) => {
      if (page.active) {
        this.currentActivePage = index;
        page.active = false;
      }
    });
    this.currentActivePage--;
    this.pageNumbers[this.currentActivePage].active = true;
    console.log(this.currentActivePage);
  }

  trackByFn(index: number, item: any): number {
    return index;
  }
}
