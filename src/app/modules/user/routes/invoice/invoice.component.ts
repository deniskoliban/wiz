import { Component, OnInit } from '@angular/core';
import { AwbService } from '../../../../services/awb.service';
import { ActivatedRoute } from '@angular/router';
import { Invoice } from '../../../../models/model';

export interface MerchantInfo {
  name?: string;
  city?: string;
  phone?: string;
  address?: string;
}

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  invoiceId: string | any = '';
  invoiceData!: Invoice;
  awbValue: string | any;

  merchantInfo: MerchantInfo = {
    address: '',
    city: '',
    name: '',
    phone: ''
  };

  constructor(private awbService: AwbService, private activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(res => this.invoiceId = res.id);
  }

  ngOnInit(): void {
    this.getInvoiceData();
  }

  getInvoiceData(): void {
    this.awbService.getInvoiceForPrinting(this.invoiceId).subscribe(res => {
      this.invoiceData = res;
      this.awbValue = this.invoiceData.meta_data?.filter(meta => meta.key === 'awb_number')[0]?.value || '';
      console.log(this.invoiceData);
      this.getMerchantInfo();
    }, err => {
      console.error(err);
    });
  }

  getMerchantInfo(): void {
    this.awbService.getMerchantInfo().subscribe(res => {
      this.merchantInfo = res;
      console.log(res);
      this.print();
    }, err => console.log(err));
  }

  print(): void {
    const printBtn = document.getElementById('btn-print') || HTMLElement;
    setTimeout(() => {
      if ('click' in printBtn) {
        printBtn.click();
      }
    }, 100);
  }

}
