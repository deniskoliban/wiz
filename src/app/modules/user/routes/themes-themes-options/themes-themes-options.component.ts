import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-themes-themes-options',
  templateUrl: './themes-themes-options.component.html',
  styleUrls: ['./themes-themes-options.component.scss']
})
export class ThemesThemesOptionsComponent implements OnInit {
  url: SafeResourceUrl | undefined;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/themes.php?page=optionsframework`);
  }

}
