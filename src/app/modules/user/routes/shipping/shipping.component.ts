import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { google } from 'google-maps';
import { CountryISO, PhoneNumberFormat, SearchCountryField, TooltipLabel } from 'ngx-intl-tel-input';
import { City, CountriesService, Country, State } from '@services/countries.service';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { Product } from '@services/products.service';
import { Observable } from 'rxjs';
import AutocompleteOptions = google.maps.places.AutocompleteOptions;
import PlaceResult = google.maps.places.PlaceResult;
import GeocoderResult = google.maps.GeocoderResult;
import { ShippingService } from '@services/shipping.service';
import some from 'lodash-es/some';

enum Shipping {
  FREE = 'free',
  CONDITIONAL_FREE = 'conditional_free',
  FLAT_RATE = 'flat_rate'
}

enum Delivery {
  WIZ = 'wiz',
  OWN = 'own'
}

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit, OnDestroy {

  private autoComplete: ElementRef;
  private map: ElementRef;

  @ViewChild('autoComplete', {static: false}) set autoCompleteContent(content: ElementRef) {
    if (content) {
      this.autoComplete = content;
      this.initGoogleAutocomplete();
    }
  }

  @ViewChild('map', {static: false}) set mapContent(content: ElementRef) {
    if (content) {
      this.map = content;
      this.initGoogleMap();
    }
  }

  countriesList: Array<Country> = [];
  citiesList: Array<City> = [];
  areasList: Array<State> = [];

  autoCompleteInstance: google.maps.places.Autocomplete;
  autoCompleteListener: google.maps.MapsEventListener;

  mapInstance: google.maps.Map;

  form: FormGroup;
  submitted = false;

  currentStep = 1;
  totalVisibleSteps = 4;
  totalSteps = 5;

  productsControl: FormControl;
  deliveryControl: FormControl;

  countryControl: FormControl;
  cityControl: FormControl;
  areaControl: FormControl;
  streetControl: FormControl;
  building: FormControl;
  locationTypeControl: FormControl;
  daysControl: FormControl;
  timeFrom: FormControl;
  timeTo: FormControl;
  phone: FormControl;
  searchControl: FormControl;

  shippingControl: FormControl;
  minOrderControl: FormControl;
  flatRateControl: FormControl;
  flatRateBaseControl: FormControl;
  shippingRateControl: FormControl;

  conditionalsGroup: FormGroup;

  daysList = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  mapMarkers: Array<google.maps.Marker> = [];

  center: google.maps.LatLngLiteral | google.maps.LatLng = {lat: 26.8357675, lng: 30.7956597};

  mapOptions: google.maps.MapOptions = {
    zoom: 5,
    center: this.center,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  };

  googlePlaceOptions: AutocompleteOptions = {
    types: ['geocode'],
    componentRestrictions: {country: 'EG'}
  };

  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: Array<CountryISO> = [CountryISO.Egypt, CountryISO.UnitedArabEmirates];
  onlyCountries: Array<CountryISO> = [CountryISO.Egypt, CountryISO.UnitedArabEmirates, CountryISO.SaudiArabia];

  notWizProductsAllowed: boolean;
  notWizShippingAllowed: boolean;

  constructor(
    private fb: FormBuilder,
    private countries: CountriesService,
    private dialog: MatDialog,
    private router: Router,
    private shipping: ShippingService
  ) {
    this.form = this.fb.group({
      step1: this.fb.group({
        products: [undefined, Validators.required]
      }),
      step2: this.fb.group({
        delivery: [undefined, Validators.required]
      }),
      step3: this.fb.group({
        country: [undefined, Validators.required],
        city: [undefined, Validators.required],
        area: [undefined, Validators.required],
        street: [undefined, Validators.required],
        building: [undefined, Validators.required],
        location_type: [undefined, Validators.required],
        days: [undefined, Validators.required],
        timeFrom: [undefined, Validators.required],
        timeTo: [undefined, Validators.required],
        phone: [undefined, Validators.required],
        search: [undefined]
      }),
      step4: this.fb.group({
        shipping: [undefined, Validators.required]
      }),
      step5: this.fb.group({
        minOrder: [],
        flatRate: [],
        flatRateBase: [],
        shippingRate: []
      })
    });
    this.productsControl = (this.form.get('step1') as FormGroup).get('products') as FormControl;

    this.deliveryControl = (this.form.get('step2') as FormGroup).get('delivery') as FormControl;

    this.countryControl = (this.form.get('step3') as FormGroup).get('country') as FormControl;
    this.cityControl = (this.form.get('step3') as FormGroup).get('city') as FormControl;
    this.areaControl = (this.form.get('step3') as FormGroup).get('area') as FormControl;
    this.streetControl = (this.form.get('step3') as FormGroup).get('street') as FormControl;
    this.building = (this.form.get('step3') as FormGroup).get('building') as FormControl;
    this.locationTypeControl = (this.form.get('step3') as FormGroup).get('location_type') as FormControl;
    this.daysControl = (this.form.get('step3') as FormGroup).get('days') as FormControl;
    this.timeFrom = (this.form.get('step3') as FormGroup).get('timeFrom') as FormControl;

    this.timeTo = (this.form.get('step3') as FormGroup).get('timeTo') as FormControl;
    this.phone = (this.form.get('step3') as FormGroup).get('phone') as FormControl;
    this.searchControl = (this.form.get('step3') as FormGroup).get('search') as FormControl;

    this.shippingControl = (this.form.get('step4') as FormGroup).get('shipping') as FormControl;

    this.conditionalsGroup = this.form.get('step5') as FormGroup;
    this.minOrderControl = this.conditionalsGroup.get('minOrder') as FormControl;
    this.flatRateControl = this.conditionalsGroup.get('flatRate') as FormControl;
    this.flatRateBaseControl = this.conditionalsGroup.get('flatRateBase') as FormControl;
    this.shippingRateControl = this.conditionalsGroup.get('shippingRate') as FormControl;
  }

  // MUHAMMED VARIABLES, OBJECTS AND CONTAINERS
  categoriesContainer: Array<string> = [];
  disableWizRadio = false;

  ngOnInit(): void {
    window.addEventListener('click', () => {
      console.log(this.searchControl);
    });
    this.countries.getCountries()
      .subscribe(res => this.countriesList = res);
    this.countryControl.valueChanges
      .subscribe(value => {
        this.areasList = value.states;
        this.autoCompleteInstance.setComponentRestrictions({country: value.iso2});
      });
    this.areaControl.valueChanges
      .subscribe(value => {
        this.citiesList = value.cities;
      });

  }

  ngOnDestroy(): void {
    if (this.autoCompleteListener) {
      this.autoCompleteListener.remove();
    }
  }

  // MUHAMMED START

  displaySelectedItems(event: any): void {
    this.categoriesContainer = event;
    console.log(this.categoriesContainer, 'FROM SHIPPING LISTENER');
  }

  postItems(): void {
    this.shipping.postSelectedProducts({categories: this.categoriesContainer}).subscribe(res => {
      console.log(res, 'CATEGORIES RES');
    });
  }

  // MUHAMMED END
  initGoogleAutocomplete(): void {
    this.autoCompleteInstance = new google.maps.places.Autocomplete(this.autoComplete.nativeElement, this.googlePlaceOptions);
    this.autoCompleteListener = google.maps.event.addListener(this.autoCompleteInstance, 'place_changed', (places: PlaceResult) => {
      const place: PlaceResult = places ? places : this.autoCompleteInstance.getPlace();
      this.searchControl.setValue(place.geometry && place.geometry.location.toJSON());
      this.searchControl.updateValueAndValidity();
      this.setMarker(place);
    });
  }

  initGoogleMap(): void {
    this.mapInstance = new google.maps.Map(this.map.nativeElement, this.mapOptions);
    this.mapInstance.panTo(this.center);
  }

  removeDay(name: string): void {
    const alteredArray = this.daysControl.value.filter((day: string) => {
      return day !== name;
    });
    this.daysControl.setValue(alteredArray);
  }

  isCurrentStepValid(): boolean {
    const group = this.form.get(`step${this.currentStep}`);
    return group?.invalid as boolean;
  }

  nextStep(): void {
    const filterProducts = (): Array<Product> => {
      return this.productsControl.value.filter((p: Product) => !p.shipping_available);
    };
    if (this.currentStep < this.totalSteps) {
      const group = this.form.get(`step${this.currentStep}`);
      if (group?.valid) {
        if (this.currentStep === 1) {
            // this.notWizProductsAllowed = false;
            if (some(this.productsControl.value, item => !item.shipping_available)) {
              this.openDialog().subscribe(res => {
                if (res) {
                  this.postItems();
                  this.disableWizRadio = true;
                  this.currentStep++;
                }
              });
            } else {
              this.postItems();
              this.disableWizRadio = false;
              this.currentStep++;
            }
        }
        if (this.currentStep === 2 && !this.notWizShippingAllowed) {
          const productsFiltered = filterProducts();
          if (productsFiltered.length && this.deliveryControl.value === Delivery.WIZ) {
            this.notWizShippingAllowed = false;
            this.currentStep++;
            return;
          }
        }
        if (this.currentStep === 3) {
          this.autoCompleteListener.remove();
        }
        if (this.currentStep === 4) {
          this.updateControlsValidity();
          this.currentStep++;
        }
      }
    }
  }

  backStep(): void {
    if (this.currentStep > 1) {
      this.currentStep--;
    }
  }

  formSubmit(): void {
    this.submitted = true;
    if (this.form.valid) {
      if (this.shippingControl.value === Shipping.FREE) {
        this.openDialog('free').subscribe(() => this.redirectToSuccess());
      } else {
        this.redirectToSuccess();
      }
    }
  }

  redirectToSuccess(): void {
    this.router.navigate(['user', 'shipping', 'success']).then();
  }

  protected updateControlsValidity(): void {
    this.minOrderControl.setValidators([]);
    this.flatRateControl.setValidators([]);
    this.flatRateBaseControl.setValidators([]);
    this.shippingRateControl.setValidators([]);
    switch (this.shippingControl.value) {
      case (Shipping.FREE):
        this.minOrderControl.setValidators([Validators.required]);
        this.flatRateControl.setValidators([Validators.required]);
        break;
      case (Shipping.CONDITIONAL_FREE):
        break;
      case (Shipping.FLAT_RATE):
      default:
        this.flatRateBaseControl.setValidators([Validators.required]);
        this.shippingRateControl.setValidators([Validators.required]);
        break;
    }
    this.minOrderControl.updateValueAndValidity();
    this.flatRateControl.updateValueAndValidity();
    this.flatRateBaseControl.updateValueAndValidity();
    this.shippingRateControl.updateValueAndValidity();
  }

  moveMap(event: any): void {
    this.center = event.latLng;
    const geocoder = new google.maps.Geocoder();
    const params: google.maps.GeocoderRequest = {};
    if (event.hasOwnProperty('placeId')) {
      params.placeId = (event as google.maps.IconMouseEvent).placeId;
    } else {
      params.location = event.latLng.toJSON();
    }
    geocoder.geocode(params, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        this.autoComplete.nativeElement.value = results[0].formatted_address;
        this.searchControl.setValue(results[0].formatted_address);
        this.searchControl.updateValueAndValidity();
        this.setMarker(results[0]);
      }
    });
  }

  setMarker(result: PlaceResult | GeocoderResult): void {
    if (!result || !result.geometry) {
      return;
    }
    this.mapMarkers.forEach(m => m.setMap(null));
    this.center = (result.geometry.location.toJSON());
    const marker = new google.maps.Marker({
      position: result.geometry.location,
      draggable: true,
      clickable: false
    });
    marker.setMap(this.mapInstance);
    marker.addListener('dragend', res => this.moveMap(res));
    this.mapMarkers = [];
    this.mapMarkers.push(marker);
    this.mapInstance.panTo(result.geometry.location);
    this.mapInstance.setZoom(16);

    this.streetControl.setValue(result.formatted_address);
    this.streetControl.updateValueAndValidity();
  }

  openDialog(template = 'alert'): Observable<boolean> {
    const dialogRef = this.dialog.open(AlertDialogComponent, {data: {template}});

    return dialogRef.afterClosed();
  }

}
