import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShippingComponent } from './shipping.component';
import { ShippingRoutingModule } from './shipping-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TimeRageInputComponent } from './components/time-rage-input/time-rage-input.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { StepProductsComponent } from './components/step-products/step-products.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { ShippingSuccessComponent } from './components/shipping-success/shipping-success.component';
import { ConditionalFreeFormComponent } from './components/conditional-free-form/conditional-free-form.component';

@NgModule({
  declarations: [
    ShippingComponent,
    TimeRageInputComponent,
    StepProductsComponent,
    AlertDialogComponent,
    ShippingSuccessComponent,
    ConditionalFreeFormComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ShippingRoutingModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatChipsModule,
    MatExpansionModule,
    TimepickerModule.forRoot(),
    NgxIntlTelInputModule,
    MatIconModule,
    MatDialogModule
  ],
  exports: [
    TimeRageInputComponent
  ]
})
export class ShippingModule { }
