import { Component } from '@angular/core';

@Component({
  selector: 'app-shipping-success',
  templateUrl: './shipping-success.component.html',
  styleUrls: ['./shipping-success.component.scss']
})
export class ShippingSuccessComponent {}
