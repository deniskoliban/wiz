import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-time-rage-input',
  templateUrl: './time-rage-input.component.html',
  styleUrls: ['./time-rage-input.component.scss']
})
export class TimeRageInputComponent implements OnInit, OnChanges {

  @Input() from: FormControl;
  @Input() to: FormControl;
  @Input() startTimeArr: Array<number>;
  @Input() endTimeArr: Array<number>;
  @Output() startDateChanged = new EventEmitter<Date>();
  @Output() endDateChanged = new EventEmitter<Date>();
  timeFromLiteral = '';
  timeToLiteral = '';

  static formatAMPM(date: Date): string {
    if (!date) {
      return '';
    }
    let hours = date.getHours();
    const minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    const minutesStr = minutes < 10 ? '0' + minutes : minutes;

    return hours + ':' + minutesStr + ' ' + ampm;
  }

  ngOnInit(): void {
    this.from.updateValueAndValidity();
    this.to.updateValueAndValidity();
    // this.updateLiterals();
    // this.from.valueChanges.subscribe(() => this.updateLiterals());
    // this.to.valueChanges.subscribe(() => this.updateLiterals());
    this.from.setValue(this.getDefaultTime(this.startTimeArr[0], this.startTimeArr[1]));
    this.to.setValue(this.getDefaultTime(this.endTimeArr[0], this.endTimeArr[1]));

  }

  ngOnChanges(): void {
    this.from.setValue(this.getDefaultTime(this.startTimeArr[0], this.startTimeArr[1]));
    this.to.setValue(this.getDefaultTime(this.endTimeArr[0], this.endTimeArr[1]));
    this.from.valueChanges.subscribe(() => {
      this.startDateChanged.emit(this.from.value);
      this.updateLiterals();
    });
    this.to.valueChanges.subscribe(() => {
      this.endDateChanged.emit(this.to.value);
      this.updateLiterals();
    });
  }

  getDefaultTime(hours: number, minutes: number): Date {
    const today = new Date();
    today.setHours(hours);
    today.setMinutes(minutes);

    return today;
  }

  updateLiterals(): void {
    if (this.from && this.from.valid) {
      this.timeFromLiteral = TimeRageInputComponent.formatAMPM(this.from.value);
    }
    if (this.to && this.to.valid) {
      this.timeToLiteral = TimeRageInputComponent.formatAMPM(this.to.value);
    }
  }

}
