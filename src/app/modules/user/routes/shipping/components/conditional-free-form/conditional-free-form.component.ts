import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConditionalFreeShipping } from '@src/app/models/shipping';

@Component({
  selector: 'app-conditional-free-form',
  templateUrl: './conditional-free-form.component.html',
  styleUrls: ['./conditional-free-form.component.scss']
})
export class ConditionalFreeFormComponent implements OnInit {

  @Input() formGroup: FormGroup;
  conditionsArray: FormArray;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    if (!this.formGroup) {
      return;
    }
    this.formGroup.addControl('conditions', this.fb.array([]));
    this.conditionsArray = this.formGroup.get('conditions') as FormArray;
    this.addConditional();
  }

  addConditional(withValues: ConditionalFreeShipping = {}): void {
    this.conditionsArray.push(this.createConditional(withValues));
    this.updateConditionalValidators();
  }

  removeConditional(i: number): void {
    if (this.conditionsArray.length > 1) {
      this.conditionsArray.removeAt(i);
      this.updateConditionalValidators();
    }
  }

  createConditional(withValues: ConditionalFreeShipping = {}): FormGroup {
    const group = this.fb.group({
      from_order_value: [withValues['from_order_value'], Validators.required],
      to_order_value: [withValues['to_order_value'], Validators.required],
      percentage_rate: [withValues['percentage_rate'], Validators.required]
    });
    const rangeMinControl = group.get('from_order_value');
    rangeMinControl?.valueChanges.subscribe(() => this.updateConditionalValidators());
    const rangeMaxControl = group.get('to_order_value');
    rangeMaxControl?.valueChanges.subscribe(() => this.updateConditionalValidators());

    return group;
  }

  updateConditionalValidators(): void {
    if (!this.conditionsArray.length) {
      return;
    }

    const getMinRangeValue = (j: number): number => {
      const compareGroup = this.conditionsArray.controls[j];
      return compareGroup && parseInt((compareGroup.get('to_order_value') as FormControl).value || 0, 10);
    };

    const generateValidators = (required: boolean, min?: number, max?: number) => {
      const v = [];
      if (required) {
        v.push(Validators.required);
      }
      if (min !== undefined) {
        v.push(Validators.min(min));
      }
      if (max !== undefined) {
        v.push(Validators.max(max));
      }

      return v;
    };

    let i;
    for (i = 0; i < this.conditionsArray.length; i++) {
      const group = this.conditionsArray.controls[i];
      const fromControl = group.get('from_order_value') as FormControl;
      const toControl = group.get('to_order_value') as FormControl;
      const percentageControl = group.get('percentage_rate') as FormControl;

      // Set validators
      const minValuePrevious = getMinRangeValue(i - 1) + 1;
      if (i === this.conditionsArray.length - 1) {
        toControl.setValidators(generateValidators(true));
        if (toControl.invalid) {
          toControl.setValue(-1, {emitEvent: false});
        }
        fromControl.setValidators(generateValidators(true));
      } else {
        if (toControl.value === -1) {
          toControl.reset();
        }
        if (i === 0) {
          toControl.setValidators(generateValidators(true));
          fromControl.setValidators(generateValidators(true, undefined, parseInt(toControl.value, 10) - 1));
        } else {
          toControl.setValidators(generateValidators(true, Math.max(minValuePrevious, parseInt(fromControl.value, 10) + 1)));
        }
      }
      if (i > 0) {
        fromControl.setValidators(generateValidators(true, minValuePrevious));
      }

      percentageControl.setValidators([Validators.required]);

      // Update value and validity
      toControl.updateValueAndValidity({emitEvent: false});
      fromControl.updateValueAndValidity({emitEvent: false});
      percentageControl.updateValueAndValidity();
    }
  }

}
