import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ShippingService } from '@services/shipping.service';
import { Product, ProductsService } from '@services/products.service';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-step-products',
  templateUrl: './step-products.component.html',
  styleUrls: ['./step-products.component.scss']
})
export class StepProductsComponent implements OnInit {
  ownShippingSupported = true;
  itemList: Array<Product> = [];

  @Input() control: FormControl;
  @Output() selectedItems = new EventEmitter<Array<string>>();

  constructor(
    protected shipping: ShippingService,
    protected products: ProductsService
  ) { }

  ngOnInit(): void {
    this.getSelectedProducts();
  }

  getSelectedProducts(): void {
    this.products.getProducts()
      .pipe(
        map(res => this.ownShippingSupported ? res : res.filter(item => item.shipping_available)),
        switchMap(products => {
          this.itemList = products;

          return this.shipping.getShopProducts();
        })
      )
      .subscribe(products => {
        const selectedItemsInternal = [];
        for (const product of products) {
          for (const item of this.itemList) {
            if (product === item.name) {
              selectedItemsInternal.push(item);
            }
          }
        }
        this.control.setValue(selectedItemsInternal);
        this.control.updateValueAndValidity();
        this.emitUpdates();
      });
  }

  removeProduct(name: string): void {
    const alteredArray = this.control.value.filter((product: Product) => {
      return product.name !== name;
    });
    this.control.setValue(alteredArray);
    this.emitUpdates();
  }

  emitUpdates(): void {
    this.selectedItems.emit(this.control.value);
    console.log(this.control.value, 'EMIT UPDATES');
  }
}
