import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShippingComponent } from './shipping.component';
import { ShippingSuccessComponent } from './components/shipping-success/shipping-success.component';

const routes: Routes = [
  {
    path: '',
    component: ShippingComponent
  },
  {
    path: 'success',
    component: ShippingSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingRoutingModule {
}
