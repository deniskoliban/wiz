import {Component, OnInit} from '@angular/core';
import {ChartDataSets, ChartType} from 'chart.js';
import {Colors, Label} from 'ng2-charts';
import {HomeService} from '../../../../services/home.service';
import {ActivityBoard, DateSelection, LineItems, Orders} from '../../../../models/orders.model';
import {IDictionary, WizardData} from '../../../../models/home';
import {environment} from '../../../../../environments/environment';
import {CookieService} from 'ngx-cookie';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {SuccessAlertComponent} from '../../components/success-alert/success-alert.component';
import {ConfirmOrderAlertComponent} from '../../components/confirm-order-alert/confirm-order-alert.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Line Chart for Store Visitors
  public visitorsLabels = [''];
  public visitorsData: ChartDataSets[] = [
    {data: [], label: 'Store Visitors'}
  ];
  public lineColors: Colors[] = [
    {
      backgroundColor: 'transparent',
      borderColor: '#000'
    }
  ];
  public lineChartType: ChartType = 'line';

  // Bar Chart for best selling
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barColors: Colors[] = [
    {
      backgroundColor: '#CEE4EC',
      borderColor: 'transparent'
    }
  ];
  public barChartData: ChartDataSets[] = [
    {data: [], label: 'Best Selling'}
  ];

  // Bar Chart for best views
  public barChartViewsLabels: Label[] = [''];
  public barChartViewsType: ChartType = 'bar';
  public barViewsColors: Colors[] = [
    {
      backgroundColor: '#CEE4EC',
      borderColor: 'transparent'
    }
  ];
  public barChartViewsData: ChartDataSets[] = [
    {data: [], label: 'Top Viewed'}
  ];

  // variables
  totalVisitorsNumber: any;
  currency$: any;
  abandonValue: any;
  shopUrl = '';
  isOrdersEmpty: boolean | undefined;
  wizardLoaded = false;
  qty: number | any = 0;
  // error layer variables
  wizardErr = false;
  revenueErr = false;
  visitorsErr = false;
  ordersErr = false;
  visitorsStateErr = false;
  bestSellingErr = false;
  pendingOrdersErr = false;
  activityBoardErr = false;
  totalProductsErr = false;
  // container and objects


  visitorsContainer: any[] = [];
  nameColors = [
    {
      color: '#EA5B29',
      bg: '#F9CDBE'
    },
    {
      color: '#229FFF',
      bg: '#EBF6FF'
    },
    {
      color: '#00823D',
      bg: '#EBF5EF'
    },
    {
      color: '#FFD55C',
      bg: '#FFF2CE'
    },
    {
      color: '#BB3EE1',
      bg: '#FAE3FF'
    },
    {
      color: '#EA5B29',
      bg: '#F9CDBE'
    },
    {
      color: '#229FFF',
      bg: '#EBF6FF'
    },
    {
      color: '#00823D',
      bg: '#EBF5EF'
    },
    {
      color: '#FFD55C',
      bg: '#FFF2CE'
    },
    {
      color: '#BB3EE1',
      bg: '#FAE3FF'
    }
  ];
  pendingOrders: Orders[] = [];

  totalOrders = 0;
  totalRevenue = 0;
  totalItems = 0;
  totalVisitors = 0;
  pendingStatus = [
    'processing',
    'pending',
    'on-hold'
  ];
  orders: any = {
    abounded: null,
    pending: null,
    delivered: null,
    cancelled: null
  };
  wizardData: IDictionary = {} as IDictionary;
  verifying = false;
  activityBoardObj: ActivityBoard = {};


  range = new FormGroup({
    start_date: new FormControl(new Date(new Date().setDate(new Date().getDate() - 2)).toISOString().slice(0, 10), [Validators.required]),
    end_Date: new FormControl(new Date().toISOString().slice(0, 10), [Validators.required])
  });

  dateDate: any = {
    start_date: '',
    end_date: ''
  };

  constructor(private homeService: HomeService, private cookies: CookieService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getTotalRevenue();
    this.getTotalProducts();
    this.getPendingOrders();
    this.getBestSellingGraph();
    this.getOrdersStatus();
    this.getVisitorStatistics();
    this.getWizardData();
    this.getTheCurrency();
    this.getAbandon();
    this.getActivityBoard();
    this.dateChanged();
    this.redirectToYourShop();
  }

// *********** MUHAMMED START ************** //

  dateChanged(): void {
    this.dateDate = {
      start_date: this.range?.controls?.start_date?.value?.toLocaleString()?.split(',')[0]?.replace(/[/]/g, '-'),
      end_date: this.range?.controls?.end_Date?.value?.toLocaleString()?.split(',')[0]?.replace(/[/]/g, '-')
    };
    console.log(this.dateDate, 'date range');
    if (this.dateDate?.start_date && this.dateDate?.end_date) {
      this.homeService.filterChartByDate(this.dateDate.start_date, this.dateDate.end_date).subscribe((res) => {
        console.log(this.dateDate, '');
        console.log(res, 'res vis');
        this.visitorsLabels = res.map(it => it.created_at);
        this.visitorsData[0].data = res.map(it => Number(it.count_date_visitor));
        console.log(this.visitorsData[0].data, this.visitorsLabels, 'data');
      }, err => {
        console.log(err);
        this.visitorsStateErr = true;
      });
      // this.homeService.showViewsDataByDate(this.dateDate.start_date, this.dateDate.end_Date).subscribe(res => {
      //   this.barChartViewsLabels = res.map(it => it.created_at);
      //   this.barChartViewsData[0].data = res.map(it => Number(it.count_product));
      // }, err => console.log('Something'));
    }
  }

  getBestSellingGraph(): void {
    this.homeService.getBestSellingGraph().subscribe(res => {
      console.log(res, 'BestSelling');
      this.barChartLabels = res.map((barLabel: ChartTitle) => barLabel.name);
      console.log(this.barChartLabels, 'labels of best!!!!!!!!!!!');
      this.barChartData[0].data = res.map((chartQty: ChartQuantity) => chartQty.quantity);
    });
  }

  getVisitorStatistics(): void {
    this.homeService.getVisitorsData().subscribe((res) => {
      this.visitorsContainer = res.map((visitor: any) => ({
        title: Object.keys(visitor)[0].split('_').join(' '),
        value: Object.values(visitor)[0]
      })).flat();
      res.forEach((visitor: any, index: number) => {
        if (Object.keys(visitor)[0] === 'total_visitors') {
          console.log(this.visitorsContainer[index].value);
          this.totalVisitorsNumber = this.visitorsContainer[index].value;
        }
      });
      console.log(this.visitorsContainer, 'VISITORS');
    }, err => {
      this.visitorsErr = true;
    });
  }

  redirectToYourShop(): void {
    if (environment.production) {
      this.shopUrl = this.cookies.get('WIZ_API').toString();
    } else {
      this.shopUrl = environment.apiWithUrl;
    }
  }

  getTheCurrency(): void {
    this.homeService.getCurrentCurrency().subscribe(res => {
      this.currency$ = res.currency;
      console.log(this.currency$);
    });
  }

  getAbandon(): void {
    this.homeService.getAbandonedValue().subscribe(res => {
      console.log(res[0].count_abandoned_orders, 'abandoooooon');
      this.abandonValue = res[0].count_abandoned_orders;
    });
  }

  getActivityBoard(): void {
    this.homeService.getActivityBoardData().subscribe(res => {
      this.activityBoardObj = res;
      console.log(this.activityBoardObj, 'Activity');
    }, err => {
      this.activityBoardErr = true;
    });
  }

// *********** MUHAMMED END ************** //
  getTotalRevenue(): void {
    this.homeService.getTotalRevenue().subscribe(res => {
      console.log(res);
      this.totalRevenue = res[0].total_sales;
      // this.totalItems = res[0].total_items;
      this.totalOrders = res[0].total_orders;
    }, err => {
      this.revenueErr = true;
    });
  }

  getTotalProducts(): void {
    this.homeService.getTotalProducts().subscribe(res => {
      console.log(res, 'total products');
      this.totalItems = res
        .filter(data => (data.slug !== 'grouped' && data.slug !== 'external' && data.slug !== 'variable'))
        .map(it => it.total).reduce((prev: any = 0, curr: any = 0) => prev + curr, 0);
    }, err => {
      console.log(err);
      this.totalProductsErr = true;
    });
  }

  getPendingOrders(): void {
    this.homeService.getPendingOrders().subscribe(res => {
      this.pendingOrders = res
        .filter(it => it.status === 'pending' ||
          it.status === 'processing' ||
          it.status === 'on-hold');
      if (this.pendingOrders.length > 0) {
        this.isOrdersEmpty = false;
      } else {
        this.isOrdersEmpty = true;
      }
      res.forEach(data => {
        data.line_items?.forEach(item => {
          this.qty += item.quantity;
        });
      });
      console.log(this.pendingOrders, 'pending orders');
    });
  }

  calcQty(order: Orders): number {
    const lineItemsQty = order?.line_items?.map(it => it?.quantity) || [];
    let total = 0;
    for (const qty of lineItemsQty) {
      total += qty || 0;
    }
    return total;
    // .reduce((prev: any = 0, next: any = 0) => prev + next) as number;
  }

  // zeyad
  getOrdersStatus(): void {
    this.homeService.getOrdersStatus().subscribe((res) => {
      console.log(res);
      const pendingOrders = res.filter(it => it.slug === 'pending' || it.slug === 'on-hold' || it.slug === 'processing');
      const deliveredOrders = res.filter(it => it.slug === 'completed');
      const canceledOrders = res.filter(it => it.slug === 'cancelled');
      this.orders.pending = pendingOrders.map(item => item.total).reduce((prev: any = 0, next: any = 0) => prev + next);
      this.orders.delivered = deliveredOrders.map(item => item.total).reduce((prev: any = 0, next: any = 0) => prev + next);
      this.orders.cancelled = canceledOrders.map(item => item.total).reduce((prev: any = 0, next: any = 0) => prev + next);
    }, err => {
      this.ordersErr = true;
    });

  }

  getWizardData(): void {
    this.homeService.getWizardData().subscribe((res) => {
      this.wizardData = res;
      this.wizardLoaded = true;
      console.log(res, 'wizard');
    }, err => {
      this.wizardErr = true;
      this.wizardLoaded = false;
    });
  }

  isGoLive(): boolean {
    return this.wizardData.step_7?.status === 'done';
  }

  sendVerification(status: boolean): void {
    this.verifying = true;
    if (!status) {
      this.homeService.verifyShop().subscribe(res => {
        this.openVerificationDialog();
        this.verifying = false;
      }, err => {
        console.log(err);
        this.verifying = false;
      });
    } else {
      this.verifying = false;
      window.location.href = 'https://' + this.shopUrl;
    }
  }

  openVerificationDialog(): void {
    this.dialog.open(ConfirmOrderAlertComponent, {
      width: '500px',
      data: {
        title: 'Your email is not verified. \nEmail verification has been sent to your email.',
        titleWithLink: {
          link: 'Click Resend',
          title: 'if you did not receive the email'
        }
      }
    });
  }

  // muhammed

}

interface ChartTitle {
  name?: string;
}

interface ChartQuantity {
  quantity?: number;
}
