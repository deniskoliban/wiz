import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-themes-themes-static-pages',
  templateUrl: './themes-themes-static-pages.component.html',
  styleUrls: ['./themes-themes-static-pages.component.scss']
})
export class ThemesThemesStaticPagesComponent implements OnInit {

  url: SafeResourceUrl | undefined;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/themes.php?page=optionsframework`);
  }

}
