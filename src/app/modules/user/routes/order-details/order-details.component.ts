import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Observable, Subscription} from 'rxjs';
import {LineItems, Orders, ProductImages, WizardStatus} from '../../../../models/orders.model';
import {OrdersService} from '../../../../services/orders.service';
import {ConfirmOrderAlertComponent} from '../../components/confirm-order-alert/confirm-order-alert.component';
import {RejectOrderAlertComponent} from '../../components/reject-order-alert/reject-order-alert.component';
import {PickupRequestComponent} from '../../components/pickup-request/pickup-request.component';
import {SuccessAlertComponent} from '../../components/success-alert/success-alert.component';
import {marker} from '@biesbjerg/ngx-translate-extract-marker';
import {Line} from 'tslint/lib/verify/lines';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  // variables
  imagesIdSub: Subscription | any;
  orderId: any;
  selectedItems: number | any = 0;
  allIsSelected = false;
  sendRequest = false;
  subTotal = 0;
  shippingAmount = 0;
  wizardCurrentStatus: string | undefined;
  paymentStatus: string | any;
  billingStatus: string | any;
  // containers
  wizardStatus = [
    {
      step: 1,
      status: 'rejected',
      caption: marker('Rejected Order')
    },
    {
      step: 2,
      status: 'pending',
      caption: marker('Ready To Ship')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Dispatched')
    },
    {
      step: 4,
      status: 'rejected',
      caption: marker('Out For Delivery')
    },
    {
      step: 5,
      status: 'default',
      caption: marker('Delivered')
    },
  ];
  selectionContainer = [
    {
      selected: true,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: true,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    },
    {
      selected: false,
      value: 'Item1'
    }
  ];
  rightWizardStatus = [
    {
      step: 1,
      status: 'default',
      caption: marker('Rejected Order')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Ready To Ship')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Dispatched')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Out For Delivery')
    },
  ];
  selectedContainer: LineItems[] | any = [];
  quantityArr = [];
  orderDetailsIds: number[] = [];
  imagesContainer: ProductImages[] | any = [];
  itemsIDS = [];
  itemsQuantities = [];
  differenceContainer: LineItems[] | any = [];
  // Upper Wizard Containers
  readyToShipContainer = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Ready To Ship')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Dispatched')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Out Of Delivery')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Delivered')
    },
  ];
  dispatchedContainer = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Ready To Ship')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Dispatched')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Out Of Delivery')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Delivered')
    },
  ];
  outOfDeliveryContainer = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Ready To Ship')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Dispatched')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Out Of Delivery')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Delivered')
    },
  ];
  deliveredContainer = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Ready To Ship')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Dispatched')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Out Of Delivery')
    },
    {
      step: 4,
      status: 'confirmed',
      caption: marker('Delivered')
    },
  ];
  wizardPendingContainer = [
    {
      step: 1,
      status: 'default',
      caption: marker('Ready To Ship')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Dispatched')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Out Of Delivery')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Delivered')
    },
  ];

  // Lower Wizard Container
  wizardPaidOnline = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Paid Online')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];

  wizardCollectionPending = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  wizardCashCourier = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  wizardMerchantPayment = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  wizardMerchantPaid = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  wizardMerchantPaidPending = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  wizardConfirmed = [
    {
      step: 1,
      status: 'confirmed',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'confirmed',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'confirmed',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'confirmed',
      caption: marker('Merchant Paid')
    },
  ];

  wizardNotReady = [
    {
      step: 1,
      status: 'default',
      caption: marker('Collection\n Pending')
    },
    {
      step: 2,
      status: 'default',
      caption: marker('Cash Collected\n By Courier')
    },
    {
      step: 3,
      status: 'default',
      caption: marker('Merchant Payment\n Pending')
    },
    {
      step: 4,
      status: 'default',
      caption: marker('Merchant Paid')
    },
  ];
  // Objects


  constructor(public orders: OrdersService, private activated: ActivatedRoute, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getOrderId();
    this.getOrderDetailsData();
  }

  ngOnDestroy(): void {
    this.imagesIdSub?.unsubscribe();
  }


  confirmThisOrder(): void {
    if (this.selectedItems < 1) {
      this.openConfirmAlertDialog();
    } else {
      this.sendRequest = true;
      this.differenceContainer = this.orders.orderDetailsObj.line_items?.filter((x: LineItems) => !this.selectedContainer.includes(x));

      // PARTIAL ACCEPTANCE --> IF USER NOT SELECT ALL ITEMS
      if (this.selectedContainer.length !== this.orders.orderDetailsObj.line_items?.length) {
          const unSelectedItems = this.differenceContainer.map((data: LineItems) => ({id: data.id, quantity: 0}));
          console.log(unSelectedItems, 'UNSELECTED');
          const metaData = [{key: 'wiz_partial', value: true}, {key: 'order_accepted_items', value: this.selectedContainer.length}];
          this.orders.confirmOrder(this.orderId, {status: 'confirmed', line_items: unSelectedItems, meta_data: metaData}).subscribe(res => {
            this.openSuccessAlert();
            this.orders.orderDetailsObj = res;
            console.log(this.orders.orderDetailsObj);
            this.sendRequest = false;
          }, err => this.sendRequest = false);
      }

      // COMPLETE ACCEPTANCE --> IF USER SELECT ALL ITEMS
      if (this.selectedContainer.length === this.orders.orderDetailsObj.line_items?.length) {
          this.orders.confirmOrder(this.orderId, {status: 'confirmed'}).subscribe(res => {
            this.openSuccessAlert();
            this.orders.orderDetailsObj = res;
            console.log(this.orders.orderDetailsObj);
            this.sendRequest = false;
          }, err => this.sendRequest = false);
      }
      //
      // console.log(this.orders.orderDetailsObj.line_items, 'LINE ITEMS');
      // console.log(this.selectedContainer, 'SELECTED ITEMS');
      // console.log(this.differenceContainer, 'NOT SELECTED ITEMS');
    }


  }

  rejectThisOrder(): void {
    this.orderId.rejectOrder(this.orderId, {status: 'cancelled'}).subscribe((res: any) => {
      console.log(res);
    });
  }

  printAWBData(): void {
    this.orders.printAWB(this.orderId).subscribe(res => {
      console.log(res, 'AWB');
    });
  }

  getOrderDetailsData(): void {
    this.orders.getOrderDetails(this.orderId).subscribe((res) => {
      this.orders.orderDetailsObj = res;
      console.log(this.orders.orderDetailsObj);
      this.quantityArr = res.line_items.map((item: any) => Array.from(Array(item.quantity).keys()).map(count => count + 1)); // [[2], [3]]
      res.line_items.forEach((item: any) => item.selected = false);
      this.calculateSubTotal();
      this.calculateShippingAmount();
      this.orderDetailsIds = res.line_items.map((ids: any) => ids.product_id);
      console.log(this.orderDetailsIds, 'IDS');
      this.getProductImages(this.orderDetailsIds);
      console.log(this.quantityArr, 'qty');
      res.meta_data.forEach((data: any) => {
        if (data.key === 'wiz_shipping_status') {
          this.wizardCurrentStatus = data.value;
          console.log(this.wizardCurrentStatus, 'Wizard Status');
        }
        if (data.key === 'wiz_payment_status') {
          this.paymentStatus = data.value;
          console.log(this.paymentStatus, 'payment status');
        }
        if (data.key === 'wiz_merchant_billing_status') {
          this.billingStatus = data.value;
          console.log(this.billingStatus, 'Billing status');
        }
      });
      this.bellowWizardStatus();

    });
  }

  bellowWizardStatus(): WizardStatus[] | any {
      if (this.orders?.orderDetailsObj?.status !== 'rejected' && this.orders?.orderDetailsObj?.status !== 'failed' && this.orders?.orderDetailsObj?.status !== 'cancelled') {
        if (this.paymentStatus === 'pending') {
          return this.wizardCollectionPending;
        }
        if (this.paymentStatus === 'paid') {
          if (this.billingStatus === 'paid') {
              return this.wizardConfirmed;
          } else {
            return this.wizardMerchantPaidPending;
          }
        }
      } else {
        return this.wizardNotReady;
      }
  }

  getProductImages(imagesContainer: any[]): void {
    this.imagesIdSub = this.orders.getImagesByIDs(imagesContainer).subscribe((res: ProductImages[]) => {
      this.imagesContainer = res;
      console.log(this.imagesContainer, 'IMAGES');
    });
  }

  calculateSubTotal(): void {
    this.orders?.orderDetailsObj?.line_items?.forEach(item => {
      this.subTotal += (Number(item.total));
    });
  }

  calculateShippingAmount(): void {
    this.shippingAmount = (Number(this.orders?.orderDetailsObj?.total) - Number(this.subTotal) - Number(this.orders?.orderDetailsObj?.discount_total));
  }

  // ********* start alerts *********** //

  openDialog(): void {
    this.dialog.open(DialogComponent);
    // setTimeout(() => {
    //   this.dialog.closeAll();
    // }, 2000);
  }


  openConfirmAlertDialog(): void {
    this.dialog.open(ConfirmOrderAlertComponent, {data: {title: 'Please select the items you want to confirm to continue'}});
  }

  openPickUpAlert(): void {
    this.dialog.open(PickupRequestComponent);
  }

  openRejectionAlertDialog(): void {
    this.dialog.open(RejectOrderAlertComponent, {data: {orderId: this.orderId}, disableClose: true});
  }

  openSuccessAlert(): void {
    this.dialog.open(SuccessAlertComponent, {data: {orderId: this.orderId}});
  }

  // ********* end alerts *********** //
  toggleSelection(item: LineItems, index: number): void {
    if (item.selected) {
      item.selected = false;
      this.selectedItems--;
    } else {
      item.selected = true;
      this.checkSelectedItems();
    }
    console.log(item.selected);
    this.fillSelectedItems(item);
  }

  fillSelectedItems(item: LineItems): void {
    const itemIndex = this.selectedContainer.indexOf(item);
    console.log(itemIndex);
    itemIndex < 0 ? this.selectedContainer.push(item) : this.selectedContainer.splice(itemIndex, 1);
    console.log(this.selectedContainer);
    console.log(this.selectedContainer.map((data: any) => data.id), this.selectedContainer.map((data: any) => data.quantity));
  }

  checkSelectedItems(): void {
    console.log('Select work!');
    this.selectedItems = 0;
    this.orders?.orderDetailsObj?.line_items?.forEach((item: LineItems, index: any) => {
      if (item.selected === true) {
        this.selectedItems++;
      }
    });
  }

  selectAllItems(): void {

    this.allIsSelected = !this.allIsSelected;
    if (this.allIsSelected) {
      this.orders?.orderDetailsObj?.line_items?.forEach(item => {
        item.selected = true;
      });
      this.selectedItems = this.orders?.orderDetailsObj?.line_items?.length;
      this.selectedContainer = [...this.orders?.orderDetailsObj?.line_items || ''];
    } else {
      this.selectedItems = 0;
      this.orders?.orderDetailsObj?.line_items?.forEach(item => {
        item.selected = false;
      });
      this.selectedContainer =  [];
    }
  }

  getOrderId(): void {
    this.activated.params.subscribe(res => {
      this.orderId = res.id;
    });
  }

}

@Component({
  selector: 'app-dialog',
  templateUrl: '../../components/dialog.html',
  styleUrls: ['../../components/dialog.scss']
})
export class DialogComponent {
}
