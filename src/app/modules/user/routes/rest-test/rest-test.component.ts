import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { WizconfigService } from 'src/app/wizconfig.service';
import { CookieService } from 'ngx-cookie';
@Component({
  selector: 'app-rest-test',
  templateUrl: './rest-test.component.html',
  styleUrls: ['./rest-test.component.scss']
})
export class RestTestComponent implements OnInit {
  orders: any;

  constructor(private wizConfig: WizconfigService) { }

  ngOnInit(): void {
    this.wizConfig.getListOrders().subscribe(this.showOrders);
  }

  // tslint:disable-next-line: typedef
  showOrders(data: any){
    this.orders = data;
    console.log(this.orders);
  }

}
