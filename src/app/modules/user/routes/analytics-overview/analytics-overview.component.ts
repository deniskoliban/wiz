import { Component, OnInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-analytics-overview',
  templateUrl: './analytics-overview.component.html',
  styleUrls: ['./analytics-overview.component.scss']
})
export class AnalyticsOverviewComponent implements OnInit {
  url: SafeResourceUrl | undefined;

  constructor(private sanitizer: DomSanitizer, private auth: AuthService) { }

  ngOnInit(): void {
    this.url = this.auth.GetIframeURL('/wp-admin/admin.php?page=wc-admin&path=/analytics/overview');
  }

}
