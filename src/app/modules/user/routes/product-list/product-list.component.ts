import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  url: SafeResourceUrl | undefined;

  constructor(private sanitizer: DomSanitizer, private auth: AuthService) { }

  ngOnInit(): void {
    const redirectUrl = encodeURI('/wp-admin/edit.php?frame=yes&post_type=product');
    this.auth.Authorize();
    this.url =
      this.sanitizer.bypassSecurityTrustResourceUrl(`${this.auth.api}jsauth?jwt=${this.auth.jwt}&redirect=${redirectUrl}`);
    console.log(this.sanitizer.bypassSecurityTrustResourceUrl(`${this.auth.api}jsauth?jwt=${this.auth.jwt}&redirect=${redirectUrl}`));
    console.log(this.sanitizer.bypassSecurityTrustUrl(`${this.auth.api}jsauth?jwt=${this.auth.jwt}&redirect=${redirectUrl}`));
  }

}
