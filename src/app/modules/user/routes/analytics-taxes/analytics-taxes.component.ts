import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-analytics-taxes',
  templateUrl: './analytics-taxes.component.html',
  styleUrls: ['./analytics-taxes.component.scss']
})
export class AnalyticsTaxesComponent implements OnInit {
  url: SafeResourceUrl | undefined;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wc-admin&path=/Fanalytics/taxes`);
  }

}
