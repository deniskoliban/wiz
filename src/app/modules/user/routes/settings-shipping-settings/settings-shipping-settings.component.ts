import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-settings-shipping-settings',
  templateUrl: './settings-shipping-settings.component.html',
  styleUrls: ['./settings-shipping-settings.component.scss']
})
export class SettingsShippingSettingsComponent implements OnInit {

  url: SafeResourceUrl | undefined;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wiz_merchant_marketing_settings`);
  }

}
