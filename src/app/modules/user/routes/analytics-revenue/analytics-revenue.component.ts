import { Component, OnInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-analytics-revenue',
  templateUrl: './analytics-revenue.component.html',
  styleUrls: ['./analytics-revenue.component.scss']
})
export class AnalyticsRevenueComponent implements OnInit {

  url: SafeResourceUrl | undefined;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wc-admin&path=/Fanalytics/revenue`);
  }

}
