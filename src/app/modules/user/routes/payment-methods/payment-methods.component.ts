import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivateWizPayComponent } from '@src/app/modules/user/components/activate-wiz-pay/activate-wiz-pay.component';
import { PaymentsService } from '@services/payments.service';
import { Subscription } from 'rxjs';

interface Payments {
  cod_enabled?: string;
  ccod_enabled?: string;
}

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: ['./payment-methods.component.scss']
})
export class PaymentMethodsComponent implements OnInit, OnDestroy {
  // variables
  methodsSub: Subscription;
  // objects
  paymentsObj: Payments = {
    cod_enabled: '',
    ccod_enabled: ''
  };

  constructor(public dialog: MatDialog, private payment: PaymentsService) {
  }

  ngOnInit(): void {
    this.getCurrentPaymentMethods();
  }

  ngOnDestroy(): void {
    this.methodsSub.unsubscribe();
  }

  getCurrentPaymentMethods(): void {
   this.methodsSub =  this.payment.getPaymentMethods().subscribe(res => {
      this.paymentsObj = res;
      console.log(this.paymentsObj, 'PAYMENTS');
    });
  }

  toggleCod(): void {
    if (this.paymentsObj.cod_enabled === 'no') {
      this.paymentsObj.cod_enabled = 'yes';
      this.setPaymentMethod();
    } else {
      this.paymentsObj.cod_enabled = 'no';
      this.setPaymentMethod();
    }
    console.log(this.paymentsObj);
  }

  toggleCcod(): void {
    if (this.paymentsObj.ccod_enabled === 'no') {
      this.paymentsObj.ccod_enabled = 'yes';
      this.setPaymentMethod();
    } else {
      this.paymentsObj.ccod_enabled = 'no';
      this.setPaymentMethod();
    }
    console.log(this.paymentsObj);
  }

  setPaymentMethod(): void {
    this.payment.setPaymentMethod({...this.paymentsObj}).subscribe(res => {
      console.log(res, 'POST');
    });
  }

  openPayAlert(): void {
    this.dialog.open(ActivateWizPayComponent, {
      data: {
        head: 'Activate Wiz Pay',
        title: 'Please insert your Wiz Pay setup information to be able to activate this method'
      }
    });
  }


}
