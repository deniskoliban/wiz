import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-analytics-coupons',
  templateUrl: './analytics-coupons.component.html',
  styleUrls: ['./analytics-coupons.component.scss']
})
export class AnalyticsCouponsComponent implements OnInit {
  url: SafeResourceUrl | undefined

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`${environment.apiWithUrl}auth?jwt=${environment.jwt}&redirect=/wp-admin/admin.php?page=wc-admin&path=/Fanalytics/coupons`);
  }

}
