import { Component, OnInit } from '@angular/core';
import { ThemeService } from '@services/theme.service';
import { Themes } from '@src/app/models/thems.model';


@Component({
  selector: 'app-themes-layouts',
  templateUrl: './themes-layouts.component.html',
  styleUrls: ['./themes-layouts.component.scss']
})
export class ThemesLayoutsComponent implements OnInit {
  // variables
  activeRequest = false;
  // containers
  themesContainer: Themes[] = [];
  constructor(private themeService: ThemeService) { }

  ngOnInit(): void {
    this.displayThemes();
  }

  displayThemes(): void {
    this.themeService.getThemes().subscribe(res => {
      const newData = Object.entries(res).map((obj: any) => ({themeName: obj[0], data: obj[1]}));
      this.themesContainer = newData;
      this.themesContainer.forEach((theme: any) => {
        theme.data.activeRequest = false;
      });
    });
  }

  themeActivation(theme: Themes | any): void {
    theme.data.activeRequest = true;
    this.themeService.activeCurrentTheme({layout_id: theme?.data?.id}).subscribe(res => {
      theme.data.activeRequest = false;
      this.activeRequest = false;
      this.displayThemes();
    });
  }

  trackByFn(index: number, item: any): number {
    return index;
  }
}
