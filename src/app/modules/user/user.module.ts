import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { HomeComponent } from './routes/home/home.component';
import { OrdersComponent } from './routes/orders/orders.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { _MatMenuDirectivesModule, MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { ActivityBoardComponent } from './components/activity-board/activity-board.component';
import { DialogComponent, OrderDetailsComponent } from './routes/order-details/order-details.component';
import { ChartsModule } from 'ng2-charts';
import { ProductsComponent } from './routes/products/products.component';
import { CustomersComponent } from './routes/customers/customers.component';
import { AnalyticsComponent } from './routes/analytics/analytics.component';
import { MarketingComponent } from './routes/marketing/marketing.component';
import { DiscountComponent } from './routes/discount/discount.component';
import { ThemesComponent } from './routes/themes/themes.component';
import { SettingsComponent } from './routes/settings/settings.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CookieModule } from 'ngx-cookie';
import { ProductListComponent } from './routes/product-list/product-list.component';
import { ProductCategoriesComponent } from './routes/product-categories/product-categories.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { MatButtonModule } from '@angular/material/button';
import { InvoiceComponent } from './routes/invoice/invoice.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnalyticsOrdersComponent } from './routes/analytics-orders/analytics-orders.component';
import { AnalyticsProductsComponent } from './routes/analytics-products/analytics-products.component';
import { AnalyticsCategoriesComponent } from './routes/analytics-categories/analytics-categories.component';
import { AnalyticsCouponsComponent } from './routes/analytics-coupons/analytics-coupons.component';
import { AnalyticsTaxesComponent } from './routes/analytics-taxes/analytics-taxes.component';
import { AnalyticsDownloadsComponent } from './routes/analytics-downloads/analytics-downloads.component';
import { AnalyticsStockComponent } from './routes/analytics-stock/analytics-stock.component';
import { ThemesLayoutsComponent } from './routes/themes-layouts/themes-layouts.component';
import { ThemesThemesOptionsComponent } from './routes/themes-themes-options/themes-themes-options.component';
import { ThemesThemesStaticPagesComponent } from './routes/themes-themes-static-pages/themes-themes-static-pages.component';
import { ThemesThemesFaqsComponent } from './routes/themes-themes-faqs/themes-themes-faqs.component';
import { ThemesThemesCustomizeComponent } from './routes/themes-themes-customize/themes-themes-customize.component';
import { SettingsGeneralSettingsComponent } from './routes/settings-general-settings/settings-general-settings.component';
import { SettingsPaymentSettingsComponent } from './routes/settings-payment-settings/settings-payment-settings.component';
import { SettingsMarketingSettingssComponent } from './routes/settings-marketing-settingss/settings-marketing-settingss.component';
import { SettingsShippingSettingsComponent } from './routes/settings-shipping-settings/settings-shipping-settings.component';
import { AnalyticsOverviewComponent } from './routes/analytics-overview/analytics-overview.component';
import { AnalyticsRevenueComponent } from './routes/analytics-revenue/analytics-revenue.component';
import { NewProductComponent } from './routes/new-product/new-product.component';
import { TagsComponent } from './routes/tags/tags.component';
import { AttributesComponent } from './routes/attributes/attributes.component';
import { ConfirmOrderAlertComponent } from './components/confirm-order-alert/confirm-order-alert.component';
import { RejectOrderAlertComponent } from './components/reject-order-alert/reject-order-alert.component';
import { PickupRequestComponent } from './components/pickup-request/pickup-request.component';
import { SuccessAlertComponent } from './components/success-alert/success-alert.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { MatIconModule } from '@angular/material/icon';
import { LanguageSwitcherComponent } from './components/language-switcher/language-switcher.component';
import { ErrorLayerComponent } from './components/error-layer/error-layer.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPrintModule } from 'ngx-print';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NewThemesModule } from './routes/new-themes/new-themes.module';
import { CommingSoonComponent } from './components/comming-soon/comming-soon.component';
import { PaymentMethodsComponent } from './routes/payment-methods/payment-methods.component';
import { ActivateWizPayComponent } from './components/activate-wiz-pay/activate-wiz-pay.component';
import { WizpayFormComponent } from './routes/wizpay-form/wizpay-form.component';
import { MatSelectModule } from '@angular/material/select';
import { OwnConditionalFreeShippingComponent } from './sub-modules/shipping-setup/components/own-conditional-free-shipping/own-conditional-free-shipping.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { ThemeCustomizeSectionsComponent } from './routes/themes-themes-customize/theme-customize-sections/theme-customize-sections.component';
import { MatListModule } from '@angular/material/list';
import { ThemeCustomizeSectionComponent } from './routes/themes-themes-customize/theme-customize-sections/theme-customize-section/theme-customize-section.component';
import { SectionCustomizeHeadingComponent } from './routes/themes-themes-customize/theme-customize-sections/theme-customize-section/section-customize-heading/section-customize-heading.component';
import { SectionCustomizeSubheadingComponent } from './routes/themes-themes-customize/theme-customize-sections/theme-customize-section/section-customize-subheading/section-customize-subheading.component';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { PricingComponent } from './sub-modules/shipping-setup/components/pricing/pricing.component';

@NgModule({
  declarations: [
    UserComponent,
    HomeComponent, OrdersComponent,
    ActivityBoardComponent, OrderDetailsComponent,
    ProductsComponent, CustomersComponent, AnalyticsComponent,
    MarketingComponent, DiscountComponent, ThemesComponent, SettingsComponent, ProductListComponent,
    ProductCategoriesComponent, InvoiceComponent, AnalyticsOrdersComponent,
    AnalyticsProductsComponent,
    AnalyticsCategoriesComponent,
    AnalyticsCouponsComponent,
    AnalyticsTaxesComponent,
    AnalyticsDownloadsComponent,
    AnalyticsStockComponent,
    ThemesLayoutsComponent,
    ThemesThemesOptionsComponent,
    ThemesThemesStaticPagesComponent,
    ThemesThemesFaqsComponent,
    ThemesThemesCustomizeComponent,
    SettingsGeneralSettingsComponent,
    SettingsPaymentSettingsComponent,
    SettingsMarketingSettingssComponent,
    SettingsShippingSettingsComponent,
    AnalyticsOverviewComponent,
    AnalyticsRevenueComponent,
    NewProductComponent,
    TagsComponent,
    AttributesComponent,
    ConfirmOrderAlertComponent,
    RejectOrderAlertComponent,
    PickupRequestComponent,
    SuccessAlertComponent,
    IframeComponent,
    DialogComponent,
    LanguageSwitcherComponent,
    ErrorLayerComponent,
    CommingSoonComponent,
    PaymentMethodsComponent,
    ActivateWizPayComponent,
    WizpayFormComponent,
    OwnConditionalFreeShippingComponent,
    ThemeCustomizeSectionsComponent,
    ThemeCustomizeSectionComponent,
    SectionCustomizeHeadingComponent,
    SectionCustomizeSubheadingComponent,
    PricingComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MatSidenavModule,
    MatRippleModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    MatCardModule,
    ChartsModule,
    MatDialogModule,
    CookieModule.forRoot(),
    MatButtonModule,
    MatIconModule,
    NgxBarcodeModule.forRoot(),
    FormsModule,
    TranslateModule,
    NgxPrintModule,
    MatDatepickerModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NewThemesModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatInputModule,
    MatTabsModule
  ],
})
export class UserModule { }
