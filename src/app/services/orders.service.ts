import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orders, ProductImages, } from '../models/orders.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { AuthService } from './auth.service';
import {Form} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  orderDetailsObj: Orders = {
    line_items: []
  };
  constructor(private http: HttpClient, private auth: AuthService) {
  }

  getOrders(page: number, limit: number): Observable<Orders[]> {
    const params = new HttpParams().set('page', page.toString()).set('per_page', limit.toString());
    // params.set('page', `${page}`);
    // params.set('per_page', `${limit}`);
    console.log(page, limit, 'PAGINATION');
    return this.http.get<Orders[]>(this.auth.GetAPIUrl('wp-json/wc/v3/orders', params));
  }

  getSortedOrders(order: string, limit: number): Observable<Orders[]> {
    const params = new HttpParams().set('order', order).set('per_page', limit.toString());
    return this.http.get<Orders[]>(this.auth.GetAPIUrl('wp-json/wc/v3/orders', params));
  }

  searchInOrders(searchText: string): Observable<Orders[]> {
    const params = new HttpParams().set('search', searchText);
    return this.http.get<Orders[]>(this.auth.GetAPIUrl('wp-json/wc/v3/orders', params));
  }

  getImagesByIDs(idsContainer: any[]): Observable<any[]> {
    const params = new HttpParams().set('include', idsContainer.toString());
    return this.http.get<any[]>(this.auth.GetAPIUrl(`wp-json/wc/v3/products`, params));
  }
  getOrderDetails(id: number): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wc/v3/orders/${id}`));
  }

  confirmOrder(id: number, data: object): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wc/v3/orders/${id}`), data);
  }


  rejectOrder(id: number, data: object): Observable<any> {
    return this.http.put(this.auth.GetAPIUrl(`wp-json/wc/v3/orders/${id}`), data);
  }

  pickUpRequest(id: number): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wizapi/orders/pickup/${id}`), {});
  }

  printAWB(orderId: any): Observable<any> {
    const params = new HttpParams().set('order_id', orderId);
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wizapi/orders/awb`, params));
  }
}

