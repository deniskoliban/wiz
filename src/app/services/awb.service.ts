import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AwbService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  getInvoiceForPrinting(invoiceId: string): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wc/v3/orders/${invoiceId}`));
  }

  getMerchantInfo(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wizapi/merchant-info'));
  }
}
