import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '@services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(private http: HttpClient, private auth: AuthService) { }

  getPaymentMethods(): Observable<{cod_enabled: string, ccod_enabled: string}> {
    return this.http.get<{cod_enabled: string, ccod_enabled: string}>(this.auth.GetAPIUrl(`wp-json/wizapi/payment/methods`));
  }

  setPaymentMethod(data: object): Observable<{cod_enabled: string, ccod_enabled: string}> {
    return this.http.post<{cod_enabled: string, ccod_enabled: string}>(this.auth.GetAPIUrl(`wp-json/wizapi/payment/methods`), data);
  }
}
