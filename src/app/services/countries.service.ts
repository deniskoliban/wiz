import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface City {
  id?: number;
  state_id?: number;
  name?: string;
  city_code?: string;
  latitude?: string;
  longitude?: string;
  active?: number;
  selected?: boolean;
}

export interface State {
  id?: number;
  country_id?: number;
  name?: string;
  state_code?: string;
  active?: number;
  selected?: boolean;
  cities?: Array<City>;
  searchQuery?: string;
}

export interface Country {
  id?: number;
  name?: string;
  iso2?: string;
  iso3?: string;
  phone_code?: string;
  selected?: boolean;
  active?: number;
  states?: Array<State>;
}

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http: HttpClient) { }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('assets/data/countries.json');
  }
}
