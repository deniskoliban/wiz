import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { AuthService } from './auth.service';
import { Orders, OrdersStatus } from '../models/orders.model';
import { DataAfterLogin, IDictionary, MerchantInfo, WizardData } from '../models/home';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  // Objects
  topMenuData: DataAfterLogin = {};
  nameFirstLetter: string | undefined = '';
  nameLastLetter: string | undefined = '';
  storeName: string | undefined = '';
  merchantInfo: MerchantInfo = {
    address: '',
    city: '',
    phone: '',
    name: '',
    business_type: '',
    country: '',
    currency: ''
  };

  merchantInfoSubject = new Subject<MerchantInfo>();
  merchantInfo$: Observable<MerchantInfo> = this.merchantInfoSubject.asObservable();

  constructor(private http: HttpClient, private auth: AuthService) {
    this.getUserDataAfterLogin();
    this.getStoreName();
    this.getMerchantInfo();
  }

  getTotalRevenue(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wc/v3/reports/sales'));
  }

  getVisitorsState(): Observable<object[]> {
    return this.http.get<object[]>(this.auth.GetAPIUrl('wp-json/wizapi/visitors/stats'));
  }


  getTotalProducts(): Observable<OrdersStatus[]> {
    return this.http.get<OrdersStatus[]>(this.auth.GetAPIUrl('wp-json/wc/v3/reports/products/totals'));
  }

  getPendingOrders(): Observable<Orders[]> {
    return this.http.get<Orders[]>(this.auth.GetAPIUrl('wp-json/wc/v3/orders'));
  }

// muhammed
  getBestSellingGraph(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wc/v3/reports/top_sellers'));
  }

  getVisitorsData(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wizapi/visitors/stats'));
  }

  getCurrentCurrency(): Observable<{ currency: string }> {
    return this.http.get<{ currency: string }>(this.auth.GetAPIUrl('wp-json/wizapi/currency'));
  }

  getAbandonedValue(): Observable<[{ count_abandoned_orders: object }]> {
    return this.http.get<[{ count_abandoned_orders: object }]>(this.auth.GetAPIUrl('wp-json/wizapi/orders/abandoned'));
  }

  getActivityBoardData(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wizapi/activity'));
  }

  filterChartByDate(startDate: string, endDate: string): Observable<{ count_date_visitor: string, created_at: string }[]> {
    const params = new HttpParams();
    params.set('start_date', `${startDate}`);
    params.set('end_date', `${endDate}`);
    return this.http.get<{ count_date_visitor: string, created_at: string }[]>(this.auth.GetAPIUrl(`wp-json/wizapi/visitors/graph`, params));
  }

  showViewsDataByDate(startDate: string, endDate: string): Observable<{ created_at: string, count_product: string }[]> {
    const params = new HttpParams();
    params.set('start_date', `${startDate}`);
    params.set('end_date', `${endDate}`);
    return this.http.get<{ created_at: string, count_product: string }[]>(this.auth.GetAPIUrl(`wp-json/wizapi/products/views`));
  }

  getUserDataAfterLogin(): void {
    this.http.get<DataAfterLogin>(this.auth.GetAPIUrl(`wp-json/wizapi/v0/get/merchant`)).subscribe(res => {
      this.topMenuData = res;
      this.nameFirstLetter = res.display_name?.split(' ')[0][0];
      this.nameLastLetter = res.display_name?.split(' ')[res.display_name?.split(' ').length - 1][0];
    });
  }

  getStoreName(): void {
    this.http.get<{ blogname: string }>(this.auth.GetAPIUrl('wp-json/wizapi/v0/get/store')).subscribe(res => {
      this.storeName = res.blogname;
    }, err => console.log(err, 'errr'));
  }

  //  Zeyad
  getOrdersStatus(): Observable<OrdersStatus[]> {
    return this.http.get<OrdersStatus[]>(this.auth.GetAPIUrl('wp-json/wc/v3/reports/orders/totals'));
  }

  getWizardData(): Observable<IDictionary> {
    return this.http.get<IDictionary>(this.auth.GetAPIUrl('/wp-json/wizapi/wizard'));
  }

  verifyShop(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl('wp-json/wizapi/resend-verification'));
  }

  checkMerchant(): void {
    if (this.merchantInfo) {
      this.merchantInfoSubject.next(this.merchantInfo);
    }
  }

  getMerchantInfo(): void {
    this.http.get<MerchantInfo>(this.auth.GetAPIUrl('/wp-json/wizapi/merchant-info')).subscribe(res => {
      this.merchantInfo = res;
      if (this.merchantInfo?.currency != null && this.merchantInfo?.country != null) {
        localStorage.setItem('_country', this.merchantInfo?.country);
      }
      this.merchantInfoSubject.next(res);
    });
  }
}
