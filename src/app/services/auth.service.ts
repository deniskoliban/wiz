import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public authorized = false;
  public jwt = '';
  public api = '';
  constructor(private sanitizer: DomSanitizer, private cookies: CookieService) {
  }

  public GetAPIUrl(base: string, params: HttpParams = new HttpParams()): string {
    params = params.set('jwt', this.jwt);
    return this.GetURL(base, params);
  }

  public GetURL(base: string, params: HttpParams = new HttpParams()): string{
    this.Authorize();
    return `${this.api}${base}?${params.toString()}`;
  }

  public GetIframeURL(redirectUrl: string): SafeResourceUrl {
    this.Authorize();
    redirectUrl = encodeURIComponent(redirectUrl);
    return this.sanitizer.bypassSecurityTrustResourceUrl(`${this.api}jsauth?jwt=${this.jwt}&redirect=${redirectUrl}`);
  }

  public LogOff() {
    this.cookies.remove('WIZ_AUTH');
  }

  public Authorize(): boolean {
    // tslint:disable-next-line: one-variable-per-declaration
    let jwt, api = '';
    this.authorized = false;
    if (environment.production) {
      jwt = this.cookies.get('WIZ_AUTH');
      api = this.cookies.get('WIZ_API');
    } else {
      jwt = environment.jwt ? environment.jwt : this.cookies.get('WIZ_AUTH');
      api = environment.apiWithUrl ? environment.apiWithUrl : this.cookies.get('WIZ_API');
    }

    if (jwt && api) {
      this.api = 'https://' + api + '/';
      this.jwt = jwt;
      this.authorized = true;
      return true;
    } else {
      if (environment.production) {
        window.location.href = 'https://merchant.wizshops.com/login';
      } else {
        console.log(`jwt=${jwt}`);
        console.log(`api=${api}`);
        alert('not authorized!');
      }
    }
    return false;
  }

  isAuthorized(): boolean {
    // if (this.token.length > 0) {
    //   return this.token.length > 0 && this.token !== 'error';
    //
    // }

    /// THIS is useless. DOnt use it as soon as you verify once in the app.component.

    if (this.cookies.get('WIZ_AUTH')) {
      return this.cookies.get('WIZ_AUTH').length > 0 && this.cookies.get('WIZ_AUTH') !== 'error';
    } else {
      return false;
    }
  }
}
