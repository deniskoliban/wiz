import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface InputBlock {
  topCategories: string;
  input: 'checkbox' | '';
  inputPlaceholder: string;
  image?: string;
  value?: string;
}

export interface Subheading {
  title: string;
  subheading: InputBlock[];
}

export interface Section {
  section: string;
  heading?: InputBlock[];
  subheadings?: Subheading[];
}

export interface Layout {
    title: string;
    sections: Section[];
}

export interface Themes {
  [str: string]: Layout;
}


@Injectable({
  providedIn: 'root'
})
export class ThemeCustomizationService {
  currentSection: BehaviorSubject<Section | undefined> = new BehaviorSubject<Section | undefined>(undefined);
  themes: Themes = {
    'layout-1': {
      title: 'Fashion',
      sections: [
        {
          section: 'Slideshow',
          subheadings: [
            {
              title: 'Layer # 1',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
              ],
            },
            {
              title: 'Layer # 2',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
              ],
            },
            {
              title: 'Layer # 3',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
              ],
            },
          ]
        },
        {
          section: 'Banner section',
          subheadings: [
            {
              title: 'Big Banner',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Small Banner 1',
              subheading: [
                {
                  topCategories: 'image 1',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Url 1',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
                {
                  topCategories: 'image 2',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Url 2',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Wide Banner',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: 'New Series',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: 'Power Tools',
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '#',
                },
              ],
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Product section title',
              input: '',
              inputPlaceholder: '',
              value: 'Trendy Item',
            },
            {
              topCategories: 'Products tab 1 title',
              input: '',
              inputPlaceholder: '',
              value: 'ALL',
            },
            {
              topCategories: 'Products tab 2 title',
              input: '',
              inputPlaceholder: '',
              value: 'WOMAN',
            },
            {
              topCategories: 'Products tab 3 title',
              input: '',
              inputPlaceholder: '',
              value: 'MAN',
            },
            {
              topCategories: 'Products tab 4 title',
              input: '',
              inputPlaceholder: '',
              value: 'ON SALE',
            },
            {
              topCategories: 'Products tab 5 title',
              input: '',
              inputPlaceholder: '',
              value: 'NEW',
            },
          ]
        },
        {
          section: 'Products Deals Section',
          heading: [
            {
              topCategories: 'Product deals background',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*853',
            },
          ]
        },
      ]
    },
    'layout-2': {
      title: 'Retail',
      sections: [
        {
          section: 'Categories Section',
          heading: [
            {
              topCategories: 'Top categories background',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*497',
            },
            {
              topCategories: 'Top categories title',
              input: '',
              inputPlaceholder: 'Top Categories',
            },
            {
              topCategories: 'Top categories subtitle',
              input: '',
              inputPlaceholder: 'Lorem Ipsum has been the industry',
            },
            {
              topCategories: 'Top categories items',
              input: 'checkbox',
              inputPlaceholder: 'Uncategorized',
            },
          ]
        },
        {
          section: 'List Of Features',
          subheadings: [
            {
              title: 'Shipping',
              subheading: [
                {
                  topCategories: 'Shipping Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Free Shipping',
                },
                {
                  topCategories: 'Shipping Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Free Shipping for all US order'
                },
              ],
            },
            {
              title: 'Support',
              subheading: [
                {
                  topCategories: 'Support Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Support 24/7',
                },
                {
                  topCategories: 'Support Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'We support 24h a day'
                },
              ],
            },
            {
              title: 'Money Back',
              subheading: [
                {
                  topCategories: 'Money Back Title',
                  input: '',
                  inputPlaceholder: '',
                  value: '100% Money Back',
                },
                {
                  topCategories: 'Money Back Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'You have 30 days to Return'
                },
              ],
            },
            {
              title: 'Payment Secure',
              subheading: [
                {
                  topCategories: 'Payment Secure Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Payment Secure',
                },
                {
                  topCategories: 'Payment Secure Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'We ensure secure payment'
                },
              ],
            },
          ]
        },
        {
          section: 'First Products Section',
          heading: [
            {
              topCategories: 'First products section left title',
              input: '',
              inputPlaceholder: '',
              value: 'Special',
            },
            {
              topCategories: 'First products section right title',
              input: '',
              inputPlaceholder: '',
              value: 'Digital',
            },
          ]
        },
        {
          section: 'Splitter Text Slider',
          heading: [
            {
              topCategories: 'Splitter slide 1 text',
              input: '',
              inputPlaceholder: '',
              value: 'UP TO 30% OFF THE ENTIRE STORE! - MADE WITH LOVE by US',
            },
            {
              topCategories: 'Splitter slide 2 text',
              input: '',
              inputPlaceholder: '',
              value: 'UP TO 40% OFF THE ENTIRE STORE! - STARTING FROM JAN 2021',
            },
          ]
        },
        {
          section: 'Second Products Section',
          heading: [
            {
              topCategories: 'Second products section left title',
              input: '',
              inputPlaceholder: '',
              value: 'Furniture',
            },
            {
              topCategories: 'Second products section right title',
              input: '',
              inputPlaceholder: '',
              value: 'Special',
            },
          ]
        },
        {
          section: 'Banner section',
          subheadings: [
            {
              title: 'Image 1',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*497'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools',
                },
              ],
            },
            {
              title: 'Image 2',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*497'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Special Sale',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'on products',
                },
              ],
            },
            {
              title: 'Image 3',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*497'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power tools',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: '&Accessories',
                },
              ],
            },
          ]
        },
        {
          section: 'Third Products Section',
          heading: [
            {
              topCategories: 'Third products section title',
              input: '',
              inputPlaceholder: '',
              value: 'Handtools',
            },
            {
              topCategories: 'Third products section subtitle',
              input: '',
              inputPlaceholder: '',
              value: 'lorem ipsum has been the industry standard dummy text ever since the 1500s',
            },
          ]
        },
        {
          section: 'About Us Section',
          heading: [
            {
              topCategories: 'About YouTube url',
              input: '',
              inputPlaceholder: '',
              value: 'https://www.youtube.com/watch?v=dmftqrnbse0',
            },
            {
              topCategories: 'About us title',
              input: '',
              inputPlaceholder: '',
              value: 'About Us',
            },
            {
              topCategories: 'About us description',
              input: '',
              inputPlaceholder: '',
              value: 'Our mission is to bring together a diverse, curated collection',
            },
          ],
          subheadings: [
            {
              title: 'About info 1',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '60*60'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'We work in Global',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem ipsum'
                },
              ],
            },
            {
              title: 'About info 2',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '60*60'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Our guarantee',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'From 4 - 8 years'
                },
              ],
            },
            {
              title: 'About info 3',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '60*60'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'On the market',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: '12 years'
                },
              ],
            },
            {
              title: 'About info 4',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '60*60'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Best quality',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem ipsum'
                },
              ],
            },
          ]
        },
      ]
    },
    'layout-3': {
      title: 'Grocery, Health & Personal Care',
      sections: [
        {
          section: 'Slideshow',
          subheadings: [
            {
              title: 'Layer # 1',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
              ],
            },
            {
              title: 'Layer # 2',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Power Tools'
                },
              ],
            },
            {
              title: 'Layer # 3',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
              ],
            },
          ]
        },
        {
          section: 'List Of Features',
          heading: [
            {
              topCategories: 'Features shipping title',
              input: '',
              inputPlaceholder: '',
              value: 'Free Shipping',
            },
            {
              topCategories: 'Features shipping description',
              input: '',
              inputPlaceholder: '',
              value: 'Free Shipping for all US order',
            },
            {
              topCategories: 'Features support title',
              input: '',
              inputPlaceholder: '',
              value: 'Support 24/7',
            },
            {
              topCategories: 'Features support description',
              input: '',
              inputPlaceholder: '',
              value: 'We support 24h a day',
            },
            {
              topCategories: 'Features money back title',
              input: '',
              inputPlaceholder: '',
              value: '100% Money Back',
            },
            {
              topCategories: 'Features money back description',
              input: '',
              inputPlaceholder: '',
              value: 'You have 30 days to Return',
            },
            {
              topCategories: 'Features payment secure title',
              input: '',
              inputPlaceholder: '',
              value: 'Payment Secure',
            },
            {
              topCategories: 'Features payment secure description',
              input: '',
              inputPlaceholder: '',
              value: 'We ensure secure payment',
            },
          ]
        },
        {
          section: 'Categories Section',
          heading: [
            {
              topCategories: 'Top categories title',
              input: '',
              inputPlaceholder: '',
              value: 'Top Categories',
            },
            {
              topCategories: 'Top categories items',
              input: 'checkbox',
              inputPlaceholder: 'Uncategorized',
            },
          ]
        },
        {
          section: 'Products By Categories Section',
          heading: [
            {
              topCategories: 'Products section title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Sellers Products',
            },
            {
              topCategories: 'Products category 1 title',
              input: '',
              inputPlaceholder: '',
              value: 'Fruits',
            },
            {
              topCategories: 'Products category 2 title',
              input: '',
              inputPlaceholder: '',
              value: 'Vegetables',
            },
            {
              topCategories: 'Products category 3 title',
              input: '',
              inputPlaceholder: '',
              value: 'Milk & Cream',
            },
            {
              topCategories: 'Products category 4 title',
              input: '',
              inputPlaceholder: '',
              value: 'Banana',
            },
          ]
        },
        {
          section: 'Banner section',
          subheadings: [
            {
              title: 'Image 1',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Joobie<br />Ice Cream',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sale Off 25%'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: 'Discover now',
                  value: 'Shop now'
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Image 2',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Joobie<br />Guava Juice',
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: 'Discover now',
                  value: 'Shop now'
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Image 3',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Joobie<br />Vegetables',
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shop now'
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
          ]
        },
        {
          section: 'Columns Products Section',
          heading: [
            {
              topCategories: 'Column 1 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Top Rate',
            },
            {
              topCategories: 'Column 2 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Selling',
            },
            {
              topCategories: 'Column 3 products title',
              input: '',
              inputPlaceholder: '',
              value: 'On Sale',
            },
          ]
        },
      ]
    },
    'layout-4': {
      title: 'Electronics Accessories',
      sections: [
        {
          section: 'Accessories Banner',
          heading: [
            {
              topCategories: 'Accessories paralex image',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*500',
            },
            {
              topCategories: 'Accessories paralex title',
              input: '',
              inputPlaceholder: 'Upload Image',
              value: 'Select Your Favorite <span class=“primary-color” style=“font-weight: bold;”',
            },
            {
              topCategories: 'Accessories paralex subtitle',
              input: '',
              inputPlaceholder: 'Upload Image',
              value: 'OVER 10.000 ACCESSORIES',
            },
          ]
        },
        {
          section: 'Banner section',
          subheadings: [
            {
              title: 'Banner 1 Image',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Joobie<br />Ice Cream',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sale Off 25%'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shop Now'
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Banner 2 Image',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
              ],
            },
            {
              title: 'Banner 3 Image',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Joobie<br />Ice Cream',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sale Off 25%'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shop Now'
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
          ]
        },
        {
          section: 'Categories Section',
          heading: [
            {
              topCategories: 'Categories section title',
              input: '',
              inputPlaceholder: '',
              value: 'Featured Category',
            },
            {
              topCategories: 'Categories section items',
              input: 'checkbox',
              inputPlaceholder: 'Uncategorized',
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Product section title',
              input: '',
              inputPlaceholder: '',
              value: 'Trendy Items',
            },
            {
              topCategories: 'Products category 1 title',
              input: '',
              inputPlaceholder: '',
              value: 'NEW ARRIVALS 2021',
            },
            {
              topCategories: 'Products category 2 title',
              input: '',
              inputPlaceholder: '',
              value: 'FEATURED',
            },
            {
              topCategories: 'Products category 3 title',
              input: '',
              inputPlaceholder: '',
              value: 'ON SALE',
            },
          ]
        },
        {
          section: 'Products Smart Slider',
          heading: [
            {
              topCategories: 'Products slide currency',
              input: '',
              inputPlaceholder: '',
              value: '$',
            },
          ],
          subheadings: [
            {
              title: 'Product Slide 1',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*480'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'MOBILE ACCESSORIES',
                },
                {
                  topCategories: 'Product name',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Apple watch Sport Band'
                },
                {
                  topCategories: 'Price',
                  input: '',
                  inputPlaceholder: '',
                  value: '299.00'
                },
                {
                  topCategories: 'Sale price',
                  input: '',
                  inputPlaceholder: '',
                  value: '199.00'
                },
                {
                  topCategories: 'Sale expire date',
                  input: '',
                  inputPlaceholder: '',
                  value: '2021-03-31'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shop Now'
                },
                {
                  topCategories: 'Button url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Product Slide 2',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*220'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'MOBILE ACCESSORIES',
                },
                {
                  topCategories: 'Product name',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Integrated PTZ camera Stabilizes'
                },
                {
                  topCategories: 'Price',
                  input: '',
                  inputPlaceholder: '',
                  value: '455.00'
                },
                {
                  topCategories: 'Sale price',
                  input: '',
                  inputPlaceholder: '',
                  value: '310.00'
                },
                {
                  topCategories: 'Sale expire date',
                  input: '',
                  inputPlaceholder: '',
                  value: '2021-04-31'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shop Now'
                },
                {
                  topCategories: 'Button url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
          ]
        },
        {
          section: 'Columns Products Section',
          heading: [
            {
              topCategories: 'Column 1 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Top Rate',
            },
            {
              topCategories: 'Column 2 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Selling',
            },
            {
              topCategories: 'Column 3 products title',
              input: '',
              inputPlaceholder: '',
              value: 'On Sale',
            },
          ]
        },
        {
          section: 'Recently Viewed Products Section',
          heading: [
            {
              topCategories: 'Recently viewed products title',
              input: '',
              inputPlaceholder: '',
              value: 'Recently Viewed',
            },
          ]
        },
      ]
    },
    'layout-5': {
      title: 'Furniture',
      sections: [
        {
          section: 'Header Banner',
          heading: [
            {
              topCategories: 'Header banner image',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*805',
            },
          ]
        },
        {
          section: 'Categories Banners',
          heading: [
            {
              topCategories: 'Top categories title',
              input: '',
              inputPlaceholder: '',
              value: 'Featured Categories',
            },
            {
              topCategories: 'Top categories subtitle',
              input: '',
              inputPlaceholder: '',
              value: 'The freshest and most exciting Categories',
            },
          ],
          subheadings: [
            {
              title: 'Category 1 Wide',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '580*260'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sofas',
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Category 2 Small',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '280*250'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sofas',
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Category 3 Small',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '580*260'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sofas',
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Category 4 Big',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '580*530'
                },
                {
                  topCategories: 'Link text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sofas',
                },
                {
                  topCategories: 'Link url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Products section title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Sellers Product',
            },
            {
              topCategories: 'Products tab 1 title',
              input: '',
              inputPlaceholder: '',
              value: 'ALL',
            },
            {
              topCategories: 'Products tab 2 title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Seller',
            },
            {
              topCategories: 'Products tab 3 title',
              input: '',
              inputPlaceholder: '',
              value: 'Featured',
            },
            {
              topCategories: 'Products tab 4 title',
              input: '',
              inputPlaceholder: '',
              value: 'ON SALE',
            },
            {
              topCategories: 'Products tab 5 title',
              input: '',
              inputPlaceholder: '',
              value: 'Deal',
            },
          ]
        },
        {
          section: 'Splitter Text Slider',
          heading: [
            {
              topCategories: 'Splitter slide 1 text',
              input: '',
              inputPlaceholder: '',
              value: 'UP TO 30% OFF THE ENTIRE STORE! - MADE WITH LOVE by US',
            },
            {
              topCategories: 'Splitter slide 2 text',
              input: '',
              inputPlaceholder: '',
              value: 'UP TO 50% OFF THE ENTIRE STORE! - STARTING FROM JAN 2021',
            },
          ]
        },
        {
          section: 'About Us Section',
          heading: [
            {
              topCategories: 'About YouTube url',
              input: '',
              inputPlaceholder: '',
              value: 'https://www.youtube.com/watch?v=DmFtQrnBSe0',
            },
            {
              topCategories: 'About us title',
              input: '',
              inputPlaceholder: '',
              value: 'About Us',
            },
            {
              topCategories: 'About us description',
              input: '',
              inputPlaceholder: '',
              value: 'Our mission is to bring together a diverse, curated collection of beautiful fu',
            },
          ],
          subheadings: [
            {
              title: 'Info 1',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '43*42'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'We work in Global',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem ipsum'
                },
              ],
            },
            {
              title: 'Info 2',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '43*42'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Our guarantee',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'From 4 - 8 years'
                },
              ],
            },
            {
              title: 'Info 3',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '43*42'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'On the market',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: '12 years'
                },
              ],
            },
            {
              title: 'Info 4',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '43*42'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Best quality',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem ipsum'
                },
              ],
            },
          ]
        },
        {
          section: 'Columns Products Section',
          heading: [
            {
              topCategories: 'Column 1 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Top Rate',
            },
            {
              topCategories: 'Column 2 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Selling',
            },
            {
              topCategories: 'Column 3 products title',
              input: '',
              inputPlaceholder: '',
              value: 'On Sale',
            },
          ]
        },
      ]
    },
    'layout-6': {
      title: 'Electronics & Major Appliances',
      sections: [
        {
          section: 'Slideshow',
          subheadings: [
            {
              title: 'Layer # 1',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '880*350'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Featured Product',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Moto phone<br />New Trending Design'
                },
                {
                  topCategories: 'Price info',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Only $199.00'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Get it Now'
                },
              ],
            },
            {
              title: 'Layer # 2',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '880*350'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Best Selling',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Arrivals<br>Best Perfomance'
                },
                {
                  topCategories: 'Price Info',
                  input: '',
                  inputPlaceholder: '',
                  value: '199.00'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Get it Now'
                },
              ],
            },
          ]
        },
        {
          section: 'Banners Section',
          subheadings: [
            {
              title: 'Banner 1',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '277*121'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'NEW 360 VR',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: '30% off this week'
                },
              ],
            },
            {
              title: 'Banner 2',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '277*121'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'MOTO-Z',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sale off 45%'
                },
              ],
            },
            {
              title: 'Banner 3',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '277*121'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'NEW PHONE',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Exclusive offer'
                },
              ],
            },
          ]
        },
        {
          section: 'Left Column Items',
          heading: [
            {
              topCategories: 'Special deals products title',
              input: '',
              inputPlaceholder: '',
              value: 'Special offer',
            },
            {
              topCategories: 'Top rated products title',
              input: '',
              inputPlaceholder: '',
              value: 'Top Rated',
            },
          ]
        },
        {
          section: 'Best Selling Products Section',
          heading: [
            {
              topCategories: 'Best selling products title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Selling',
            },
          ]
        },
        {
          section: 'Wide Banner 1 Section',
          heading: [
            {
              topCategories: 'Image',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '860*157',
            },
            {
              topCategories: 'Title',
              input: '',
              inputPlaceholder: '',
              value: 'Video Meeting',
            },
            {
              topCategories: 'Subtitle',
              input: '',
              inputPlaceholder: '',
              value: 'Meetup Conference Cam',
            },
            {
              topCategories: 'Price info',
              input: '',
              inputPlaceholder: '',
              value: 'Only $159.00',
            },
          ]
        },
        {
          section: 'Deal Offers Products Section',
          heading: [
            {
              topCategories: 'Deal offers products title',
              input: '',
              inputPlaceholder: '',
              value: 'Deal Offer',
            },
          ]
        },
        {
          section: 'Wide Banner 2 Section',
          heading: [
            {
              topCategories: 'Image',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '860*157',
            },
            {
              topCategories: 'Title',
              input: '',
              inputPlaceholder: '',
              value: 'BIG SALE',
            },
            {
              topCategories: 'Subtitle',
              input: '',
              inputPlaceholder: '',
              value: 'For all Virtual Reality Glasses',
            },
            {
              topCategories: 'Description',
              input: '',
              inputPlaceholder: '',
              value: 'Saving 70% Off on all online store items',
            },
          ]
        },
        {
          section: 'On Sale Products Section',
          heading: [
            {
              topCategories: 'On sale products title',
              input: '',
              inputPlaceholder: '',
              value: 'On Sale',
            },
          ]
        },
        {
          section: 'Splitter Text Slider',
          heading: [
            {
              topCategories: 'Splitter slide 1 text',
              input: '',
              inputPlaceholder: '',
              value: 'With each receipt over $150 from Digi get voucher <span style=“font-size: 1',
            },
            {
              topCategories: 'Splitter slide 2 text',
              input: '',
              inputPlaceholder: '',
              value: '10% instant discount using <span style=“font-size: 140%;”>NBK bank </spa',
            },
          ]
        },
        {
          section: 'Columns Products Section',
          heading: [
            {
              topCategories: 'Column 1 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Featured',
            },
            {
              topCategories: 'Column 2 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Best Selling',
            },
            {
              topCategories: 'Column 3 products title',
              input: '',
              inputPlaceholder: '',
              value: 'Recent',
            },
          ]
        },
      ]
    },
    'layout-7': {
      title: 'Beauty & Cosmetics',
      sections: [
        {
          section: 'Slideshow',
          subheadings: [
            {
              title: 'Layer # 1',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'WIZ store',
                  value: 'WIZ store',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'natural<br>spa cosmetic',
                  value: 'natural<br>spa cosmetic'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'shop now',
                  value: 'shop now'
                },
              ],
            },
            {
              title: 'Layer # 2',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'New Series',
                  value: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'Power Tools',
                  value: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'Power Tools',
                  value: 'Power Tools'
                },
              ],
            },
            {
              title: 'Layer # 3',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'New Series',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'Power Tools'
                },
              ],
            },
          ]
        },
        {
          section: 'Testimonials Sections',
          subheadings: [
            {
              title: 'Client Review 1',
              subheading: [
                {
                  topCategories: 'Content',
                  input: '',
                  inputPlaceholder: '',
                  value: 'I really like my shopping experience here <br>they has large product'
                },
                {
                  topCategories: 'Client name',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Ahmed Taher',
                },
                {
                  topCategories: 'Client job',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Accountant'
                },
              ],
            },
            {
              title: 'Client Review 1',
              subheading: [
                {
                  topCategories: 'Content',
                  input: '',
                  inputPlaceholder: '',
                  value: 'I will buy from your store again, <br>really very good quality of prod'
                },
                {
                  topCategories: 'Client name',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Soha Gameel',
                },
                {
                  topCategories: 'Client job',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sergant'
                },
              ],
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Products tab 1 title',
              input: '',
              inputPlaceholder: 'ALL',
              value: 'ALL',
            },
            {
              topCategories: 'Products tab 2 title',
              input: '',
              inputPlaceholder: 'WOMEN',
              value: 'WOMEN',
            },
            {
              topCategories: 'Products tab 3 title',
              input: '',
              inputPlaceholder: 'MEN',
              value: 'MEN',
            },
            {
              topCategories: 'Products tab 4 title',
              input: '',
              inputPlaceholder: 'ON SALE',
              value: 'ON SALE',
            },
            {
              topCategories: 'Products tab 5 title',
              input: '',
              inputPlaceholder: 'NEW',
              value: 'NEW',
            },
          ]
        },
        {
          section: 'Portfolio Section',
          heading: [
            {
              topCategories: 'Background',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*650'
            },
            {
              topCategories: 'Title',
              input: '',
              inputPlaceholder: 'Pink Platform Sneakers',
              value: 'WIZ store '
            },
            {
              topCategories: 'Subtitle',
              input: '',
              inputPlaceholder: 'natural<br>spa cosmetic',
            },
          ],
          subheadings: [
            {
              title: 'Portfolio Item 1',
              subheading: [
                {
                  topCategories: 'Image before',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Image after',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Before - After',
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
                {
                  topCategories: 'Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem Ipsum has been the industry standard dummy text ever sinc'
                },
              ],
            },
            {
              title: 'Portfolio Item 2',
              subheading: [
                {
                  topCategories: 'Image before',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Image after',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Before - After',
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
                {
                  topCategories: 'Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem Ipsum has been the industry standard dummy text ever sinc'
                },
              ],
            },
            {
              title: 'Portfolio Item 3',
              subheading: [
                {
                  topCategories: 'Image before',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Image after',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '380*422'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Before - After',
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
                {
                  topCategories: 'Description',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Lorem Ipsum has been the industry standard dummy text ever sinc'
                },
              ],
            },
          ]
        },
      ]
    },
    'layout-8': {
      title: 'Shoes',
      sections: [
        {
          section: 'Banners Section',
          subheadings: [
            {
              title: 'Banner 1',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '430*245'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New <br>season 2021',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shoes & Accessories'
                },
              ],
            },
            {
              title: 'Banner 2',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '430*245'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Classic Collections',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Sale off 70%'
                },
              ],
            },
            {
              title: 'Banner 3',
              subheading: [
                {
                  topCategories: 'Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '430*245'
                },
                {
                  topCategories: 'Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Spring <br>Arrivals',
                },
                {
                  topCategories: 'Subtitle',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Shoes Collections'
                },
              ],
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Product section title',
              input: '',
              inputPlaceholder: 'Trendy Items',
              value: 'Trendy Items',
            },
            {
              topCategories: 'Products tab 1 title',
              input: '',
              inputPlaceholder: 'ALL',
              value: 'ALL',
            },
            {
              topCategories: 'Products tab 2 title',
              input: '',
              inputPlaceholder: 'FEATURED',
              value: 'FEATURED',
            },
            {
              topCategories: 'Products tab 3 title',
              input: '',
              inputPlaceholder: 'BEST SELLING',
              value: 'BEST SELLING',
            },
            {
              topCategories: 'Products tab 4 title',
              input: '',
              inputPlaceholder: 'TOP RATED',
              value: 'TOP RATED',
            },
            {
              topCategories: 'Products tab 5 title',
              input: '',
              inputPlaceholder: '',
              value: 'TRENDS',
            },
          ]
        },
        {
          section: 'Featured Products Section',
          heading: [
            {
              topCategories: 'Featured product image',
              input: '',
              inputPlaceholder: 'Trendy Items',
              image: '1160*820'
            },
            {
              topCategories: 'Featured product title',
              input: '',
              inputPlaceholder: '',
              value: 'Pink Platform Sneakers',
            },
            {
              topCategories: 'Featured product currency',
              input: '',
              inputPlaceholder: '',
              value: '$',
            },
            {
              topCategories: 'Featured product price',
              input: '',
              inputPlaceholder: '',
              value: '50',
            },
            {
              topCategories: 'Featured product sale price',
              input: '',
              inputPlaceholder: '',
              value: '39',
            },
            {
              topCategories: 'Featured product expire date',
              input: '',
              inputPlaceholder: '',
              value: '2020-12-31',
            },
            {
              topCategories: 'Featured product button text',
              input: '',
              inputPlaceholder: '',
              value: 'SHOP NOW',
            },
            {
              topCategories: 'Featured product button url',
              input: '',
              inputPlaceholder: '',
              value: '#',
            },
          ]
        },
      ]
    },
    'layout-9': {
      title: 'Jewelry',
      sections: [
        {
          section: 'Slideshow',
          subheadings: [
            {
              title: 'Layer # 1',
              subheading: [
                {
                  topCategories: 'background image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'Gold Bracelets',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Exclusive Collections with Awesome Design'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'SHOW NOW'
                },
              ],
            },
            {
              title: 'Layer # 2',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'WIZ store',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: '',
                  value: 'New Collections with Awesome Design'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: '',
                  value: 'shop now'
                },
              ],
            },
            {
              title: 'Layer # 3',
              subheading: [
                {
                  topCategories: 'Background Image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '1920*1280'
                },
                {
                  topCategories: 'Mini Title',
                  input: '',
                  inputPlaceholder: 'WIZ store',
                },
                {
                  topCategories: 'Big Title',
                  input: '',
                  inputPlaceholder: 'Pendants Collections with Awesome Design'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'shop now'
                },
              ],
            },
          ]
        },
        {
          section: 'Banner section',
          subheadings: [
            {
              title: 'Big Banner',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '580*580'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: 'Collections for <br>Love That Rings True',
                  value: 'Collections for <br>Love That Rings True',
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: 'From $99.00',
                  value: 'From $99.00'
                },
                {
                  topCategories: 'Button text',
                  input: '',
                  inputPlaceholder: 'Discover now',
                  value: 'Discover now'
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '',
                  value: '#'
                },
              ],
            },
            {
              title: 'Wide Banner',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '580*280'
                },
                {
                  topCategories: 'title',
                  input: '',
                  inputPlaceholder: 'Gift <br>for her',
                  value: 'Gift <br>for her'
                },
                {
                  topCategories: 'subtitle',
                  input: '',
                  inputPlaceholder: 'From $49.00',
                  value: 'From $49.00'
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '#',
                  value: '#',
                },
              ],
            },
            {
              title: 'Small Banner 1',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '280*280'
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '#',
                  value: '#'
                },
              ],
            },
            {
              title: 'Small Banner 2',
              subheading: [
                {
                  topCategories: 'image',
                  input: '',
                  inputPlaceholder: 'Upload Image',
                  image: '280*280'
                },
                {
                  topCategories: 'Url',
                  input: '',
                  inputPlaceholder: '#',
                  value: '#'
                },
              ],
            },
          ]
        },
        {
          section: 'Products Section',
          heading: [
            {
              topCategories: 'Product section title',
              input: '',
              inputPlaceholder: '',
              value: 'Trendy Items',
            },
            {
              topCategories: 'Products tab 1 title',
              input: '',
              inputPlaceholder: '',
              value: 'ALL',
            },
            {
              topCategories: 'Products tab 2 title',
              input: '',
              inputPlaceholder: '',
              value: 'FEATURED',
            },
            {
              topCategories: 'Products tab 3 title',
              input: '',
              inputPlaceholder: '',
              value: 'BEST SELLING',
            },
            {
              topCategories: 'Products tab 4 title',
              input: '',
              inputPlaceholder: '',
              value: 'TOP RATED',
            },
          ]
        },
        {
          section: 'Call to Action Section',
          heading: [
            {
              topCategories: 'Image',
              input: '',
              inputPlaceholder: 'Upload Image',
              image: '1920*570'
            },
            {
              topCategories: 'Title',
              input: '',
              inputPlaceholder: 'Pink Platform Sneakers',
            },
            {
              topCategories: 'Description',
              input: '',
              inputPlaceholder: '$',
            },
            {
              topCategories: 'Description 2',
              input: '',
              inputPlaceholder: '50',
            },
            {
              topCategories: 'Link text',
              input: '',
              inputPlaceholder: '39',
            },
            {
              topCategories: 'Url',
              input: '',
              inputPlaceholder: '2020-12-31',
            },
          ]
        },
        {
          section: 'Categories Section',
          heading: [
            {
              topCategories: 'Title',
              input: '',
              inputPlaceholder: 'Shop By Category',
              value: 'Shop By Category',
            },
            {
              topCategories: 'Subtitle',
              input: '',
              inputPlaceholder: 'Visit our shop to see amazing creations from our designers',
              value: 'Visit our shop to see amazing creations from our designers',
            },
            {
              topCategories: 'Items',
              input: 'checkbox',
              inputPlaceholder: 'Uncategorized',
            },
          ]
        },
      ]
    },
  };

  constructor() { }

  toCamelCaseString(str: string): string {
    return str.split(' ')
      .map((word, index) => {
        if (index === 0) {
          return word.toLowerCase();
        } else {
          return word[0].toUpperCase() + word.substr(1).toLowerCase();
        }
      })
      .join('');
  }
}
