import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { COD, DeliveryRegions, ShippingCharges, ShippingChargesOwn } from '@src/app/models/shipping';
import { Countries, PickUpAddress } from '@src/app/models/pickup-address.model';
import { OwnFreeShipping } from '@src/app/models/own.free-shipping.model';

export enum DeliveryMethod {
  WIZ = 'wiz',
  OWN = 'own'
}

export interface ShippingHeader {
  step?: string;
  head?: string;
  bigParagraph?: string;
  smallParagraph?: string;
  viewPricing?: ViewPricing;
}
export interface ViewPricing {
  link?: string;
}
@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  GMTime = -(new Date().getTimezoneOffset() / 60);

  constructor(private http: HttpClient, private auth: AuthService) { }


  // ***************** GET SHOP PRODUCTS (SELECTED) ****************** //

  getShopProducts(): Observable<Array<string>> {
    return this.http.get<{ categories: Array<string> }>(
      this.auth.GetAPIUrl(`wp-json/wizapi/shop/categories`))
      .pipe(
        map( data => data.categories)
      );
  }


  // ***************** POST (SELECTED) PRODUCTS ****************** //
  postSelectedProducts(data: { categories: Array<string> }): Observable<void> {
    return this.http.post<void>(this.auth.GetAPIUrl(`wp-json/wizapi/shop/categories`), data);
  }

  getShippingCharges(): Observable<ShippingCharges> {
    return this.http.get<ShippingCharges>(this.auth.GetAPIUrl('/wp-json/wizapi/shipping/charges'));
  }

  setShippingCharges(shippingCharges: ShippingCharges): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl('/wp-json/wizapi/shipping/charges'), shippingCharges);
  }

  getOwnShippingCharges<T>(): Observable<OwnFreeShipping<T> | ShippingChargesOwn> {
    return this.http.get<OwnFreeShipping<T>>(this.auth.GetAPIUrl('/wp-json/wizapi/shipping/charges/own'));
  }

  setOwnShippingCharges(Data: OwnFreeShipping): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`/wp-json/wizapi/shipping/charges/own`), Data);
  }

  getCOD(): Observable<COD> {
    return this.http.get<COD>(this.auth.GetAPIUrl('/wp-json/wizapi/shipping/own/cod'));
  }

  setCOD(cod: COD): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl('/wp-json/wizapi/shipping/own/cod'), cod);
  }

  setDeliveryRegions(deliveryRegions: DeliveryRegions): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl('/wp-json/wizapi/delivery/region'), deliveryRegions);
  }

  getDeliveryRegions(): Observable<DeliveryRegions> {
    return this.http.get<DeliveryRegions>(this.auth.GetAPIUrl('/wp-json/wizapi/delivery/region'));
  }

  // ***************** SET DELIVERY METHOD ****************** //
  setDeliveryMethod(data: object): Observable<{delivery_method: DeliveryMethod}> {
    return this.http.post<{delivery_method: DeliveryMethod}>(this.auth.GetAPIUrl(`wp-json/wizapi/delivery/method`), data);
  }

  // ***************** GET DELIVERY METHOD ****************** //
  getDeliveryMethod(): Observable<{ delivery_method: DeliveryMethod }> {
    return this.http.get<{ delivery_method: DeliveryMethod }>(this.auth.GetAPIUrl(`wp-json/wizapi/delivery/method`));
  }


  // ***************** GET PICKUP ADDRESS ****************** //
  getPickupAddress(): Observable<PickUpAddress> {
    return this.http.get<PickUpAddress>(this.auth.GetAPIUrl(`wp-json/wizapi/pickup_address`));
  }

  setPickupAddress(data: PickUpAddress): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wizapi/pickup_address`), data);
  }
  // ******************* GET COUNTRIES AND STATES ************************* //

  countriesAndStates(): Observable<Array<Countries>> {
    return this.http.get<Array<Countries>>(`assets/data/countries.json`);
  }

}

// https://wiz-logistics-staging.s3.me-south-1.amazonaws.com/countries.json
