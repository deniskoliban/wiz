import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private http: HttpClient, private auth: AuthService) { }

  getThemeOptions(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wizapi/v0/theme/options`));
  }

  saveThemeOptions(data: Object): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wizapi/v0/theme/options`), data);
  }

  getThemes(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wizapi/layouts`));
  }

  activeCurrentTheme(data: object): Observable<any> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wizapi/layout/active`), data);
  }

  getForceOfflineStatus(): Observable<any> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wizapi/force/offline`));
  }

  uploadMedia(data: any, optionId: any): Observable<Object> {
    return this.http.post(this.auth.GetAPIUrl(`wp-json/wizapi/v0/theme/media/upload/` + optionId), data)
  }

  getLayoutDetail(): Observable<Object> {
    return this.http.get(this.auth.GetAPIUrl(`wp-json/wizapi/layouts`));
  }


}
