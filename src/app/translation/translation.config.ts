import { MissingTranslationHandler, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from './http-loader.factory';
import { HttpClient } from '@angular/common/http';
import { MissingTranslationService } from './missing-translation.service';

export const translationConfig = {
  loader: {
    provide: TranslateLoader,
    useFactory: HttpLoaderFactory,
    deps: [HttpClient],
  },
  missingTranslationHandler: { provide: MissingTranslationHandler, useClass: MissingTranslationService },
};
