FROM node:15.2.1-alpine3.10 as builder

WORKDIR /app
ARG ENVIRONMENT=staging
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app  dependencies
COPY package.json /app/package.json

RUN npm install --force
RUN npm install -g @angular/cli

# add app
COPY . /app

RUN ng build -c ${ENVIRONMENT} --output-path=dist --base-href=/storeadmin/

FROM nginx:1.14.1-alpine

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/dist /usr/share/nginx/html/storeadmin

CMD ["nginx", "-g", "daemon off;"]
